# Logistic Regression

The foundational logic in deep learning is the idea of using some
training data, *x*, to determine the probability of a label, *y*:
*ŷ* = *P*(*y* = 1\|*x*). For example, given if a picture contains eyes,
ears, nose, mouth, etc.(x data), the picture is likely that of a face (y
data). This was originally achieved with application of a sigmoidal
function.In more recent years, it has been determined that this
calculation can be completed faster if Rectified Learning Units (ReLU)
are used for the calculation rather than the sigmoidal function. We will
start our work understanding the foundational sigmoidal function.

![](/Users/jolante/Bryan/ML_work/deep_learning.ai_coursera/Month1:%20Neural%20Networks%20and%20Deep%20Learning/sig_vs_relu.png)

## The Sigmoid function

To determine the probability of *y*, denoted by *ŷ*, we can attempt to
make a description with parameters, *w*, and a scalar, *b*:
*ŷ* = *w*<sup>*T*</sup> \* *x* + *b*. However, this does not work
because the output of this function is either negative or much larger
than one. As we are looking for the probability of *y*, we require a
value between 0 and 1. Because of this the sigmoidal activation function
was developed. This can be shown by
*ŷ* = *σ*(*w*<sup>*T*</sup> \* *x* + *b*) or *ŷ* = *σ*(*z*), where
*z* = (*w*<sup>*T*</sup> \* *x* + *b*) and
$\\sigma (z) = \\frac{1}{1 + e^{-z}}$. This transforms the outputs from
the first equation, be they large or negative, and fits them between 0
and 1. In other words, if *z* is a large number you have,
$\\sigma (z)= \\frac{1}{1+0}$ is \~ 1 and if *z* is small you are left
with, $\\sigma (z)= \\frac{1}{1+ BigNumber}$ which is \~ 0. Thus, the
sigmoid function is an effective way to insure a probability is found
for *y* given *x*.

## Picking values for w and b

A loss function is applied to a singular training example and through
super-position, it is expanded outward into a cost function to find the
average of the loss for each parameter.

### Loss Function

A general loss function used to develop a cost function is
$L(\\hat{y},y) = \\frac{1}{2}(\\hat{y}-y)^{2}$. This is the square
error. This cost function is designed to help train the parameters *w*
and *b*. This seems like a good fit but it has an oscillatory problem in
application. Instead it is preferential to use:

*L*(*ŷ*, *y*) =  − (*y*log *ŷ* + (1 − *y*)log (1 − *ŷ*))

We want the loss values to be as small as possible. For example we will
look at two cases

#### y=1

If *y* = 1 then the function above becomes
*L*(*ŷ*, *y*) =  − (1log *ŷ* + (1 − 1)log (1 − *ŷ*)) or
*L*(*ŷ*, *y*) =  − (1log *ŷ* + 0 \* log (1 − *ŷ*)). This leaves

*L*(*ŷ*, *y*) =  − log *ŷ*.

This means that we want *ŷ* to be as large as possible to give a small
loss value.

#### y=0

If *y* = 0 then the resulting function is

*L*(*ŷ*, *y*) =  − log (1 − *ŷ*).

In this circumstance we want *ŷ* to be as small as possible.

### Cost Function

$J(w,b)=\\frac{1}{m}\\sum{L(\\hat{y}^{i},y^{i})} = -\\frac{1}{m}\\sum{\[y^{i}\\log{(\\hat{y}^i)}+(1-y^{i})\\log{(1-\\hat{y}^{i})}\]}$

we are going to try to find values for *w* and *b* that minimize the
values of *J* in the function above.

### Gradient Descent Algarythm

The cost function described above, when plotted in 3D space for *w*,
*b*, and *J*, produces a convex surface. This means that it is fairly
simple to find a global minimum value for *J*. Some functions are
oscillatory and will have many local max and min, this is why we prefer
this function for logistic regression. What gradient descent does is we
take a random initialization to find a point of the surface created by
plotted J in 3D space and then we take a step in the direction of the
steepest gradient.

![](/Users/jolante/Bryan/ML_work/deep_learning.ai_coursera/Month1:%20Neural%20Networks%20and%20Deep%20Learning/convex_surface.png)

To do this we will repeat,

$w:=w-\\alpha \\frac{dJ(w)}{dw}$

where *α* is the learning rate and controls how big of a step we take in
every iteration. $\\frac{dJ(w)}{dw}$ is the size of change we want to
make to *w* with every iteration. In the coding we will represent this
with “*d**w*”. Remember that the definition of a derivative is the
slope, or finding the hypotenuse.

So for *J*(*w*, *b*) we update *w* with
$w:=w-\\alpha \\frac{\\partial J(w,b)}{\\partial w}$ and we update *b*
with $b:=b-\\alpha \\frac{\\partial J(w,b)}{\\partial b}$. Similary to
how we annotate $\\frac{\\partial J(w,b)}{\\partial w}$ as *d**w* in the
code, we will annotate $\\frac{\\partial J(w,b)}{\\partial b}$ as
*d**b*.

## The Computation Graph

The computation of a neural network is completed by the structure of a
forward pass, in which the outputs are calculated, and a backward pass,
in which we calculate gradients/derivatives. The computation graph is a
way to visualize the expansion of functions and the direction of passes
involved in applying those funciton. For example lets look at the
function *J*(*a*, *b*, *c*) = 3(*a* + *b**c*). The computation graph
would break up this function into three parts, *u* = *b**c*,
*v* = *a* + *u*, and *J* = 3*v*. We can then visualize these
relationships starting with the input vector of the values of *a*, *b*,
and *c* and passing from left to right until the final calculation of
*J*. This is the forward pass and it finds the outputs from functions.
The backward pass is the using this same visualization but calculating
the derivatives by moving from right to left.

![](/Users/jolante/Bryan/ML_work/deep_learning.ai_coursera/Month1:%20Neural%20Networks%20and%20Deep%20Learning/computation_graph.png)

### Derivatives with the Computation Graph

The backwards propagation or backwards pass takes the computational
graph and takes the derivative with respect to the object to the left.
For example we would start with the output *J* and perform the
derivative $\\frac{dJ}{dv}$. This is one backwards pass. This can be
expanded outward to deeper layers of backward propagation by way of the
chain rule. For example $\\frac{dJ}{da}$ is the chain rule of
$\\frac{dJ}{dv}\* \\frac{dv}{da}$.

#### Coding Note:

In this course, we will use a convention that *d**v**a**r* represents
the derivative of a final output variable with respect to various
intermediate quantities ($\\frac{dJ}{du}$ or $\\frac{dJ}{dv}$). \#\#
Logistic Regression in Gradient Descent Up to this point we have
introduced the functions:

*z* = *w*<sup>*T*</sup>*x* + *b* *ŷ* = *σ*(*z*) = *a*
*L*(*a*, *y*) =  − (*y*log (*a*) + (1 − *y*)log (1 − *a*))

If we have two inputs, or training examples, *x*<sub>1</sub> and
*x*<sub>2</sub>, from these we can make the computation graph of:

![](/Users/jolante/Bryan/ML_work/deep_learning.ai_coursera/Month1:%20Neural%20Networks%20and%20Deep%20Learning/regres_comp_graph.png)

Through application of the chain rule, we can now determine
$da=\\frac{\\partial L(a,y)}{\\partial a}$ which reduces to
$da=-\\frac{y}{a}+ \\frac{1-y}{1-a}$, and then
$dz=\\frac{\\partial L(a,y)}{\\partial z}= \\frac{\\partial L(a,y)}{\\partial a} \\frac{\\partial a}{\\partial z}$.
$\\frac{\\partial a}{\\partial z}=a(1-a)$ .If we then multiply
$\\frac{\\partial L(a,y)}{\\partial a} \\frac{\\partial a}{\\partial z}$
or $(-\\frac{y}{a}+ \\frac{1-y}{1-a}) \* a(1-a)$ we are left with
*d**z* = *a* − *y*.

So if we want to find $\\frac{ \\partial L(a,b)}{ \\partial w\_{i}}$ we
can express it in terms of *x*<sub>*i*</sub>∂*z* becuase
*x*<sub>*i*</sub> and *b* are considered a constant in respect to ∂*w*.
Similarly, $\\frac{ \\partial L(a,b)}{ \\partial b}$ yields ∂*z* becuase
*w*<sub>*i*</sub> and *x*<sub>*i*</sub> are considered constants in
regaurds to ∂*b*.

we can then plug in the value of *d**z* = *a* − *y* to
*x*<sub>*i*</sub>∂*z* and ∂*z* and use these to update the values of
*w*<sub>*i*</sub> and *b*:

$w:=w\_{i}-\\alpha \\frac{\\partial L(a,y)}{\\partial w\_{i}}$

$b:= \\frac{\\partial L(a,y)}{\\partial b}=dz$

This is just for a singular training set though. How do we apply this to
*m* amounts of training sets?

## Gradient Descent on m Examples

Practice in R. Not working yet.

``` r
# m=10
# n=2
# j=0
# dw=matrix(c(0,0),nrow = 1)
# db=matrix(c(0),nrow = 1)
# #the line below generates random values for a matrix
# # matrix(runif(n = m, -10, 10),nrow = m)
# 
# z=matrix(runif(n = m, 0, 10),nrow = m)
# a=matrix(runif(n = m, 0, 10),nrow = m)
# dz=matrix(runif(n = m, 0, 10),nrow = m)
# w=matrix(c(runif(n = m, 0, 10),runif(n = m, 0, 10)),nrow = m)
# x=matrix(c(runif(n = m, 0, 10),runif(n = m, 0, 10)),nrow = m)
# b=matrix(runif(n = m, 0, 10),nrow = m)
# sigma_z=matrix(c(2),nrow = m)
# y=matrix(runif(n = m, 0, 10),nrow = m)
# 
# 
# for (i in 1:m) {
#   z[i]=t(w[i,])%*%x[i,]+b[i]
#   a[i]=1/(1+exp(z[i]))
#   j=j-(y[i]*log(a[i])+(1-y[i])*log(1-a[i]))
#   dz[i]=a[i]-y[i]
#   for (j in 1:n) {
#     dw[i,j]=x[i,j]*dz[i]
#   }
#   db[i]=dz[i]
# }
# 
# j_prime = j/m
# dw_prime = dw/m
# db_prime = db/m
# alpha = 0
# 
# w_prime = matrix(c(rep(0,n*m)),nrow = m)
# b_prime = matrix(c(rep(0,m)),nrow = m)
# for (i in 1:m) {
#   for (j in 1:n) {
#       w_prime[i,j] = w[i,j]-alpha*dw[j]
#   }
#   b_prime[i] = b[i]-alpha*db
# }
```

## Vectorization in Python

Creating vectorization in Python is simple, this can be done with the
Numpy library.

``` python
import numpy as np

a = np.array([1,2,3,4])
print(a)
```

    ## [1 2 3 4]

Vectorization is much faster than using loops, this is demonstrated
below.

``` python
import time

a = np.random.rand(1000000)
b = np.random.rand(1000000)

tic = time.time()
c = np.dot(a,b)
toc = time.time()

print ("Vectorized version:" + str(1000*(toc-tic)) +"ms")
```

    ## Vectorized version:36.96084022521973ms

``` python
c = 0
tic = time.time()
for i in range(1000000):
  c += a[i]*b[i]
toc = time.time()

print("For loop:" + str(1000*(toc-tic)) +"ms")
```

    ## For loop:913.9449596405029ms

Vectorization makes parallel processing much more efficient. This is
relevant in that GPU’s are often looked at for parallelizing code, how
ever with vectorization, CPU’s are often efficient enough due to the
reduction in computation cost.

#### Note: Comparing R and Python

R has built in vectorization in its structure, this allows for similar
optimization to be performed, as shown below. I am not sure yet it his
rolls over to parallel processing but here are my timed outputs.

``` r
a <- runif(1000000,0,1)
b <- runif(1000000,0,1)

c = 0
tictoc::tic()
  for (i in 1:length(a)) {
    c = a[i]*b[i] + c
  }
tictoc::toc()
```

    ## 0.09 sec elapsed

``` r
c = 0
tictoc::tic()
c = sum(a * b)
tictoc::toc()
```

    ## 0.01 sec elapsed

## More Vectoriztion Examples

``` python
import math

v = np.random.rand(1000000)
n = 1000000

u = np.zeros((n,1))

tic = time.time()
for i in range(n):
  u[i]=math.exp(v[i])
toc = time.time()

print ("For loop version:" + str(1000*(toc-tic)) +"ms")
```

    ## For loop version:797.8250980377197ms

``` python
tic = time.time()
u =np.exp(v)
toc = time.time()

print ("Vectorized version:" + str(1000*(toc-tic)) +"ms")
```

    ## Vectorized version:43.576717376708984ms

## Vectorization of Logistic Regression

getting rid of the internal for loop. This is an incomplete example. I
will complete this later on as I learn Python more fully.

``` python
# j=0
# n=100
# dw=np.zeros(n,1)
# db=0
# m=100
# z=np.dot(w.T,x) + b
# x=0
# 
# for i in range(m):
#   z[i]= w.T*x+b
#   a[i]= 1/(1+np.exp(-z))
#   j+= -[y[i]*np.log(a[i])+(1-a[i])*np.log(1-a[i])]
#   dz[i]= a[i](1-a[i])
#   dw+= x[i]*dz[i]
#   db+=dz[i]
#   
# j = j/m
# dw /=m
# db=db/m
```

``` python
# j=0
# n=100
# dw=np.zeros(n,1)
# db=0
# m=100
# z=np.dot(w.T,x) + b
# x=0
# 
# Z=np.dot(w.T,x)+b 
# A=sigma(Z)
# dz=A-Y
# dw=1/m*X*dZ.T
# db=1/m*np.sum(dZ)
# w:=w-alpha*dw
# b:=b-alpha*db
# for i in range(m):
#   z[i]= w.T*x+b
#   a[i]= 1/(1+np.exp(-z))
#   j+= -[y[i]*np.log(a[i])+(1-a[i])*np.log(1-a[i])]
#   dz[i]= a[i](1-a[i])
#   dw+= x[i]*dz[i]
#   db+=dz[i]
#   
# j = j/m
# dw /=m
# db=db/m
```

``` python
import numpy as np

A = np.array([[56.00,0.0,4.4,68.0],
              [1.2,104.0,52.0,8.0],
              [1.8,135.0,99.0,0.9]])
              
cal = A.sum(axis=0) # axis means to sum vertically
print(cal)
```

    ## [ 59.  239.  155.4  76.9]

``` python
percentage = 100*A/cal.reshape(1,4)
print(percentage)
```

    ## [[94.91525424  0.          2.83140283 88.42652796]
    ##  [ 2.03389831 43.51464435 33.46203346 10.40312094]
    ##  [ 3.05084746 56.48535565 63.70656371  1.17035111]]

## Notes on Numpy Vectors in Python

``` python
import numpy as np

a = np.random.randn(5)

print(a.shape)
```

    ## (5,)

``` python
print(a.T)
```

    ## [-0.44648961 -0.72116464 -2.32504473  0.08100501 -0.45075261]

``` python
print(np.dot(a,a.T))
```

    ## 6.335004106518232

This should have output a matrix rather than a singular number, this is
an issue with the way python handles vectors. Avoid using any structure
the has the same shape as

``` python
print(a.shape)
```

    ## (5,)

instead if we use

``` python
a = np.random.randn(5,1)
print(a)
```

    ## [[-1.17433376]
    ##  [ 1.01139196]
    ##  [ 1.95442258]
    ##  [-0.24085924]
    ##  [-0.16063576]]

now if we print a.T we get a 1X5 matrix rather than a singular number

``` python
print(a.T)
```

    ## [[-1.17433376  1.01139196  1.95442258 -0.24085924 -0.16063576]]

``` python
print(np.dot(a,a.T))
```

    ## [[ 1.37905977 -1.18771172 -2.29514441  0.28284914  0.18864   ]
    ##  [-1.18771172  1.0229137   1.97668729 -0.2436031  -0.16246572]
    ##  [-2.29514441  1.97668729  3.81976761 -0.47074073 -0.31395016]
    ##  [ 0.28284914 -0.2436031  -0.47074073  0.05801317  0.03869061]
    ##  [ 0.18864    -0.16246572 -0.31395016  0.03869061  0.02580385]]

These oddly shaped arrays of (n,) are called rank 1 arrray’s and in
general try to avoid them as they can be buggy. shape them properly when
first formed or reshape them as necessary like we did with the case of
`cal.reshape(1,4)`.

## buliding a simple Logistic Regression Tool

#### Importing the packages needed to complete the code

``` python
# import numpy as np
# import copy
# import matplotlib.pyplot as plt
# import h5py
# import scipy
# from PIL import Image
# from scipy import ndimage
# from lr_utils import load_dataset # line not working outside of provided ipython notebook
# from public_tests import *  # line not working outside of provided ipython notebook
# 
# %matplotlib inline  # line not working outside of provided ipython notebook
# %load_ext autoreload  # line not working outside of provided ipython notebook
# %autoreload 2  # line not working outside of provided ipython notebook
```

``` python
def sigmoid(z):
    s=1/(1+np.exp(-z))
    return s
```

``` python
def initialize_with_zeros(dim):
    w=np.zeros([dim,1])
    b=0.0
    return w, b
```

``` python
w =  np.array([[1.], [2.]])
b = 2.
X =np.array([[1., 2., -1.], [3., 4., -3.2]])
Y = np.array([[1, 0, 1]])

def propagate(w, b, X, Y):
  
  m = X.shape[1]
  
  A = sigmoid(np.dot(w.T,X) + b )
  cost = -(1/m)*np.sum(Y*np.log(A) + (1-Y)*np.log(1-A), axis=1)
  # cost = -(1/m)*np.dot(Y,np.log(A), axis=1) + np.dot((1-Y),np.log(1-A), axis=1)
  
  dw =(1/m)*np.dot(X,(A-Y).T)
  db = (1/m)*(np.sum(A-Y))
  
  cost = np.squeeze(np.array(cost))
  
  grads = {"dw": dw,
  "db": db}
  return grads, cost

grads, cost = propagate(w, b, X, Y)
```

``` python
def optimize(w, b, X, Y, num_iterations=100, learning_rate=0.009, print_cost=False):
  
  w = copy.deepcopy(w)
  b = copy.deepcopy(b)
  
  costs = []
  
  for i in range(num_iterations):
    
    grads, cost = propagate(w, b, X, Y)
    
    dw = grads["dw"]
    db = grads["db"]
    
    w = w - learning_rate * dw
    b = b - learning_rate * db
    
    if i % 100 == 0:
      costs.append(cost)
      
      if print_cost:
        print ("Cost after iteration %i: %f" %(i, cost))
        
        params = {"w": w,
        "b": b}
        
        grads = {"dw": dw,
        "db": db}
        
        return params, grads, costs
```

``` python
def predict(w, b, X):
  m = X.shape[1]
  Y_prediction = np.zeros((1, m))
  w = w.reshape(X.shape[0], 1)
  
  A = sigmoid(np.dot(w.T,X)+b)
  
  for i in range(A.shape[1]):
    
    if A[0,i] > 0.5:
      Y_prediction[0,i] = 1
    else:
      Y_prediction[0,i] = 0
        
  return Y_prediction
```

``` python
def model(X_train, Y_train, X_test, Y_test, num_iterations=2000, learning_rate=0.5, print_cost=False):
  w, b = initialize_with_zeros(X_train.shape[0])
  parameters, grads, costs = optimize(w, b, X_train, Y_train, num_iterations, learning_rate, print_cost)
  w = parameters["w"]
  b = parameters["b"]
  Y_prediction_test = predict(w, b, X_test)
  Y_prediction_train = predict(w, b, X_train)
  if print_cost:
    print("train accuracy: {} %".format(100 - np.mean(np.abs(Y_prediction_train - Y_train)) * 100))
    print("test accuracy: {} %".format(100 - np.mean(np.abs(Y_prediction_test - Y_test)) * 100))
    d = {"costs": costs,
    "Y_prediction_test": Y_prediction_test, 
    "Y_prediction_train" : Y_prediction_train, 
    "w" : w, 
    "b" : b,
    "learning_rate" : learning_rate,
    "num_iterations": num_iterations}
    return d
```

``` python
# learning_rates = [0.01, 0.001, 0.0001]
# models = {}
# for lr in learning_rates:
#     print ("Training a model with learning rate: " + str(lr))
#     # models[str(lr)] = model(train_set_x, train_set_y, test_set_x, test_set_y, num_iterations=1500, learning_rate=lr, print_cost=False)
#     print ('\n' + "-------------------------------------------------------" + '\n')
# for lr in learning_rates:
#     plt.plot(np.squeeze(models[str(lr)]["costs"]), label=str(models[str(lr)]["learning_rate"]))
# plt.ylabel('cost')
# plt.xlabel('iterations (hundreds)')
# legend = plt.legend(loc='upper center', shadow=True)
# frame = legend.get_frame()
# frame.set_facecolor('0.90')
# plt.show()
```

``` python
# change this to the name of your image file
# my_image = "image.jpg"   
# 
# # We preprocess the image to fit your algorithm.
# fname = "images/" + my_image
# image = np.array(Image.open(fname).resize((num_px, num_px)))
# plt.imshow(image)
# image = image / 255.
# image = image.reshape((1, num_px * num_px * 3)).T
# my_predicted_image = predict(logistic_regression_model["w"], logistic_regression_model["b"], image)
# 
# print("y = " + str(np.squeeze(my_predicted_image)) + ", your algorithm predicts a \"" + classes[int(np.squeeze(my_predicted_image)),].decode("utf-8") +  "\" picture.")
```

## Deepening the network

In the former work, we used *x* values to calculate *σ**z* values, alos
called *a* values. These are found for a node in a singular layer. We
will now pass this to other iterations for new *a* values with added
layers in the model.

As we deepen each layer in the network, the inputs *x* in the equation
*n**p*.*d**o**t*(*w*.*T* \* *x*) + *b* is substituted with *a* from the
former layers output.

### Activation Functions

Thus far we have discussed the *s**i**g**m**o**i**d* function. We will
now introduce other alternatives like *t**a**n**h*, *R**e**L**U* or
*L**e**a**k**y**R**e**L**U*.

#### tanh

*g*(*z*) = tanh *z* therefor $g\`(z)= 1-(tanh(z)^{2}$ with a range of
+1:-1

#### ReLU

*g*(*z*) = *m**a**x*(0, *z*), so $g\`(z)=0 if z\<0$, $g\`(z)=1 if z\>0$
and $g\`(z)=undefined if z=0$

#### Leaky ReLU

*g*(*z*) = *m**a**x*(0.01*z*, *z*), so $g\`(z)=0.01 if z\<0$,
$g\`(z)=1 if z\>0$ and $g\`(z)=undefined if z=0$

### Gradient descent for Neural Networks

\#\#\#\#The first hidden layer

Forward propagation
*Z*<sub>1</sub> = *W*<sub>1</sub>*X* + *b*<sub>1</sub>
*A*<sub>1</sub> = *g*<sub>1</sub>(*Z*<sub>1</sub>)
*Z*<sub>2</sub> = *W*<sub>2</sub>*A*<sub>1</sub> + *b*<sub>2</sub>
*A*<sub>2</sub> = *g*<sub>2</sub>(*Z*<sub>2</sub>) = *s**i**g**m**a*(*Z*<sub>2</sub>)

Backward Propagation *d**z*<sub>2</sub> = *A*<sub>2</sub> − *Y*
$dw\_{2}= \\frac\_{1}{m}dz\_{2}A\_{1}.T$
$db\_{2}= \\frac\_{1}{m}.np.sum(dz\_{2}, amis=1, keepdims=True)$
*d**z*<sub>1</sub> = *W*<sub>2</sub>.*T**d**Z*<sub>2</sub> \* *g*<sub>1</sub>(*Z*<sub>1</sub>
$dW\_{1}= \\frac\_{1}{m}dz\_{1}X.T$
$dv\_{1}=\\frac\_{1}{m}np.sum(dZ\_{1}, amis=1, keepdims=True)$

## Building Our First Nueral Network With 1 Hidden Layer

#### Packages

``` python
# Package imports
# import numpy as np
# import matplotlib.pyplot as plt
# from testCases_v2 import *
# from public_tests import *
# import sklearn
# import sklearn.datasets
# import sklearn.linear_model
# from planar_utils import plot_decision_boundary, sigmoid, load_planar_dataset, load_extra_datasets
# 
# %matplotlib inline
# 
# np.random.seed(2) # set a seed so that the results are consistent
# 
# %load_ext autoreload
# %autoreload 2
```

#### Data

``` python
X = np.array([[ 1.20444229e+00,  1.58709904e-01,  9.52471960e-02,
         3.49178475e-01,  6.94150378e-01,  1.62065038e+00,
         1.53856225e+00,  3.63085641e-02,  4.74591109e-01,
         1.65695828e-01,  1.66446249e+00,  8.40285720e-01,
         2.61695163e-01,  2.31614896e-01,  1.58013020e+00,
         6.35509950e-03,  6.80610419e-01,  1.21400432e-01,
         1.13281261e+00,  1.61505892e+00,  1.66454441e-01,
         1.72438241e+00,  1.88667246e+00,  1.72327227e+00,
         1.54661332e+00,  9.84590400e-01,  1.45313345e+00,
         7.49043388e-01,  1.45048341e+00,  1.64287865e+00,
         1.28141487e+00,  1.59574104e+00,  1.46298294e+00,
         1.46629048e+00,  1.54348961e+00,  1.57013416e+00,
         1.22995404e+00,  1.31142345e+00, -1.99364553e+00,
         3.94564752e-01,  1.51715449e+00,  1.69169139e+00,
         1.74186686e+00, -2.91373382e+00,  7.52150898e-01,
         1.68537303e+00,  3.71160238e-01, -3.73033884e+00,
         3.52484080e-01, -1.48694206e+00, -7.45290416e-01,
         5.63807442e-01,  1.27093179e+00,  5.35133607e-01,
        -1.71330375e-01, -2.50197293e+00, -2.63275448e+00,
        -3.15561550e+00, -2.11022490e+00, -3.51303563e+00,
        -3.52018155e-02, -3.99843114e+00, -3.08095445e+00,
        -1.79023024e+00, -3.37520544e+00, -2.38578575e+00,
        -3.54741267e+00, -2.92344384e+00, -1.01009705e+00,
        -3.02185838e-01, -1.47947199e-01, -2.41113498e+00,
        -3.52065970e+00, -3.09762480e+00, -3.86453574e+00,
         3.46287902e-02, -3.81297733e+00, -2.54941164e+00,
        -3.61833546e+00, -2.31720968e+00, -3.85843301e+00,
        -3.77182528e+00, -3.22643424e+00, -2.49748107e+00,
        -2.94157976e+00, -2.91391859e+00, -3.70623964e+00,
        -1.95265709e+00, -2.66020434e+00,  8.17304141e-01,
         1.21768512e+00, -1.66246175e+00, -2.75111411e+00,
        -3.31873701e+00, -2.14063182e-01, -1.09902222e+00,
        -1.94767785e+00, -4.99991014e-01, -2.52618507e+00,
         1.96360773e+00, -1.02715843e+00,  3.03248341e+00,
         1.51559027e+00,  2.21864283e+00, -2.40385821e+00,
         2.00630521e+00,  3.17911628e+00, -1.45188351e+00,
         1.21496393e+00,  2.04613301e+00, -1.62182614e+00,
         3.17993200e+00,  3.96799188e+00,  3.29897728e-01,
         4.07816588e+00, -3.17566012e-01,  3.31129113e+00,
        -9.52809386e-01,  3.03768998e+00,  3.55052992e+00,
         3.55990775e+00,  2.33663285e+00,  2.31352433e+00,
         2.79495212e-01,  1.51862547e-01,  1.96155941e+00,
         5.47353260e-01,  3.49044034e+00,  2.72566042e+00,
         2.13606508e+00,  3.67042248e+00,  3.92260947e+00,
         3.08862031e+00,  1.92948433e+00,  1.38946995e+00,
         3.51428218e+00,  1.13035696e+00,  3.33545460e+00,
         9.15905230e-03,  1.68097254e+00,  2.30710971e+00,
         2.21404832e+00, -5.31842726e-01, -3.45874829e-01,
        -9.83826601e-02,  6.55029428e-01,  5.83612433e-01,
        -7.68172854e-01, -5.52725703e-02, -1.25961956e+00,
         9.42805936e-01,  3.77681531e+00, -1.70922458e+00,
        -1.08169517e+00, -1.40307039e+00, -6.43634519e-01,
        -6.32543454e-01, -8.53713550e-01, -1.22772526e+00,
         9.50740231e-01, -5.97375125e-01,  5.01933176e-01,
        -1.88988467e+00, -1.23602220e+00, -1.74861600e+00,
        -1.48828262e+00, -1.27810421e+00, -1.99338361e+00,
        -6.80758371e-02,  1.07968241e+00, -1.49733014e+00,
        -1.02248497e+00, -6.88068627e-02, -1.77284297e+00,
        -1.42903599e+00, -1.63191608e+00, -1.57413453e-01,
        -1.69732959e+00, -7.20691107e-01, -1.49836618e+00,
        -1.53285135e+00, -7.83079289e-01, -5.65545945e-01,
        -4.28247557e-03, -9.54816172e-01, -1.43027299e+00,
        -1.99139039e-01, -3.93228458e-01, -3.13917579e-01,
        -5.55629383e-01, -5.61729204e-02, -5.84257606e-02,
        -8.07037000e-01, -1.86016879e-01, -9.65590226e-03,
        -1.52664558e+00, -2.32543323e-02, -2.11665124e-02,
        -2.37912887e-01, -5.85846410e-01, -1.07049581e+00,
        -2.32905994e-03, -9.27980849e-02, -9.24081181e-01,
        -4.27415411e-02, -7.88306156e-02, -1.35471230e+00,
        -9.64506801e-02, -4.88903965e-01, -1.40428145e+00,
        -1.03032194e-01, -1.76823043e+00, -1.81435331e+00,
        -4.03453231e-01, -1.13007684e+00, -3.30143778e-01,
        -1.03592948e-01, -1.62823990e+00, -1.27072988e+00,
        -3.24165041e-01, -8.31515613e-01, -1.15541550e+00,
        -8.81319474e-01, -5.14135967e-01, -3.41907953e-01,
        -1.50008474e+00, -1.09546543e+00, -1.71840632e+00,
        -1.68448953e+00, -1.23608462e+00, -8.06782295e-01,
        -1.06026703e+00, -1.25449646e+00, -1.71040125e+00,
        -4.54245598e-01, -1.78717499e+00, -1.26536725e+00,
        -2.93821146e-01, -4.04645730e-01,  2.03128629e+00,
        -1.02564465e+00, -1.07882287e+00, -1.48728331e+00,
         1.52973464e+00, -1.39893030e+00, -3.67322611e-01,
        -1.53385549e+00,  1.75371064e+00, -1.15897743e+00,
         2.94968569e+00, -1.38165997e+00,  2.41200118e+00,
         1.48331333e+00, -1.29300070e+00,  3.02838862e+00,
        -1.14374794e+00, -1.06870636e+00,  2.70728746e+00,
        -2.29022863e-01,  1.80385877e-01,  7.87999977e-01,
        -1.65291432e-01,  3.73979818e+00,  3.48775653e+00,
         2.08377776e+00,  2.36553487e+00,  3.47039369e+00,
         2.42914859e+00,  3.90210781e+00,  2.03812322e+00,
         4.04388429e+00,  3.15341019e+00,  3.09441731e+00,
         1.00077505e+00,  3.22109077e+00,  3.03895157e+00,
        -1.10566174e-01,  1.45338553e+00,  3.42704770e+00,
         3.16377425e+00,  3.06435279e+00,  2.97153034e+00,
         3.66817242e+00,  1.81576373e+00, -6.52056092e-01,
         2.39565436e+00, -1.83495474e-02, -1.85426248e+00,
        -7.55084589e-02,  3.68327360e+00,  3.01854545e+00,
         6.67169560e-01,  3.21959504e+00,  6.71420203e-01,
         3.62660798e+00,  3.41692475e+00, -4.99555802e-01,
         2.82564692e+00,  3.67261725e+00,  7.88007167e-01,
         3.86155325e+00, -2.85587710e-01,  2.28420287e+00,
         2.81396650e-01,  3.13503544e-01, -1.43368358e+00,
         9.57195160e-01, -1.85436896e+00,  3.81805817e-01,
        -2.78967630e+00, -3.28548806e+00, -2.22349866e+00,
        -2.78061591e+00,  8.26303188e-01, -3.72885115e+00,
        -1.23292968e+00, -9.58935652e-01, -3.85811062e+00,
        -3.59301074e+00, -3.19689248e+00, -2.12766839e+00,
        -2.96424870e+00, -3.60274154e+00, -1.17472896e+00,
        -2.58752094e+00, -3.16293962e+00, -1.72757867e+00,
        -3.45341213e+00, -2.81273166e+00, -1.03553265e+00,
        -3.15675590e-01, -3.70141041e+00, -4.21189811e+00,
        -2.46094977e+00, -3.43569797e+00, -3.34469058e+00,
        -1.38193635e+00, -3.17949403e+00, -2.42119633e+00,
        -1.33643509e+00, -3.06299272e+00, -3.56504553e+00,
        -3.34451075e+00,  7.77655270e-01, -2.98030710e+00,
        -3.55225078e+00, -1.82974404e+00,  1.46532816e+00,
         1.56688198e+00, -2.96543364e+00, -9.60755845e-01,
        -3.37890943e+00,  1.47135345e+00, -3.02829566e+00,
         7.50982262e-03,  1.79730919e+00,  3.91077683e-01,
         1.63881069e+00, -8.84278418e-01,  1.56546135e+00,
         1.59529239e+00,  1.27139537e+00,  1.40701977e+00,
         1.58125802e+00, -1.72159327e+00,  5.68722722e-01,
         1.60960641e+00,  1.70849761e+00,  3.77494361e-02,
         1.77885147e+00,  1.57017560e+00,  1.31626221e+00,
         1.45580659e+00,  9.95670227e-02,  1.49806312e+00,
         1.19805866e+00,  1.30759534e+00,  1.69066279e+00,
         1.54457758e+00,  3.36164302e-01,  4.71520896e-01,
         1.79332637e+00,  1.75802645e+00,  1.11059510e+00,
         2.25410298e-02,  1.03402966e+00,  1.46010385e-01,
         1.50301824e+00,  5.29563078e-01,  7.23526086e-01,
         1.24444151e+00,  7.11308695e-01,  3.65824427e-02,
         5.96719268e-01,  1.77160218e-01,  4.50367941e-01,
         3.75726453e-02,  2.13641164e-01,  3.24386893e-01,
         7.30780825e-01],
       [ 3.57611415e+00, -1.48217090e+00, -1.27995533e+00,
        -2.06437997e+00,  2.88910878e+00, -4.03512400e+00,
         3.24255518e+00, -8.53952855e-01,  2.47793820e+00,
         1.81117216e+00,  3.45270042e+00, -3.44017638e+00,
         2.10549647e+00,  1.81380501e+00,  3.30267114e+00,
         4.18131769e-01,  3.09636434e+00,  1.33096004e+00,
         3.78708833e+00,  3.67113005e+00,  1.77614048e+00,
         2.76152023e+00,  3.25544108e+00,  3.46859174e+00,
         2.48644177e+00,  3.77377130e+00,  3.60609974e+00,
         3.11254032e+00,  3.57519059e+00,  2.61610253e+00,
         3.71658382e+00,  3.70508842e+00,  3.83692688e+00,
         4.03764347e+00,  3.66278004e+00,  2.58286653e+00,
         3.46644213e+00,  1.81009586e+00, -1.49385117e+00,
         4.15245354e-01,  2.27195929e+00,  3.36955063e+00,
         3.12091874e+00, -1.81920100e+00,  8.92965442e-01,
         2.58295944e+00,  3.90987436e-01, -1.63847358e+00,
         3.58745594e-01, -1.20025645e+00, -6.62747682e-01,
         6.31091667e-01,  1.91148705e+00,  5.61807626e-01,
        -1.64789940e-01, -1.67941725e+00, -1.52497835e+00,
        -1.68684842e+00, -1.45714301e+00, -1.77895038e+00,
        -3.48915965e-02, -1.52257926e+00, -1.66776383e+00,
        -1.34036665e+00, -1.71311210e+00, -1.51851549e+00,
        -1.13514717e+00, -6.42637843e-01, -6.83366271e-02,
        -2.92017074e-01, -1.41218116e-01, -1.50670574e+00,
        -1.52995707e+00, -7.94385720e-01, -1.39958852e+00,
         3.56093081e-02, -1.79730878e+00, -5.11392884e-01,
        -1.12667182e+00, -4.21185479e-01, -1.45669050e+00,
        -1.33847132e+00, -8.16479132e-01, -4.73790481e-01,
        -6.39753507e-01, -6.34286374e-01, -1.38061641e+00,
        -2.57794858e-01, -4.47701819e-01, -4.12904131e-02,
        -9.76752768e-02, -1.78618154e-01, -5.67541671e-01,
        -8.13740301e-01, -2.63923558e-03, -7.25097028e-02,
        -2.63437687e-01, -2.06478223e-02, -4.03305715e-01,
        -2.38704899e-01, -9.51626957e-02, -7.98991813e-01,
        -1.66006988e-01, -3.64406026e-01, -3.86024370e-01,
        -2.20189574e-01, -7.75370339e-01, -1.22170582e-01,
        -8.42420756e-02, -2.98099361e-01, -1.96869702e-01,
        -7.53075017e-01, -1.46831842e+00, -9.55610600e-03,
        -1.20211122e+00, -9.60546773e-03, -8.10882920e-01,
        -5.68346353e-02, -1.67347742e+00, -1.40460974e+00,
        -1.12343152e+00, -4.04122779e-01, -1.57164755e+00,
        -2.60933944e-01, -2.62404061e-04, -1.44902115e+00,
        -4.90044260e-01, -1.85263115e+00, -5.41699195e-01,
        -1.53914207e+00, -1.68871377e+00, -1.48674074e+00,
        -7.97674459e-01, -1.39073589e+00, -1.14367531e+00,
        -1.57393834e+00, -8.87426686e-01, -1.21102011e+00,
        -8.58734620e-03, -1.19452167e+00, -1.53428360e+00,
        -1.57898433e+00,  5.80732218e-01,  3.71643284e-01,
         9.98470127e-02, -5.86327973e-01, -5.26306388e-01,
         8.87962668e-01,  5.64689692e-02,  1.59318262e+00,
        -8.25136845e-01, -1.20845750e+00,  2.78336582e+00,
         3.43105607e+00,  1.88901133e+00,  7.17082698e-01,
         7.16776955e-01,  1.00558709e+00,  1.57888938e+00,
        -7.97008734e-01,  6.58892564e-01, -4.71381742e-01,
         3.06914837e+00,  1.64172117e+00,  3.43138378e+00,
         2.22472260e+00,  3.59356305e+00,  3.82338186e+00,
         9.78172883e-01, -9.08628545e-01,  2.16232645e+00,
         3.55191556e+00, -1.13111936e+00,  3.41399524e+00,
         3.33509092e+00,  3.43837535e+00,  1.35204653e+00,
         3.76053347e+00,  3.30301665e+00,  3.55267433e+00,
         2.37056808e+00,  3.18484540e+00,  2.98100486e+00,
         3.10265302e-01,  3.33480741e+00,  3.74789034e+00,
         1.75698786e+00,  2.41233915e+00,  2.26160198e+00,
         2.84308753e+00, -8.08464256e-01,  7.97601218e-01,
        -3.16602301e+00, -1.73923341e+00, -3.03300460e-01,
         3.51448929e+00, -4.48211450e-01, -6.80222539e-01,
        -1.89461587e+00, -3.09125994e+00,  3.68252461e+00,
        -2.48775165e-01, -1.11264583e+00, -3.31287149e+00,
         9.83064901e-01, -1.06200552e+00,  3.64306307e+00,
         1.23598454e+00, -2.48635569e+00, -3.96546091e+00,
        -1.30407633e+00, -2.80700115e+00, -3.17916299e+00,
        -2.34899616e+00,  3.86016046e+00,  2.31046192e+00,
        -1.40325115e+00, -3.22048585e+00, -3.80050197e+00,
         2.25857585e+00, -3.30857362e+00, -1.58359847e+00,
        -3.47513222e+00, -2.68758875e+00, -3.55711876e-01,
        -3.74431790e+00, -3.66810701e+00, -3.36824403e+00,
        -3.76359366e+00, -3.41537451e+00, -9.76623415e-01,
        -1.29100568e+00, -1.65608598e+00, -3.32409572e+00,
        -4.96630821e-01, -3.14165698e+00, -1.71322197e+00,
        -3.05909111e-01, -4.46777005e-01,  1.45931378e+00,
        -1.22812034e+00, -3.69037212e+00, -3.47057694e+00,
         1.15856433e+00, -2.03606086e+00, -3.85511111e-01,
        -2.62194406e+00,  1.34798304e+00, -1.51195709e+00,
         1.73829518e+00, -1.79547235e+00,  1.54317462e+00,
         1.10954960e+00, -1.64931941e+00,  1.75797432e+00,
        -1.47251063e+00, -1.28986402e+00,  1.67620531e+00,
        -2.39669870e-01,  1.80650988e-01,  6.91778881e-01,
        -1.82942022e-01,  1.63134904e+00,  1.82884977e+00,
         1.43428651e+00,  1.60094812e+00,  1.75230668e+00,
         4.64326857e-01,  1.43351617e+00,  1.43138441e+00,
         1.42782215e+00,  1.73492674e+00,  1.70395009e+00,
         7.47685370e-02,  1.82710034e+00,  1.70923867e+00,
        -1.11868340e-01,  1.14112185e+00,  9.82679506e-01,
         1.69883486e+00,  7.33791653e-01,  1.80316095e+00,
         1.81840953e+00,  2.85892703e-01,  1.09388890e-02,
         3.40608544e-01, -8.22267713e-05,  2.09582869e-01,
        -3.52827719e-04,  1.47413722e+00,  6.19443662e-01,
         2.64445438e-02,  7.63859684e-01,  2.95005667e-02,
         1.68281773e+00,  9.40761147e-01,  9.24913182e-03,
         1.61411623e+00,  1.04594113e+00,  3.45145694e-02,
         1.49077794e+00,  7.29051825e-04,  3.52475534e-01,
         1.00877527e-02,  4.27419191e-03,  1.31839708e-01,
         4.14122283e-02,  1.91933665e-01,  7.39039595e-03,
         6.46626879e-01,  1.67303451e+00,  4.21369487e-01,
         5.56612529e-01,  4.42761985e-02,  1.29232912e+00,
         7.28430003e-02,  6.19710137e-02,  1.42351448e+00,
         1.08735728e+00,  1.72484190e+00,  1.38667445e+00,
         5.90891086e-01,  1.29900039e+00,  9.65871130e-01,
         1.56881569e+00,  1.76930017e+00,  1.29792183e+00,
         9.84995600e-01,  1.69277814e+00,  8.93630841e-01,
         3.06091086e-01,  1.14596763e+00,  1.91549748e+00,
         1.57524026e+00,  1.66266665e+00,  1.59850516e+00,
         1.10993370e+00,  1.67500495e+00,  5.25296992e-01,
         1.03566638e+00,  1.74435925e+00,  1.81971221e+00,
         1.11128726e+00, -8.89501086e-01,  6.66069451e-01,
         1.26100476e+00,  1.36655852e+00, -2.08468516e+00,
        -2.38718334e+00,  1.66924896e+00,  8.49203268e-01,
         1.72895893e+00, -2.25643833e+00,  1.69824962e+00,
        -7.63165462e-03, -3.09272026e+00, -4.09952810e-01,
        -3.79499252e+00,  7.61286324e-01, -2.30558737e+00,
        -3.03467556e+00, -1.61502125e+00, -3.90266394e+00,
        -2.90841129e+00,  1.31250839e+00,  2.59847860e+00,
        -3.50186707e+00, -2.96039129e+00, -3.60098795e-02,
        -3.27807629e+00, -2.34587605e+00, -3.56919962e+00,
        -3.51991588e+00, -1.23848616e+00, -3.47814659e+00,
        -3.65480564e+00, -3.82629239e+00, -3.80826364e+00,
        -3.56242008e+00, -2.44646389e+00, -2.51865217e+00,
        -3.70032292e+00, -2.98399234e+00, -3.49686963e+00,
        -4.88897726e-01, -3.76272064e+00,  1.42253750e+00,
        -3.69488964e+00, -2.79391432e+00, -8.53390853e-01,
        -3.97467897e+00, -3.03195393e+00, -7.20267166e-01,
         2.87171160e+00, -1.53928239e+00,  2.40812143e+00,
         8.91600979e-01,  1.73201591e+00, -2.11632466e+00,
         3.06832921e+00]])
         
Y = np.array([[0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1]])
        #, dtype=uint8)
```

#### Visualizing the Data

Need to figure out how to place matpltlib images inline in Rmarkdown

``` python
# Visualize the data:
# plt.scatter(X[0, :], X[1, :], c=Y, s=40, cmap=plt.cm.Spectral);
```

#### Sizing up the Data

``` python
# (≈ 3 lines of code)
# shape_X = ...
# shape_Y = ...
# training set size
# m = ...
# YOUR CODE STARTS HERE
shape_X = X.shape
shape_Y = Y.shape
m = X.shape[1]

# YOUR CODE ENDS HERE

print ('The shape of X is: ' + str(shape_X))
```

    ## The shape of X is: (2, 400)

``` python
print ('The shape of Y is: ' + str(shape_Y))
```

    ## The shape of Y is: (1, 400)

``` python
print ('I have m = %d training examples!' % (m))
```

    ## I have m = 400 training examples!

#### Testing Logistic Regression Perfomrance with SKLearn

``` python
# Train the logistic regression classifier
# clf = sklearn.linear_model.LogisticRegressionCV();
# clf.fit(X.T, Y.T);
# 
# # Plot the decision boundary for logistic regression
# plot_decision_boundary(lambda x: clf.predict(x), X, Y)
# plt.title("Logistic Regression")
# 
# # Print accuracy
# LR_predictions = clf.predict(X.T)
# print ('Accuracy of logistic regression: %d ' % float((np.dot(Y,LR_predictions) + np.dot(1-Y,1-LR_predictions))/float(Y.size)*100) +
#        '% ' + "(percentage of correctly labelled datapoints)")
```

Accuracy of logistic regression: 47 % (percentage of correctly labelled
datapoints)

Interpretation: The dataset is not linearly separable, so logistic
regression doesn’t perform well. Hopefully a neural network will do
better. Let’s try this now!

Reminder: The general methodology to build a Neural Network is to:

1.  Define the neural network structure ( \# of input units, \# of
    hidden units, etc).
2.  Initialize the model’s parameters
3.  Loop:
    -   Implement forward propagation
    -   Compute loss
    -   Implement backward propagation to get the gradients
    -   Update parameters (gradient descent) In practice, you’ll often
        build helper functions to compute steps 1-3, then merge them
        into one function called nn_model(). Once you’ve built
        nn_model() and learned the right parameters, you can make
        predictions on new data.

#### Archetecture Function

``` python
def layer_sizes(X, Y):
    """
    Arguments:
    X -- input dataset of shape (input size, number of examples)
    Y -- labels of shape (output size, number of examples)
    Returns:
    n_x -- the size of the input layer
    n_h -- the size of the hidden layer
    n_y -- the size of the output layer
    """
    #(≈ 3 lines of code)
    # n_x = ... 
    # n_h = ...
    # n_y = ... 
    # YOUR CODE STARTS HERE
    n_x = X.shape[0]
    n_h = 4
    n_y = Y.shape[0]
    # YOUR CODE ENDS HERE
    return (n_x, n_h, n_y)
```

#### Initializing the Parameters W and b

``` python
# GRADED FUNCTION: initialize_parameters

def initialize_parameters(n_x, n_h, n_y):
    """
    Argument:
    n_x -- size of the input layer
    n_h -- size of the hidden layer
    n_y -- size of the output layer
    Returns:
    params -- python dictionary containing your parameters:
                    W1 -- weight matrix of shape (n_h, n_x)
                    b1 -- bias vector of shape (n_h, 1)
                    W2 -- weight matrix of shape (n_y, n_h)
                    b2 -- bias vector of shape (n_y, 1)
    """
    #(≈ 4 lines of code)
    # W1 = ...
    # b1 = ...
    # W2 = ...
    # b2 = ...
    # YOUR CODE STARTS HERE
    W1 = np.random.randn(n_h,n_x) * 0.01
    b1 = np.zeros((n_h,n_y))
    W2 = np.random.randn(n_y,n_h) * 0.01
    b2 = np.zeros((n_y,n_y))
    # YOUR CODE ENDS HERE
    parameters = {"W1": W1,
                  "b1": b1,
                  "W2": W2,
                  "b2": b2}
    return parameters
```

#### Forward Propigation

Implement `forward_propagation()` using the following equations:

$$Z^{\[1\]} =  W^{\[1\]} X + b^{\[1\]}\\tag{1}$$
$$A^{\[1\]} = \\tanh(Z^{\[1\]})\\tag{2}$$
$$Z^{\[2\]} = W^{\[2\]} A^{\[1\]} + b^{\[2\]}\\tag{3}$$
$$\\hat{Y} = A^{\[2\]} = \\sigma(Z^{\[2\]})\\tag{4}$$

**Instructions**:

-   Check the mathematical representation of your classifier in the
    figure above.
-   Use the function `sigmoid()`. It’s built into (imported) this
    notebook.
-   Use the function `np.tanh()`. It’s part of the numpy library.
-   Implement using these steps:
    1.  Retrieve each parameter from the dictionary “parameters” (which
        is the output of `initialize_parameters()` by using
        `parameters[".."]`.
    2.  Implement Forward Propagation. Compute
        *Z*<sup>\[1\]</sup>, *A*<sup>\[1\]</sup>, *Z*<sup>\[2\]</sup>
        and *A*<sup>\[2\]</sup> (the vector of all your predictions on
        all the examples in the training set).
-   Values needed in the backpropagation are stored in “cache”. The
    cache will be given as an input to the backpropagation function.

``` python
# GRADED FUNCTION:forward_propagation

def forward_propagation(X, parameters):
    """
    Argument:
    X -- input data of size (n_x, m)
    parameters -- python dictionary containing your parameters (output of initialization function)
    
    Returns:
    A2 -- The sigmoid output of the second activation
    cache -- a dictionary containing "Z1", "A1", "Z2" and "A2"
    """
    # Retrieve each parameter from the dictionary "parameters"
    #(≈ 4 lines of code)
    # W1 = ...
    # b1 = ...
    # W2 = ...
    # b2 = ...
    # YOUR CODE STARTS HERE
    W1 = parameters["W1"]
    b1 = parameters["b1"]
    
    W2 = parameters["W2"]
    b2 = parameters["b2"]
    # YOUR CODE ENDS HERE
    
    # Implement Forward Propagation to calculate A2 (probabilities)
    # (≈ 4 lines of code)
    # Z1 = ...
    # A1 = ...
    # Z2 = ...
    # A2 = ...
    # YOUR CODE STARTS HERE
    Z1 = np.dot(W1,X) + b1
    A1 = np.tanh(Z1)
    Z2 = np.dot(W2,A1) + b2
    A2 = sigmoid(Z2)
    # YOUR CODE ENDS HERE
    
    assert(A2.shape == (1, X.shape[1]))
    
    cache = {"Z1": Z1,
             "A1": A1,
             "Z2": Z2,
             "A2": A2}
    
    return A2, cache
```

Now that you’ve computed *A*<sup>\[2\]</sup> (in the Python variable
“`A2`”), which contains *a*<sup>\[2\](*i*)</sup> for all examples, you
can compute the cost function as follows:

$$J = - \\frac{1}{m} \\sum\\limits\_{i = 1}^{m} \\large{(} \\small y^{(i)}\\log\\left(a^{\[2\] (i)}\\right) + (1-y^{(i)})\\log\\left(1- a^{\[2\] (i)}\\right) \\large{)} \\small\\tag{13}$$

<a name='ex-5'></a>

Implement `compute_cost()` to compute the value of the cost *J*.

**Instructions**: - There are many ways to implement the cross-entropy
loss. This is one way to implement one part of the equation without for
loops: $- \\sum\\limits\_{i=1}^{m} y^{(i)}\\log(a^{\[2\](i)})$:

``` python
logprobs = np.multiply(np.log(A2),Y)
cost = - np.sum(logprobs)          
```

-   Use that to build the whole expression of the cost function.

**Notes**:

-   You can use either `np.multiply()` and then `np.sum()` or directly
    `np.dot()`).  
-   If you use `np.multiply` followed by `np.sum` the end result will be
    a type `float`, whereas if you use `np.dot`, the result will be a 2D
    numpy array.  
-   You can use `np.squeeze()` to remove redundant dimensions (in the
    case of single float, this will be reduced to a zero-dimension
    array).
-   You can also cast the array as a type `float` using `float()`.

#### Cost Function

``` python
# GRADED FUNCTION: compute_cost

def compute_cost(A2, Y):
    """
    Computes the cross-entropy cost given in equation (13)
    Arguments:
    A2 -- The sigmoid output of the second activation, of shape (1, number of examples)
    Y -- "true" labels vector of shape (1, number of examples
    Returns:
    cost -- cross-entropy cost given equation (13)
    
    """
    m = Y.shape[1] # number of example
    # Compute the cross-entropy cost
    # (≈ 2 lines of code)
    # logprobs = ...
    # cost = ...
    # YOUR CODE STARTS HERE
    logprobs = np.multiply(np.log(A2),Y) + np.multiply(np.log((1-A2)),(1-Y))
    cost = -(1/m) * np.sum(logprobs)
    # YOUR CODE ENDS HERE
    cost = float(np.squeeze(cost))  # makes sure cost is the dimension we expect. 
    return cost
```

## Backwards Propigation

``` python
# GRADED FUNCTION: backward_propagation

def backward_propagation(parameters, cache, X, Y):
    """
    Implement the backward propagation using the instructions above.
    Arguments:
    parameters -- python dictionary containing our parameters 
    cache -- a dictionary containing "Z1", "A1", "Z2" and "A2".
    X -- input data of shape (2, number of examples)
    Y -- "true" labels vector of shape (1, number of examples)
    Returns:
    grads -- python dictionary containing your gradients with respect to different parameters
    """
    m = X.shape[1]
    # First, retrieve W1 and W2 from the dictionary "parameters".
    #(≈ 2 lines of code)
    # W1 = ...
    # W2 = ...
    # YOUR CODE STARTS HERE
    W1 = parameters["W1"]
    W2 = parameters["W2"]
    # YOUR CODE ENDS HERE
    # Retrieve also A1 and A2 from dictionary "cache".
    #(≈ 2 lines of code)
    # A1 = ...
    # A2 = ...
    # YOUR CODE STARTS HERE
    A1 = cache["A1"]
    A2 = cache["A2"]
    # YOUR CODE ENDS HERE
    # Backward propagation: calculate dW1, db1, dW2, db2. 
    #(≈ 6 lines of code, corresponding to 6 equations on slide above)
    # dZ2 = ...
    # dW2 = ...
    # db2 = ...
    # dZ1 = ...
    # dW1 = ...
    # db1 = ...
    # YOUR CODE STARTS HERE
    dZ2 = A2 - Y
    dW2 = 1/m * np.dot(dZ2, A1.T )
    db2 = 1/m * np.sum(dZ2, axis = 1, keepdims = True)
    dZ1 = W2.T * dZ2 * (1 - np.power(A1, 2))
    dW1 = 1/m * np.dot(dZ1,X.T)
    db1 = 1/m * np.sum(dZ1, axis = 1, keepdims = True)
    # YOUR CODE ENDS HERE
    grads = {"dW1": dW1,
             "db1": db1,
             "dW2": dW2,
             "db2": db2}
    return grads
```

## Updating Parameters

Implement the update rule. Use gradient descent. You have to use (dW1,
db1, dW2, db2) in order to update (W1, b1, W2, b2).

**General gradient descent rule**:
$\\theta = \\theta - \\alpha \\frac{\\partial J }{ \\partial \\theta }$
where *α* is the learning rate and *θ* represents a parameter.

``` python
# GRADED FUNCTION: update_parameters
def update_parameters(parameters, grads, learning_rate = 1.2):
    """
    Updates parameters using the gradient descent update rule given above
    Arguments:
    parameters -- python dictionary containing your parameters 
    grads -- python dictionary containing your gradients 
    Returns:
    parameters -- python dictionary containing your updated parameters 
    """
    # Retrieve each parameter from the dictionary "parameters"
    #(≈ 4 lines of code)
    # W1 = ...
    # b1 = ...
    # W2 = ...
    # b2 = ...
    # YOUR CODE STARTS HERE
    W1 = parameters["W1"]
    b1 = parameters["b1"]
    W2 = parameters["W2"]
    b2 = parameters["b2"]
    # YOUR CODE ENDS HERE
    # Retrieve each gradient from the dictionary "grads"
    #(≈ 4 lines of code)
    # dW1 = ...
    # db1 = ...
    # dW2 = ...
    # db2 = ...
    # YOUR CODE STARTS HERE
    dW1 = grads["dW1"]
    db1 = grads["db1"]
    dW2 = grads["dW2"]
    db2 = grads["db2"]
    # YOUR CODE ENDS HERE
    # Update rule for each parameter
    #(≈ 4 lines of code)
    # W1 = ...
    # b1 = ...
    # W2 = ...
    # b2 = ...
    # YOUR CODE STARTS HERE
    W1 = W1 - learning_rate * dW1
    b1 = b1 - learning_rate * db1
    W2 = W2 - learning_rate * dW2
    b2 = b2 - learning_rate * db2
    # YOUR CODE ENDS HERE
    parameters = {"W1": W1,
                  "b1": b1,
                  "W2": W2,
                  "b2": b2}
    return parameters
```

## Intigration into a Neural Network: nn_model()

Now there is just the issue of ordering the functions in the proper
arrangement.

``` python
# GRADED FUNCTION: nn_model
def nn_model(X, Y, n_h, num_iterations = 10000, print_cost=False):
    """
    Arguments:
    X -- dataset of shape (2, number of examples)
    Y -- labels of shape (1, number of examples)
    n_h -- size of the hidden layer
    num_iterations -- Number of iterations in gradient descent loop
    print_cost -- if True, print the cost every 1000 iterations
    Returns:
    parameters -- parameters learnt by the model. They can then be used to predict.
    """
    np.random.seed(3)
    n_x = layer_sizes(X, Y)[0]
    n_y = layer_sizes(X, Y)[2]
    
    # Initialize parameters
    #(≈ 1 line of code)
    # parameters = ...
    # YOUR CODE STARTS HERE
    parameters = initialize_parameters(n_x, n_h, n_y)
    # YOUR CODE ENDS HERE
    # Loop (gradient descent)
    for i in range(0, num_iterations):
        #(≈ 4 lines of code)
        # Forward propagation. Inputs: "X, parameters". Outputs: "A2, cache".
        # A2, cache = ...
        # Cost function. Inputs: "A2, Y". Outputs: "cost".
        # cost = ..
        # Backpropagation. Inputs: "parameters, cache, X, Y". Outputs: "grads".
        # grads = ..
        # Gradient descent parameter update. Inputs: "parameters, grads". Outputs: "parameters".
        # parameters = ...
        # YOUR CODE STARTS HERE
        A2, cache = forward_propagation(X, parameters)
        cost = compute_cost(A2, Y)
        grads = backward_propagation(parameters, cache, X, Y)
        parameters = update_parameters(parameters, grads)
        # YOUR CODE ENDS HERE
        # Print the cost every 1000 iterations
        if print_cost and i % 1000 == 0:
            print ("Cost after iteration %i: %f" %(i, cost))
    return parameters
```

## Using the Neural Network for Predictions

Predict with your model by building `predict()`. Use forward propagation
to predict results.

**Reminder**: predictions =
$y\_{prediction} = \\mathbb 1 \\text{{activation \> 0.5}} = \\begin{cases}  1 & \\text{if}\\ activation \> 0.5 \\\\  0 & \\text{otherwise}  \\end{cases}$

As an example, if you would like to set the entries of a matrix X to 0
and 1 based on a threshold you would do: `X_new = (X > threshold)`

``` python
# GRADED FUNCTION: predict
def predict(parameters, X):
    """
    Using the learned parameters, predicts a class for each example in X
    Arguments:
    parameters -- python dictionary containing your parameters 
    X -- input data of size (n_x, m)
    Returns
    predictions -- vector of predictions of our model (red: 0 / blue: 1)
    """
    # Computes probabilities using forward propagation, and classifies to 0/1 using 0.5 as the threshold.
    #(≈ 2 lines of code)
    # A2, cache = ...
    # predictions = ...
    # YOUR CODE STARTS HERE
    A2, cache = forward_propagation(X, parameters)
    predictions = np.round(A2)
    # YOUR CODE ENDS HERE
    return predictions
```

## Expanding out to Deeper Networks

So far we have discussed the idea of logistic regression - a no layer
application of the stochastic approach to neural networks. We then
expand outward and added a singular layer where *x* becomes
*a*<sup>1</sup> as we loop through iterations of logistic regression.
The superscripts in these loops will be termed *L* moving forward, *L*
being the number of layers in the network. *n* has been defined as the
number of nodes within each layer and can vary for different values of
*L*.

### Hyper Parameters

Neural Nets have both parameters (*W* and *b*) as well as
hyperparameters. Hyperparameters are those parameters that control the
outputs aside from the parameters. These would include learning
rate-*α*, number of iterations, number of hidden layers-*L*, hidden
units-*n*, choice of activation function-ReLU, Sigmoid, Tanh, or Leaky
ReLU.

#### Question:

How do you optimize *L* and *n*? Is this purely a trial and error
challenge?

#### Answer:

Andrew states that Deep Learning is currently very empirical, meaning
that there is a lot of trial and error in adjusting the hyperparameters
and those best values may change as the structure adjusts. As such, it
is good practice to test new hyperparameters every few months to check
if new hardware, data structure, package structures have new
optimization.

## How Good is the Parellel Between Deep Learning and the Brain

Though the Neural Network is named after the brain and the idea of each
node receiving information from a network of other nodes, it is only in
correspondence to the brain in this way. Current neurology fails to be
able to characterize what actually goes on in a singular neuron so we
cannot strongly state that the stochastic functions we calculate
parallel the brains activity. Only in the way nodes share information
are Neural Networks similar to the brain.

## Building A 2 Layer Deep Network

``` python
# import numpy as np
# import h5py
# import matplotlib.pyplot as plt
# from testCases import *
# from dnn_utils import sigmoid, sigmoid_backward, relu, relu_backward
# from public_tests import *
# 
# %matplotlib inline
# plt.rcParams['figure.figsize'] = (5.0, 4.0) # set default size of plots
# plt.rcParams['image.interpolation'] = 'nearest'
# plt.rcParams['image.cmap'] = 'gray'
# 
# %load_ext autoreload
# %autoreload 2
# 
# np.random.seed(1)
```

``` python
# GRADED FUNCTION: initialize_parameters
def initialize_parameters(n_x, n_h, n_y):
    """
    Argument:
    n_x -- size of the input layer
    n_h -- size of the hidden layer
    n_y -- size of the output layer
    Returns:
    parameters -- python dictionary containing your parameters:
                    W1 -- weight matrix of shape (n_h, n_x)
                    b1 -- bias vector of shape (n_h, 1)
                    W2 -- weight matrix of shape (n_y, n_h)
                    b2 -- bias vector of shape (n_y, 1)
    """
    np.random.seed(1)
    #(≈ 4 lines of code)
    # W1 = ...
    # b1 = ...
    # W2 = ...
    # b2 = ...
    # YOUR CODE STARTS HERE
    W1 = np.random.randn(n_h, n_x)*0.01
    b1 = np.zeros([n_h, 1])
    W2 = np.random.randn(n_y, n_h)*0.01
    b2 = np.zeros([n_y, 1])
    # YOUR CODE ENDS HERE
    parameters = {"W1": W1,
                  "b1": b1,
                  "W2": W2,
                  "b2": b2}
    return parameters  
```

Exercise 2 - initialize_parameters_deep Implement initialization for an
L-layer Neural Network.

Instructions:

The model’s structure is *\[LINEAR -\> RELU\] × (L-1) -\> LINEAR -\>
SIGMOID*. I.e., it has 𝐿−1 layers using a ReLU activation function
followed by an output layer with a sigmoid activation function. Use
random initialization for the weight matrices. Use
np.random.randn(shape) \* 0.01. Use zeros initialization for the biases.
Use np.zeros(shape). You’ll store 𝑛\[𝑙\] , the number of units in
different layers, in a variable layer_dims. For example, the layer_dims
for last week’s Planar Data classification model would have been
\[2,4,1\]: There were two inputs, one hidden layer with 4 hidden units,
and an output layer with 1 output unit. This means W1’s shape was (4,2),
b1 was (4,1), W2 was (1,4) and b2 was (1,1). Now you will generalize
this to 𝐿 layers! Here is the implementation for 𝐿=1 (one layer neural
network). It should inspire you to implement the general case (L-layer
neural network). if L == 1: parameters\[“W” + str(L)\] =
np.random.randn(layer_dims\[1\], layer_dims\[0\]) \* 0.01
parameters\[“b” + str(L)\] = np.zeros((layer_dims\[1\], 1))

``` python
# GRADED FUNCTION: initialize_parameters_deep
def initialize_parameters_deep(layer_dims):
    """
    Arguments:
    layer_dims -- python array (list) containing the dimensions of each layer in our network
    Returns:
    parameters -- python dictionary containing your parameters "W1", "b1", ..., "WL", "bL":
                    Wl -- weight matrix of shape (layer_dims[l], layer_dims[l-1])
                    bl -- bias vector of shape (layer_dims[l], 1)
    """
    np.random.seed(3)
    parameters = {}
    L = len(layer_dims) # number of layers in the network
    for l in range(1, L):
        #(≈ 2 lines of code)
        # parameters['W' + str(l)] = ...
        # parameters['b' + str(l)] = ...
        # YOUR CODE STARTS HERE
        parameters['W' + str(l)] = np.random.randn(layer_dims[l], layer_dims[l-1]) * 0.01
        parameters['b' + str(l)] = np.zeros((layer_dims[l], 1))
        # YOUR CODE ENDS HERE
        assert(parameters['W' + str(l)].shape == (layer_dims[l], layer_dims[l - 1]))
        assert(parameters['b' + str(l)].shape == (layer_dims[l], 1))
    return parameters
```

4 - Forward Propagation Module

4.1 - Linear Forward Now that you have initialized your parameters, you
can do the forward propagation module. Start by implementing some basic
functions that you can use again later when implementing the model. Now,
you’ll complete three functions in this order:

LINEAR LINEAR -\> ACTIVATION where ACTIVATION will be either ReLU or
Sigmoid. \[LINEAR -\> RELU\] × (L-1) -\> LINEAR -\> SIGMOID (whole
model) The linear forward module (vectorized over all the examples)
computes the following equations:

𝑍\[𝑙\]=𝑊\[𝑙\]𝐴\[𝑙−1\]+𝑏[𝑙](4) where 𝐴\[0\]=𝑋 .

Exercise 3 - linear_forward Build the linear part of forward
propagation.

Reminder: The mathematical representation of this unit is
𝑍\[𝑙\]=𝑊\[𝑙\]𝐴\[𝑙−1\]+𝑏\[𝑙\] . You may also find np.dot() useful. If
your dimensions don’t match, printing W.shape may help.

``` python
# GRADED FUNCTION: linear_forward
def linear_forward(A, W, b):
    """
    Implement the linear part of a layer's forward propagation.
    Arguments:
    A -- activations from previous layer (or input data): (size of previous layer, number of examples)
    W -- weights matrix: numpy array of shape (size of current layer, size of previous layer)
    b -- bias vector, numpy array of shape (size of the current layer, 1)
    Returns:
    Z -- the input of the activation function, also called pre-activation parameter 
    cache -- a python tuple containing "A", "W" and "b" ; stored for computing the backward pass efficiently
    """
    #(≈ 1 line of code)
    # Z = ...
    # YOUR CODE STARTS HERE
    Z = np.dot(W,A) + b
    # YOUR CODE ENDS HERE
    cache = (A, W, b)
    return Z, cache
```

Linear-Activation Forward In this notebook, you will use two activation
functions:

Sigmoid: 𝜎(𝑍)=𝜎(𝑊𝐴+𝑏)=11+𝑒−(𝑊𝐴+𝑏) . You’ve been provided with the
sigmoid function which returns two items: the activation value “a” and a
“cache” that contains “Z” (it’s what we will feed in to the
corresponding backward function). To use it you could just call:

A, activation_cache = sigmoid(Z) ReLU: The mathematical formula for ReLu
is 𝐴=𝑅𝐸𝐿𝑈(𝑍)=𝑚𝑎𝑥(0,𝑍) . You’ve been provided with the relu function.
This function returns two items: the activation value “A” and a “cache”
that contains “Z” (it’s what you’ll feed in to the corresponding
backward function). To use it you could just call:

A, activation_cache = relu(Z) For added convenience, you’re going to
group two functions (Linear and Activation) into one function
(LINEAR-\>ACTIVATION). Hence, you’ll implement a function that does the
LINEAR forward step, followed by an ACTIVATION forward step.

Exercise 4 - linear_activation_forward Implement the forward propagation
of the LINEAR-\>ACTIVATION layer. Mathematical relation is:
𝐴\[𝑙\]=𝑔(𝑍\[𝑙\])=𝑔(𝑊\[𝑙\]𝐴\[𝑙−1\]+𝑏\[𝑙\]) where the activation “g” can
be sigmoid() or relu(). Use linear_forward() and the correct activation
function.

``` python
# GRADED FUNCTION: linear_activation_forward
def linear_activation_forward(A_prev, W, b, activation):
    """
    Implement the forward propagation for the LINEAR->ACTIVATION layer
    Arguments:
    A_prev -- activations from previous layer (or input data): (size of previous layer, number of examples)
    W -- weights matrix: numpy array of shape (size of current layer, size of previous layer)
    b -- bias vector, numpy array of shape (size of the current layer, 1)
    activation -- the activation to be used in this layer, stored as a text string: "sigmoid" or "relu"
    Returns:
    A -- the output of the activation function, also called the post-activation value 
    cache -- a python tuple containing "linear_cache" and "activation_cache";
             stored for computing the backward pass efficiently
    """
    if activation == "sigmoid":
        #(≈ 2 lines of code)
        # Z, linear_cache = ...
        # A, activation_cache = ...
        # YOUR CODE STARTS HERE
        Z, linear_cache = linear_forward(A_prev, W, b)
        A, activation_cache = sigmoid(Z)
        # YOUR CODE ENDS HERE
    elif activation == "relu":
        #(≈ 2 lines of code)
        # Z, linear_cache = ...
        # A, activation_cache = ...
        # YOUR CODE STARTS HERE
        Z, linear_cache = linear_forward(A_prev, W, b)
        A, activation_cache = relu(Z)
        # YOUR CODE ENDS HERE
    cache = (linear_cache, activation_cache)
    return A, cache
```

Exercise 5 - L_model_forward Implement the forward propagation of the
above model.

Instructions: In the code below, the variable AL will denote
𝐴\[𝐿\]=𝜎(𝑍\[𝐿\])=𝜎(𝑊\[𝐿\]𝐴\[𝐿−1\]+𝑏\[𝐿\]) . (This is sometimes also
called Yhat, i.e., this is 𝑌̂ .)

Hints:

Use the functions you’ve previously written Use a for loop to replicate
\[LINEAR-\>RELU\] (L-1) times Don’t forget to keep track of the caches
in the “caches” list. To add a new value c to a list, you can use
list.append(c).

``` python
# GRADED FUNCTION: L_model_forward
def L_model_forward(X, parameters):
    """
    Implement forward propagation for the [LINEAR->RELU]*(L-1)->LINEAR->SIGMOID computation
    Arguments:
    X -- data, numpy array of shape (input size, number of examples)
    parameters -- output of initialize_parameters_deep()
    Returns:
    AL -- last post-activation value
    caches -- list of caches containing:
                every cache of linear_activation_forward() (there are L-1 of them, indexed from 0 to L-1)
    """
    caches = []
    A = X
    L = len(parameters) // 2                  # number of layers in the neural network
    # Implement [LINEAR -> RELU]*(L-1). Add "cache" to the "caches" list.
    # The for loop starts at 1 because layer 0 is the input
    for l in range(1, L):
        A_prev = A 
        #(≈ 2 lines of code)
        # A, cache = ...
        # caches ...
        # YOUR CODE STARTS HERE
        A, cache = linear_activation_forward(A_prev, 
                                             parameters['W' + str(l)], 
                                             parameters['b' + str(l)], 
                                             activation='relu')
        caches.append(cache)
        # YOUR CODE ENDS HERE
    # Implement LINEAR -> SIGMOID. Add "cache" to the "caches" list.
    #(≈ 2 lines of code)
    # AL, cache = ...
    # caches ...
    # YOUR CODE STARTS HERE
    AL, cache = linear_activation_forward(A, 
                                          parameters['W' + str(L)], 
                                          parameters['b' + str(L)], 
                                          activation='sigmoid')
    caches.append(cache)
    # YOUR CODE ENDS HERE
    return AL, caches
```

Cost Function

Now you can implement forward and backward propagation! You need to
compute the cost, in order to check whether your model is actually
learning.

Exercise 6 - compute_cost Compute the cross-entropy cost *J*, using the
following formula:
$$-\\frac{1}{m} \\sum\\limits\_{i = 1}^{m} (y^{(i)}\\log\\left(a^{\[L\] (i)}\\right) + (1-y^{(i)})\\log\\left(1- a^{\[L\](i)}\\right)) \\tag{7}$$

``` python
# GRADED FUNCTION: compute_cost

def compute_cost(AL, Y):
    """
    Implement the cost function defined by equation (7).
    Arguments:
    AL -- probability vector corresponding to your label predictions, shape (1, number of examples)
    Y -- true "label" vector (for example: containing 0 if non-cat, 1 if cat), shape (1, number of examples)
    Returns:
    cost -- cross-entropy cost
    """
    m = Y.shape[1]
    # Compute loss from aL and y.
    # (≈ 1 lines of code)
    # cost = ...
    # YOUR CODE STARTS HERE
    cost =(-1/m) * np.sum(np.multiply(Y, np.log(AL)) + np.multiply(np.log(1-AL),(1-Y)))
    # YOUR CODE ENDS HERE
    cost = np.squeeze(cost)      # To make sure your cost's shape is what we expect (e.g. this turns [[17]] into 17).
    return cost
```

Backward Propagation Module Just as you did for the forward propagation,
you’ll implement helper functions for backpropagation. Remember that
backpropagation is used to calculate the gradient of the loss function
with respect to the parameters.

Linear Backward

For layer *l*, the linear part is:
*Z*<sup>\[*l*\]</sup> = *W*<sup>\[*l*\]</sup>*A*<sup>\[*l* − 1\]</sup> + *b*<sup>\[*l*\]</sup>
(followed by an activation).

Suppose you have already calculated the derivative
$dZ^{\[l\]} = \\frac{\\partial \\mathcal{L} }{\\partial Z^{\[l\]}}$. You
want to get
(*d**W*<sup>\[*l*\]</sup>, *d**b*<sup>\[*l*\]</sup>, *d**A*<sup>\[*l* − 1\]</sup>).

The three outputs
(*d**W*<sup>\[*l*\]</sup>, *d**b*<sup>\[*l*\]</sup>, *d**A*<sup>\[*l* − 1\]</sup>)
are computed using the input *d**Z*<sup>\[*l*\]</sup>.

Here are the formulas you need:
$$ dW^{\[l\]} = \\frac{\\partial \\mathcal{J} }{\\partial W^{\[l\]}} = \\frac{1}{m} dZ^{\[l\]} A^{\[l-1\] T} \\tag{8}$$
$$ db^{\[l\]} = \\frac{\\partial \\mathcal{J} }{\\partial b^{\[l\]}} = \\frac{1}{m} \\sum\_{i = 1}^{m} dZ^{\[l\](i)}\\tag{9}$$
$$ dA^{\[l-1\]} = \\frac{\\partial \\mathcal{L} }{\\partial A^{\[l-1\]}} = W^{\[l\] T} dZ^{\[l\]} \\tag{10}$$

*A*<sup>\[*l* − 1\]*T*</sup> is the transpose of
*A*<sup>\[*l* − 1\]</sup>.

``` python
# GRADED FUNCTION: linear_backward
def linear_backward(dZ, cache):
    """
    Implement the linear portion of backward propagation for a single layer (layer l)
    Arguments:
    dZ -- Gradient of the cost with respect to the linear output (of current layer l)
    cache -- tuple of values (A_prev, W, b) coming from the forward propagation in the current layer
    Returns:
    dA_prev -- Gradient of the cost with respect to the activation (of the previous layer l-1), same shape as A_prev
    dW -- Gradient of the cost with respect to W (current layer l), same shape as W
    db -- Gradient of the cost with respect to b (current layer l), same shape as b
    """
    A_prev, W, b = cache
    m = A_prev.shape[1]
    ### START CODE HERE ### (≈ 3 lines of code)
    # dW = ...
    # db = ... sum by the rows of dZ with keepdims=True
    # dA_prev = ...
    # YOUR CODE STARTS HERE
    dW =(1/m) * np.dot(dZ, A_prev.T)
    db =(1/m) * np.sum(dZ, axis = 1, keepdims = True)
    dA_prev = np.dot(W.T, dZ)
    # YOUR CODE ENDS HERE
    return dA_prev, dW, db
```

Linear-Activation Backward

Next, you will create a function that merges the two helper functions:
**`linear_backward`** and the backward step for the activation
**`linear_activation_backward`**.

To help you implement `linear_activation_backward`, two backward
functions have been provided: - **`sigmoid_backward`**: Implements the
backward propagation for SIGMOID unit. You can call it as follows:

``` python
dZ = sigmoid_backward(dA, activation_cache)
```

-   **`relu_backward`**: Implements the backward propagation for RELU
    unit. You can call it as follows:

``` python
dZ = relu_backward(dA, activation_cache)
```

If *g*(.) is the activation function, `sigmoid_backward` and
`relu_backward` compute
$$dZ^{\[l\]} = dA^{\[l\]} \* g'(Z^{\[l\]}). \\tag{11}$$

``` python
# GRADED FUNCTION: linear_activation_backward
def linear_activation_backward(dA, cache, activation):
    """
    Implement the backward propagation for the LINEAR->ACTIVATION layer.
    Arguments:
    dA -- post-activation gradient for current layer l 
    cache -- tuple of values (linear_cache, activation_cache) we store for computing backward propagation efficiently
    activation -- the activation to be used in this layer, stored as a text string: "sigmoid" or "relu"
    Returns:
    dA_prev -- Gradient of the cost with respect to the activation (of the previous layer l-1), same shape as A_prev
    dW -- Gradient of the cost with respect to W (current layer l), same shape as W
    db -- Gradient of the cost with respect to b (current layer l), same shape as b
    """
    linear_cache, activation_cache = cache
    if activation == "relu":
        #(≈ 2 lines of code)
        # dZ =  ...
        # dA_prev, dW, db =  ...
        # YOUR CODE STARTS HERE
        dZ = relu_backward(dA, activation_cache)
        dA_prev, dW, db = linear_backward(dZ, linear_cache)
        # YOUR CODE ENDS HERE
    elif activation == "sigmoid":
        #(≈ 2 lines of code)
        # dZ =  ...
        # dA_prev, dW, db =  ...
        # YOUR CODE STARTS HERE
        dZ = sigmoid_backward(dA, activation_cache)
        dA_prev, dW, db = linear_backward(dZ, linear_cache)
        # YOUR CODE ENDS HERE
    return dA_prev, dW, db
```

**Initializing backpropagation**:

To backpropagate through this network, you know that the output is:
*A*<sup>\[*L*\]</sup> = *σ*(*Z*<sup>\[*L*\]</sup>). Your code thus needs
to compute `dAL`
$= \\frac{\\partial \\mathcal{L}}{\\partial A^{\[L\]}}$. To do so, use
this formula (derived using calculus which, again, you don’t need
in-depth knowledge of!):

``` python
dAL = - (np.divide(Y, AL) - np.divide(1 - Y, 1 - AL)) # derivative of cost with respect to AL
```

You can then use this post-activation gradient `dAL` to keep going
backward. As seen in Figure 5, you can now feed in `dAL` into the
LINEAR-\>SIGMOID backward function you implemented (which will use the
cached values stored by the L_model_forward function).

After that, you will have to use a `for` loop to iterate through all the
other layers using the LINEAR-\>RELU backward function. You should store
each dA, dW, and db in the grads dictionary. To do so, use this formula
:

$$grads\["dW" + str(l)\] = dW^{\[l\]}\\tag{15} $$

For example, for *l* = 3 this would store *d**W*<sup>\[*l*\]</sup> in
`grads["dW3"]`.

``` python
# GRADED FUNCTION: L_model_backward
def L_model_backward(AL, Y, caches):
    """
    Implement the backward propagation for the [LINEAR->RELU] * (L-1) -> LINEAR -> SIGMOID group
    Arguments:
    AL -- probability vector, output of the forward propagation (L_model_forward())
    Y -- true "label" vector (containing 0 if non-cat, 1 if cat)
    caches -- list of caches containing:
                every cache of linear_activation_forward() with "relu" (it's caches[l], for l in range(L-1) i.e l = 0...L-2)
                the cache of linear_activation_forward() with "sigmoid" (it's caches[L-1])
    Returns:
    grads -- A dictionary with the gradients
             grads["dA" + str(l)] = ... 
             grads["dW" + str(l)] = ...
             grads["db" + str(l)] = ... 
    """
    grads = {}
    L = len(caches) # the number of layers
    m = AL.shape[1]
    Y = Y.reshape(AL.shape) # after this line, Y is the same shape as AL
    # Initializing the backpropagation
    #(1 line of code)
    # dAL = ...
    # YOUR CODE STARTS HERE
    dAL = - (np.divide(Y, AL) - np.divide(1 - Y, 1 - AL))
    # YOUR CODE ENDS HERE
    # Lth layer (SIGMOID -> LINEAR) gradients. Inputs: "dAL, current_cache". Outputs: "grads["dAL-1"], grads["dWL"], grads["dbL"]
    #(approx. 5 lines)
    # current_cache = ...
    # dA_prev_temp, dW_temp, db_temp = ...
    # grads["dA" + str(L-1)] = ...
    # grads["dW" + str(L)] = ...
    # grads["db" + str(L)] = ...
    # YOUR CODE STARTS HERE
    current_cache = caches[L-1]
    grads["dA" + str(L-1)], grads["dW" + str(L)], grads["db" + str(L)] = linear_activation_backward(dAL, current_cache, activation = 'sigmoid')                               
    # YOUR CODE ENDS HERE
    # Loop from l=L-2 to l=0
    for l in reversed(range(L-1)):
        # lth layer: (RELU -> LINEAR) gradients.
        # Inputs: "grads["dA" + str(l + 1)], current_cache". Outputs: "grads["dA" + str(l)] , grads["dW" + str(l + 1)] , grads["db" + str(l + 1)] 
        #(approx. 5 lines)
        # current_cache = ...
        # dA_prev_temp, dW_temp, db_temp = ...
        # grads["dA" + str(l)] = ...
        # grads["dW" + str(l + 1)] = ...
        # grads["db" + str(l + 1)] = ...
        # YOUR CODE STARTS HERE
        current_cache = caches[l]
        dA_prev_temp, dW_temp, db_temp = linear_activation_backward(grads["dA" + str(l+1)], current_cache, activation = 'relu')
        grads["dA" + str(l)] = dA_prev_temp
        grads["dW" + str(l + 1)] = dW_temp
        grads["db" + str(l + 1)] = db_temp
        # YOUR CODE ENDS HERE
    return grads
```

Update Parameters

In this section, you’ll update the parameters of the model, using
gradient descent:

$$ W^{\[l\]} = W^{\[l\]} - \\alpha \\text{ } dW^{\[l\]} \\tag{16}$$
$$ b^{\[l\]} = b^{\[l\]} - \\alpha \\text{ } db^{\[l\]} \\tag{17}$$

where *α* is the learning rate.

After computing the updated parameters, store them in the parameters
dictionary.

``` python
# GRADED FUNCTION: update_parameters
def update_parameters(params, grads, learning_rate):
    """
    Update parameters using gradient descent
    Arguments:
    params -- python dictionary containing your parameters 
    grads -- python dictionary containing your gradients, output of L_model_backward
    Returns:
    parameters -- python dictionary containing your updated parameters 
                  parameters["W" + str(l)] = ... 
                  parameters["b" + str(l)] = ...
    """
    parameters = params.copy()
    L = len(parameters) // 2 # number of layers in the neural network
    # Update rule for each parameter. Use a for loop.
    #(≈ 2 lines of code)
    for l in range(L):
        # parameters["W" + str(l+1)] = ...
        # parameters["b" + str(l+1)] = ...
        # YOUR CODE STARTS HERE
        parameters["W" + str(l+1)] = parameters["W" + str(l+1)] - learning_rate*grads["dW" + str(l+1)]
        parameters["b" + str(l+1)] = parameters["b" + str(l+1)] - learning_rate*grads["db" + str(l+1)]
        # YOUR CODE ENDS HERE
    return parameters
```

## Building an L-deep Network

``` python
# import time
# import numpy as np
# import h5py
# import matplotlib.pyplot as plt
# import scipy
# from PIL import Image
# from scipy import ndimage
# from dnn_app_utils_v3 import *
# from public_tests import *
# 
# %matplotlib inline
# plt.rcParams['figure.figsize'] = (5.0, 4.0) # set default size of plots
# plt.rcParams['image.interpolation'] = 'nearest'
# plt.rcParams['image.cmap'] = 'gray'
# 
# %load_ext autoreload
# %autoreload 2
# 
# np.random.seed(1)
```

2 - Load and Process the Dataset You’ll be using the same “Cat vs
non-Cat” dataset as in “Logistic Regression as a Neural Network”
(Assignment 2). The model you built back then had 70% test accuracy on
classifying cat vs non-cat images. Hopefully, your new model will
perform even better!

Problem Statement: You are given a dataset (“data.h5”) containing:

-   a training set of `m_train` images labelled as cat (1) or
    non-cat (0)
-   a test set of `m_test` images labelled as cat and non-cat
-   each image is of shape (num_px, num_px, 3) where 3 is for the 3
    channels (RGB). Let’s get more familiar with the dataset. Load the
    data by running the cell below.

``` python
# # Explore your dataset 
# m_train = train_x_orig.shape[0]
# num_px = train_x_orig.shape[1]
# m_test = test_x_orig.shape[0]
# 
# print ("Number of training examples: " + str(m_train))
# print ("Number of testing examples: " + str(m_test))
# print ("Each image is of size: (" + str(num_px) + ", " + str(num_px) + ", 3)")
# print ("train_x_orig shape: " + str(train_x_orig.shape))
# print ("train_y shape: " + str(train_y.shape))
# print ("test_x_orig shape: " + str(test_x_orig.shape))
# print ("test_y shape: " + str(test_y.shape))
```

Number of training examples: 209 Number of testing examples: 50 Each
image is of size: (64, 64, 3) train_x\_orig shape: (209, 64, 64, 3)
train_y shape: (1, 209) test_x\_orig shape: (50, 64, 64, 3) test_y
shape: (1, 50)

As usual, you reshape and standardize the images before feeding them to
the network. The code is given in the cell below.

![](/Users/camithomas/Desktop/Bryan/deep_learning.ai_coursera/Month1:%20Neural%20Networks%20and%20Deep%20Learning/image_shaping_dl.png)
The denominator in the next segment is the number of examples I believe?
It’s used to normalize the data training set

``` python
# # Reshape the training and test examples 
# train_x_flatten = train_x_orig.reshape(train_x_orig.shape[0], -1).T   # The "-1" makes reshape flatten the remaining dimensions
# test_x_flatten = test_x_orig.reshape(test_x_orig.shape[0], -1).T
# 
# # Standardize data to have feature values between 0 and 1.
# train_x = train_x_flatten/255.
# test_x = test_x_flatten/255.
# 
# print ("train_x's shape: " + str(train_x.shape))
# print ("test_x's shape: " + str(test_x.shape))
```

![](/Users/camithomas/Desktop/Bryan/deep_learning.ai_coursera/Month1:%20Neural%20Networks%20and%20Deep%20Learning/2Layer_arch.png)
Use the helper functions you have implemented in the previous assignment
to build a 2-layer neural network with the following structure: LINEAR
-\> RELU -\> LINEAR -\> SIGMOID. The functions and their inputs are:

def initialize_parameters(n_x, n_h, n_y): … return parameters def
linear_activation_forward(A_prev, W, b, activation): … return A, cache
def compute_cost(AL, Y): … return cost def
linear_activation_backward(dA, cache, activation): … return dA_prev, dW,
db def update_parameters(parameters, grads, learning_rate): … return
parameters

``` python
### CONSTANTS DEFINING THE MODEL ####
n_x = 12288     # num_px * num_px * 3
n_h = 7
n_y = 1
layers_dims = (n_x, n_h, n_y)
learning_rate = 0.0075
```

``` python
# GRADED FUNCTION: two_layer_model
def two_layer_model(X, Y, layers_dims, learning_rate = 0.0075, num_iterations = 3000, print_cost=False):
    """
    Implements a two-layer neural network: LINEAR->RELU->LINEAR->SIGMOID.
    Arguments:
    X -- input data, of shape (n_x, number of examples)
    Y -- true "label" vector (containing 1 if cat, 0 if non-cat), of shape (1, number of examples)
    layers_dims -- dimensions of the layers (n_x, n_h, n_y)
    num_iterations -- number of iterations of the optimization loop
    learning_rate -- learning rate of the gradient descent update rule
    print_cost -- If set to True, this will print the cost every 100 iterations 
    Returns:
    parameters -- a dictionary containing W1, W2, b1, and b2
    """
    np.random.seed(1)
    grads = {}
    costs = []                              # to keep track of the cost
    m = X.shape[1]                           # number of examples
    (n_x, n_h, n_y) = layers_dims
    # Initialize parameters dictionary, by calling one of the functions you'd previously implemented
    #(≈ 1 line of code)
    # parameters = ...
    # YOUR CODE STARTS HERE
    parameters = initialize_parameters(n_x, n_h, n_y)
    # YOUR CODE ENDS HERE
    # Get W1, b1, W2 and b2 from the dictionary parameters.
    W1 = parameters["W1"]
    b1 = parameters["b1"]
    W2 = parameters["W2"]
    b2 = parameters["b2"]
    # Loop (gradient descent)
    for i in range(0, num_iterations):
        # Forward propagation: LINEAR -> RELU -> LINEAR -> SIGMOID. Inputs: "X, W1, b1, W2, b2". Output: "A1, cache1, A2, cache2".
        #(≈ 2 lines of code)
        # A1, cache1 = ...
        # A2, cache2 = ...
        # YOUR CODE STARTS HERE
        A1, cache1 = linear_activation_forward(X, W1, b1, activation = 'relu')
        A2, cache2 = linear_activation_forward(A1, W2, b2, activation = 'sigmoid')
        # YOUR CODE ENDS HERE
        # Compute cost
        #(≈ 1 line of code)
        # cost = ...
        # YOUR CODE STARTS HERE
        cost = compute_cost(A2, Y)
        # YOUR CODE ENDS HERE
        # Initializing backward propagation
        dA2 = - (np.divide(Y, A2) - np.divide(1 - Y, 1 - A2))
        # Backward propagation. Inputs: "dA2, cache2, cache1". Outputs: "dA1, dW2, db2; also dA0 (not used), dW1, db1".
        #(≈ 2 lines of code)
        # dA1, dW2, db2 = ...
        # dA0, dW1, db1 = ...
        # YOUR CODE STARTS HERE
        dA1, dW2, db2 = linear_activation_backward(dA2, cache2, activation = 'sigmoid')
        dA0, dW1, db1 = linear_activation_backward(dA1, cache1, activation = 'relu')
        # YOUR CODE ENDS HERE
        # Set grads['dWl'] to dW1, grads['db1'] to db1, grads['dW2'] to dW2, grads['db2'] to db2
        grads['dW1'] = dW1
        grads['db1'] = db1
        grads['dW2'] = dW2
        grads['db2'] = db2
        # Update parameters.
        #(approx. 1 line of code)
        # parameters = ...
        # YOUR CODE STARTS HERE
        parameters = update_parameters(parameters, grads, learning_rate)
        # YOUR CODE ENDS HERE
        # Retrieve W1, b1, W2, b2 from parameters
        W1 = parameters["W1"]
        b1 = parameters["b1"]
        W2 = parameters["W2"]
        b2 = parameters["b2"]
        # Print the cost every 100 iterations
        if print_cost and i % 100 == 0 or i == num_iterations - 1:
            print("Cost after iteration {}: {}".format(i, np.squeeze(cost)))
        if i % 100 == 0 or i == num_iterations:
            costs.append(cost)
    return parameters, costs
# def plot_costs(costs, learning_rate=0.0075):
#     plt.plot(np.squeeze(costs))
#     plt.ylabel('cost')
#     plt.xlabel('iterations (per hundreds)')
#     plt.title("Learning rate =" + str(learning_rate))
#     plt.show()
```

### Training the Model

``` python
# parameters, costs = two_layer_model(train_x, train_y, layers_dims = (n_x, n_h, n_y), num_iterations = 2500, print_cost=True)
# plot_costs(costs, learning_rate)
```

``` python
# predictions_train = predict(train_x, train_y, parameters)
```

``` python
# predictions_test = predict(test_x, test_y, parameters)
```

![](/Users/camithomas/Desktop/Bryan/deep_learning.ai_coursera/Month1:%20Neural%20Networks%20and%20Deep%20Learning/Ldeep_arc.png)

Exercise 2 - L_layer_model Use the helper functions you implemented
previously to build an 𝐿 -layer neural network with the following
structure: *\[LINEAR -\> RELU\] × (L-1) -\> LINEAR -\> SIGMOID*. The
functions and their inputs are:

def initialize_parameters_deep(layers_dims): … return parameters def
L_model_forward(X, parameters): … return AL, caches def compute_cost(AL,
Y): … return cost def L_model_backward(AL, Y, caches): … return grads
def update_parameters(parameters, grads, learning_rate): … return
parameters

``` python
### CONSTANTS ###
layers_dims = [12288, 20, 7, 5, 1] #  4-layer model
```

``` python
# GRADED FUNCTION: L_layer_model
def L_layer_model(X, Y, layers_dims, learning_rate = 0.0075, num_iterations = 3000, print_cost=False):
    """
    Implements a L-layer neural network: [LINEAR->RELU]*(L-1)->LINEAR->SIGMOID.
    Arguments:
    X -- data, numpy array of shape (num_px * num_px * 3, number of examples)
    Y -- true "label" vector (containing 0 if cat, 1 if non-cat), of shape (1, number of examples)
    layers_dims -- list containing the input size and each layer size, of length (number of layers + 1).
    learning_rate -- learning rate of the gradient descent update rule
    num_iterations -- number of iterations of the optimization loop
    print_cost -- if True, it prints the cost every 100 steps
    Returns:
    parameters -- parameters learnt by the model. They can then be used to predict.
    """
    np.random.seed(1)
    costs = []                         # keep track of cost
    # Parameters initialization.
    #(≈ 1 line of code)
    # parameters = ...
    # YOUR CODE STARTS HERE
    parameters = initialize_parameters_deep(layers_dims)
    # YOUR CODE ENDS HERE
    # Loop (gradient descent)
    for i in range(0, num_iterations):
        # Forward propagation: [LINEAR -> RELU]*(L-1) -> LINEAR -> SIGMOID.
        #(≈ 1 line of code)
        # AL, caches = ...
        # YOUR CODE STARTS HERE
        AL, caches = L_model_forward(X, parameters)
        # YOUR CODE ENDS HERE
        # Compute cost.
        #(≈ 1 line of code)
        # cost = ...
        # YOUR CODE STARTS HERE
        cost = compute_cost(AL, Y)
        # YOUR CODE ENDS HERE
        # Backward propagation.
        #(≈ 1 line of code)
        # grads = ...    
        # YOUR CODE STARTS HERE
        grads = L_model_backward(AL, Y, caches)
        # YOUR CODE ENDS HERE
        # Update parameters.
        #(≈ 1 line of code)
        # parameters = ...
        # YOUR CODE STARTS HERE
        parameters = update_parameters(parameters, grads, learning_rate)
        # YOUR CODE ENDS HERE
        # Print the cost every 100 iterations
        if print_cost and i % 100 == 0 or i == num_iterations - 1:
            print("Cost after iteration {}: {}".format(i, np.squeeze(cost)))
        if i % 100 == 0 or i == num_iterations:
            costs.append(cost)
    return parameters, costs
```

``` python
# parameters, costs = L_layer_model(train_x, train_y, layers_dims, num_iterations = 2500, print_cost = True)
```

``` python
# pred_train = predict(train_x, train_y, parameters)
```

``` python
# pred_test = predict(test_x, test_y, parameters)
```
