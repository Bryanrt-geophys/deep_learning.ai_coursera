# Deep_Learning.AI_Coursera

This repository contains my work for the Coursera certification class  on deep learning, instructed by Andrew Ng.

![Notes for the first month's certification on deep learning](https://gitlab.com/Bryanrt-geophys/deep_learning.ai_coursera/-/blob/master/Month1:%20Neural%20Networks%20and%20Deep%20Learning/basics_of_neural_netwrok_coding.Rmd)

![Notes for the second month's certification on deep learning](https://gitlab.com/Bryanrt-geophys/deep_learning.ai_coursera/-/blob/master/Month2:Tunning%20Hyperparameters/Improving_Deep_Neural_Networks.md)
