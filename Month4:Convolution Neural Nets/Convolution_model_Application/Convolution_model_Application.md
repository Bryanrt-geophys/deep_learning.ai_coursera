# Convolutional Neural Networks: Application

Welcome to Course 4's second assignment! In this notebook, you will:

- Create a mood classifer using the TF Keras Sequential API
- Build a ConvNet to identify sign language digits using the TF Keras Functional API

**After this assignment you will be able to:**

- Build and train a ConvNet in TensorFlow for a __binary__ classification problem
- Build and train a ConvNet in TensorFlow for a __multiclass__ classification problem
- Explain different use cases for the Sequential and Functional APIs

To complete this assignment, you should already be familiar with TensorFlow. If you are not, please refer back to the **TensorFlow Tutorial** of the third week of Course 2 ("**Improving deep neural networks**").

## Table of Contents

- [1 - Packages](#1)
    - [1.1 - Load the Data and Split the Data into Train/Test Sets](#1-1)
- [2 - Layers in TF Keras](#2)
- [3 - The Sequential API](#3)
    - [3.1 - Create the Sequential Model](#3-1)
        - [Exercise 1 - happyModel](#ex-1)
    - [3.2 - Train and Evaluate the Model](#3-2)
- [4 - The Functional API](#4)
    - [4.1 - Load the SIGNS Dataset](#4-1)
    - [4.2 - Split the Data into Train/Test Sets](#4-2)
    - [4.3 - Forward Propagation](#4-3)
        - [Exercise 2 - convolutional_model](#ex-2)
    - [4.4 - Train the Model](#4-4)
- [5 - History Object](#5)
- [6 - Bibliography](#6)

<a name='1'></a>
## 1 - Packages

As usual, begin by loading in the packages.


```python
import math
import numpy as np
import h5py
import matplotlib.pyplot as plt
from matplotlib.pyplot import imread
import scipy
from PIL import Image
import pandas as pd
import tensorflow as tf
import tensorflow.keras.layers as tfl
from tensorflow.python.framework import ops
from cnn_utils import *
from test_utils import summary, comparator

%matplotlib inline
np.random.seed(1)
```

<a name='1-1'></a>
### 1.1 - Load the Data and Split the Data into Train/Test Sets

You'll be using the Happy House dataset for this part of the assignment, which contains images of peoples' faces. Your task will be to build a ConvNet that determines whether the people in the images are smiling or not -- because they only get to enter the house if they're smiling!  


```python
X_train_orig, Y_train_orig, X_test_orig, Y_test_orig, classes = load_happy_dataset()

# Normalize image vectors
X_train = X_train_orig/255.
X_test = X_test_orig/255.

# Reshape
Y_train = Y_train_orig.T
Y_test = Y_test_orig.T

print ("number of training examples = " + str(X_train.shape[0]))
print ("number of test examples = " + str(X_test.shape[0]))
print ("X_train shape: " + str(X_train.shape))
print ("Y_train shape: " + str(Y_train.shape))
print ("X_test shape: " + str(X_test.shape))
print ("Y_test shape: " + str(Y_test.shape))
```

    number of training examples = 600
    number of test examples = 150
    X_train shape: (600, 64, 64, 3)
    Y_train shape: (600, 1)
    X_test shape: (150, 64, 64, 3)
    Y_test shape: (150, 1)


You can display the images contained in the dataset. Images are **64x64** pixels in RGB format (3 channels).


```python
index = 35
plt.imshow(X_train_orig[index]) #display sample training image
plt.show()
```


![png](output_7_0.png)


<a name='2'></a>
## 2 - Layers in TF Keras 

In the previous assignment, you created layers manually in numpy. In TF Keras, you don't have to write code directly to create layers. Rather, TF Keras has pre-defined layers you can use. 

When you create a layer in TF Keras, you are creating a function that takes some input and transforms it into an output you can reuse later. Nice and easy! 

<a name='3'></a>
## 3 - The Sequential API

In the previous assignment, you built helper functions using `numpy` to understand the mechanics behind convolutional neural networks. Most practical applications of deep learning today are built using programming frameworks, which have many built-in functions you can simply call. Keras is a high-level abstraction built on top of TensorFlow, which allows for even more simplified and optimized model creation and training. 

For the first part of this assignment, you'll create a model using TF Keras' Sequential API, which allows you to build layer by layer, and is ideal for building models where each layer has **exactly one** input tensor and **one** output tensor. 

As you'll see, using the Sequential API is simple and straightforward, but is only appropriate for simpler, more straightforward tasks. Later in this notebook you'll spend some time building with a more flexible, powerful alternative: the Functional API. 
 

<a name='3-1'></a>
### 3.1 - Create the Sequential Model

As mentioned earlier, the TensorFlow Keras Sequential API can be used to build simple models with layer operations that proceed in a sequential order. 

You can also add layers incrementally to a Sequential model with the `.add()` method, or remove them using the `.pop()` method, much like you would in a regular Python list.

Actually, you can think of a Sequential model as behaving like a list of layers. Like Python lists, Sequential layers are ordered, and the order in which they are specified matters.  If your model is non-linear or contains layers with multiple inputs or outputs, a Sequential model wouldn't be the right choice!

For any layer construction in Keras, you'll need to specify the input shape in advance. This is because in Keras, the shape of the weights is based on the shape of the inputs. The weights are only created when the model first sees some input data. Sequential models can be created by passing a list of layers to the Sequential constructor, like you will do in the next assignment.

<a name='ex-1'></a>
### Exercise 1 - happyModel

Implement the `happyModel` function below to build the following model: `ZEROPAD2D -> CONV2D -> BATCHNORM -> RELU -> MAXPOOL -> FLATTEN -> DENSE`. Take help from [tf.keras.layers](https://www.tensorflow.org/api_docs/python/tf/keras/layers) 

Also, plug in the following parameters for all the steps:

 - [ZeroPadding2D](https://www.tensorflow.org/api_docs/python/tf/keras/layers/ZeroPadding2D): padding 3, input shape 64 x 64 x 3
 - [Conv2D](https://www.tensorflow.org/api_docs/python/tf/keras/layers/Conv2D): Use 32 7x7 filters, stride 1
 - [BatchNormalization](https://www.tensorflow.org/api_docs/python/tf/keras/layers/BatchNormalization): for axis 3
 - [ReLU](https://www.tensorflow.org/api_docs/python/tf/keras/layers/ReLU)
 - [MaxPool2D](https://www.tensorflow.org/api_docs/python/tf/keras/layers/MaxPool2D): Using default parameters
 - [Flatten](https://www.tensorflow.org/api_docs/python/tf/keras/layers/Flatten) the previous output.
 - Fully-connected ([Dense](https://www.tensorflow.org/api_docs/python/tf/keras/layers/Dense)) layer: Apply a fully connected layer with 1 neuron and a sigmoid activation. 
 
 
 **Hint:**
 
 Use **tfl** as shorthand for **tensorflow.keras.layers**


```python
# GRADED FUNCTION: happyModel

def happyModel():
    """
    Implements the forward propagation for the binary classification model:
    ZEROPAD2D -> CONV2D -> BATCHNORM -> RELU -> MAXPOOL -> FLATTEN -> DENSE
    
    Note that for simplicity and grading purposes, you'll hard-code all the values
    such as the stride and kernel (filter) sizes. 
    Normally, functions should take these values as function parameters.
    
    Arguments:
    None

    Returns:
    model -- TF Keras model (object containing the information for the entire training process) 
    """
    model = tf.keras.Sequential([

            
            # YOUR CODE STARTS HERE
            ## ZeroPadding2D with padding 3, input shape of 64 x 64 x 3
            tfl.ZeroPadding2D(padding=3,input_shape=(64, 64, 3)),
            ## Conv2D with 32 7x7 filters and stride of 1
            tfl.Conv2D(32, 7, strides=1),
            ## BatchNormalization for axis 3
            tfl.BatchNormalization(axis=3),
            ## ReLU
            tfl.ReLU(),
            ## Max Pooling 2D with default parameters
            tfl.MaxPool2D(),
            ## Flatten layer
            tfl.Flatten(),
            ## Dense layer with 1 unit for output & 'sigmoid' activation
            tfl.Dense(1, activation='sigmoid')
            # YOUR CODE ENDS HERE
        ])
    
    return model
```


```python
happy_model = happyModel()
# Print a summary for each layer
for layer in summary(happy_model):
    print(layer)
    
output = [['ZeroPadding2D', (None, 70, 70, 3), 0, ((3, 3), (3, 3))],
            ['Conv2D', (None, 64, 64, 32), 4736, 'valid', 'linear', 'GlorotUniform'],
            ['BatchNormalization', (None, 64, 64, 32), 128],
            ['ReLU', (None, 64, 64, 32), 0],
            ['MaxPooling2D', (None, 32, 32, 32), 0, (2, 2), (2, 2), 'valid'],
            ['Flatten', (None, 32768), 0],
            ['Dense', (None, 1), 32769, 'sigmoid']]
    
comparator(summary(happy_model), output)
```

    ['ZeroPadding2D', (None, 70, 70, 3), 0, ((3, 3), (3, 3))]
    ['Conv2D', (None, 64, 64, 32), 4736, 'valid', 'linear', 'GlorotUniform']
    ['BatchNormalization', (None, 64, 64, 32), 128]
    ['ReLU', (None, 64, 64, 32), 0]
    ['MaxPooling2D', (None, 32, 32, 32), 0, (2, 2), (2, 2), 'valid']
    ['Flatten', (None, 32768), 0]
    ['Dense', (None, 1), 32769, 'sigmoid']
    [32mAll tests passed![0m


Now that your model is created, you can compile it for training with an optimizer and loss of your choice. When the string `accuracy` is specified as a metric, the type of accuracy used will be automatically converted based on the loss function used. This is one of the many optimizations built into TensorFlow that make your life easier! If you'd like to read more on how the compiler operates, check the docs [here](https://www.tensorflow.org/api_docs/python/tf/keras/Model#compile).


```python
happy_model.compile(optimizer='adam',
                   loss='binary_crossentropy',
                   metrics=['accuracy'])
```

It's time to check your model's parameters with the `.summary()` method. This will display the types of layers you have, the shape of the outputs, and how many parameters are in each layer. 


```python
happy_model.summary()
```

    Model: "sequential"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    zero_padding2d_7 (ZeroPaddin (None, 70, 70, 3)         0         
    _________________________________________________________________
    conv2d_1 (Conv2D)            (None, 64, 64, 32)        4736      
    _________________________________________________________________
    batch_normalization_1 (Batch (None, 64, 64, 32)        128       
    _________________________________________________________________
    re_lu_1 (ReLU)               (None, 64, 64, 32)        0         
    _________________________________________________________________
    max_pooling2d_1 (MaxPooling2 (None, 32, 32, 32)        0         
    _________________________________________________________________
    flatten_1 (Flatten)          (None, 32768)             0         
    _________________________________________________________________
    dense (Dense)                (None, 1)                 32769     
    =================================================================
    Total params: 37,633
    Trainable params: 37,569
    Non-trainable params: 64
    _________________________________________________________________


<a name='3-2'></a>
### 3.2 - Train and Evaluate the Model

After creating the model, compiling it with your choice of optimizer and loss function, and doing a sanity check on its contents, you are now ready to build! 

Simply call `.fit()` to train. That's it! No need for mini-batching, saving, or complex backpropagation computations. That's all been done for you, as you're using a TensorFlow dataset with the batches specified already. You do have the option to specify epoch number or minibatch size if you like (for example, in the case of an un-batched dataset).


```python
happy_model.fit(X_train, Y_train, epochs=10, batch_size=16)
```

    Epoch 1/10
    38/38 [==============================] - 4s 98ms/step - loss: 1.2622 - accuracy: 0.6883
    Epoch 2/10
    38/38 [==============================] - 4s 95ms/step - loss: 0.1916 - accuracy: 0.9333
    Epoch 3/10
    38/38 [==============================] - 4s 95ms/step - loss: 0.1303 - accuracy: 0.9517
    Epoch 4/10
    38/38 [==============================] - 4s 95ms/step - loss: 0.1390 - accuracy: 0.9450
    Epoch 5/10
    38/38 [==============================] - 4s 95ms/step - loss: 0.0862 - accuracy: 0.9733
    Epoch 6/10
    38/38 [==============================] - 4s 97ms/step - loss: 0.0732 - accuracy: 0.9750
    Epoch 7/10
    38/38 [==============================] - 4s 95ms/step - loss: 0.3547 - accuracy: 0.8667
    Epoch 8/10
    38/38 [==============================] - 4s 95ms/step - loss: 0.0994 - accuracy: 0.9600
    Epoch 9/10
    38/38 [==============================] - 4s 97ms/step - loss: 0.1222 - accuracy: 0.9683
    Epoch 10/10
    38/38 [==============================] - 4s 95ms/step - loss: 0.0965 - accuracy: 0.9583





    <tensorflow.python.keras.callbacks.History at 0x7f18bb07d290>



After that completes, just use `.evaluate()` to evaluate against your test set. This function will print the value of the loss function and the performance metrics specified during the compilation of the model. In this case, the `binary_crossentropy` and the `accuracy` respectively.


```python
happy_model.evaluate(X_test, Y_test)
```

    5/5 [==============================] - 0s 24ms/step - loss: 0.1531 - accuracy: 0.9533





    [0.1530957967042923, 0.95333331823349]



Easy, right? But what if you need to build a model with shared layers, branches, or multiple inputs and outputs? This is where Sequential, with its beautifully simple yet limited functionality, won't be able to help you. 

Next up: Enter the Functional API, your slightly more complex, highly flexible friend.  

<a name='4'></a>
## 4 - The Functional API

Welcome to the second half of the assignment, where you'll use Keras' flexible [Functional API](https://www.tensorflow.org/guide/keras/functional) to build a ConvNet that can differentiate between 6 sign language digits. 

The Functional API can handle models with non-linear topology, shared layers, as well as layers with multiple inputs or outputs. Imagine that, where the Sequential API requires the model to move in a linear fashion through its layers, the Functional API allows much more flexibility. Where Sequential is a straight line, a Functional model is a graph, where the nodes of the layers can connect in many more ways than one. 

In the visual example below, the one possible direction of the movement Sequential model is shown in contrast to a skip connection, which is just one of the many ways a Functional model can be constructed. A skip connection, as you might have guessed, skips some layer in the network and feeds the output to a later layer in the network. Don't worry, you'll be spending more time with skip connections very soon! 

<img src="images/seq_vs_func.png" style="width:350px;height:200px;">

<a name='4-1'></a>
### 4.1 - Load the SIGNS Dataset

As a reminder, the SIGNS dataset is a collection of 6 signs representing numbers from 0 to 5.


```python
# Loading the data (signs)
X_train_orig, Y_train_orig, X_test_orig, Y_test_orig, classes = load_signs_dataset()
```

<img src="images/SIGNS.png" style="width:800px;height:300px;">

The next cell will show you an example of a labelled image in the dataset. Feel free to change the value of `index` below and re-run to see different examples. 


```python
# Example of an image from the dataset
index = 9
plt.imshow(X_train_orig[index])
print ("y = " + str(np.squeeze(Y_train_orig[:, index])))
```

    y = 4



![png](output_28_1.png)


<a name='4-2'></a>
### 4.2 - Split the Data into Train/Test Sets

In Course 2, you built a fully-connected network for this dataset. But since this is an image dataset, it is more natural to apply a ConvNet to it.

To get started, let's examine the shapes of your data. 


```python
X_train = X_train_orig/255.
X_test = X_test_orig/255.
Y_train = convert_to_one_hot(Y_train_orig, 6).T
Y_test = convert_to_one_hot(Y_test_orig, 6).T
print ("number of training examples = " + str(X_train.shape[0]))
print ("number of test examples = " + str(X_test.shape[0]))
print ("X_train shape: " + str(X_train.shape))
print ("Y_train shape: " + str(Y_train.shape))
print ("X_test shape: " + str(X_test.shape))
print ("Y_test shape: " + str(Y_test.shape))
```

    number of training examples = 1080
    number of test examples = 120
    X_train shape: (1080, 64, 64, 3)
    Y_train shape: (1080, 6)
    X_test shape: (120, 64, 64, 3)
    Y_test shape: (120, 6)


<a name='4-3'></a>
### 4.3 - Forward Propagation

In TensorFlow, there are built-in functions that implement the convolution steps for you. By now, you should be familiar with how TensorFlow builds computational graphs. In the [Functional API](https://www.tensorflow.org/guide/keras/functional), you create a graph of layers. This is what allows such great flexibility.

However, the following model could also be defined using the Sequential API since the information flow is on a single line. But don't deviate. What we want you to learn is to use the functional API.

Begin building your graph of layers by creating an input node that functions as a callable object:

- **input_img = tf.keras.Input(shape=input_shape):** 

Then, create a new node in the graph of layers by calling a layer on the `input_img` object: 

- **tf.keras.layers.Conv2D(filters= ... , kernel_size= ... , padding='same')(input_img):** Read the full documentation on [Conv2D](https://www.tensorflow.org/api_docs/python/tf/keras/layers/Conv2D).

- **tf.keras.layers.MaxPool2D(pool_size=(f, f), strides=(s, s), padding='same'):** `MaxPool2D()` downsamples your input using a window of size (f, f) and strides of size (s, s) to carry out max pooling over each window.  For max pooling, you usually operate on a single example at a time and a single channel at a time. Read the full documentation on [MaxPool2D](https://www.tensorflow.org/api_docs/python/tf/keras/layers/MaxPool2D).

- **tf.keras.layers.ReLU():** computes the elementwise ReLU of Z (which can be any shape). You can read the full documentation on [ReLU](https://www.tensorflow.org/api_docs/python/tf/keras/layers/ReLU).

- **tf.keras.layers.Flatten()**: given a tensor "P", this function takes each training (or test) example in the batch and flattens it into a 1D vector.  

    * If a tensor P has the shape (batch_size,h,w,c), it returns a flattened tensor with shape (batch_size, k), where $k=h \times w \times c$.  "k" equals the product of all the dimension sizes other than the first dimension.
    
    * For example, given a tensor with dimensions [100, 2, 3, 4], it flattens the tensor to be of shape [100, 24], where 24 = 2 * 3 * 4.  You can read the full documentation on [Flatten](https://www.tensorflow.org/api_docs/python/tf/keras/layers/Flatten).

- **tf.keras.layers.Dense(units= ... , activation='softmax')(F):** given the flattened input F, it returns the output computed using a fully connected layer. You can read the full documentation on [Dense](https://www.tensorflow.org/api_docs/python/tf/keras/layers/Dense).

In the last function above (`tf.keras.layers.Dense()`), the fully connected layer automatically initializes weights in the graph and keeps on training them as you train the model. Hence, you did not need to initialize those weights when initializing the parameters.

Lastly, before creating the model, you'll need to define the output using the last of the function's compositions (in this example, a Dense layer): 

- **outputs = tf.keras.layers.Dense(units=6, activation='softmax')(F)**


#### Window, kernel, filter, pool

The words "kernel" and "filter" are used to refer to the same thing. The word "filter" accounts for the amount of "kernels" that will be used in a single convolution layer. "Pool" is the name of the operation that takes the max or average value of the kernels. 

This is why the parameter `pool_size` refers to `kernel_size`, and you use `(f,f)` to refer to the filter size. 

Pool size and kernel size refer to the same thing in different objects - They refer to the shape of the window where the operation takes place. 

<a name='ex-2'></a>
### Exercise 2 - convolutional_model

Implement the `convolutional_model` function below to build the following model: `CONV2D -> RELU -> MAXPOOL -> CONV2D -> RELU -> MAXPOOL -> FLATTEN -> DENSE`. Use the functions above! 

Also, plug in the following parameters for all the steps:

 - [Conv2D](https://www.tensorflow.org/api_docs/python/tf/keras/layers/Conv2D): Use 8 4 by 4 filters, stride 1, padding is "SAME"
 - [ReLU](https://www.tensorflow.org/api_docs/python/tf/keras/layers/ReLU)
 - [MaxPool2D](https://www.tensorflow.org/api_docs/python/tf/keras/layers/MaxPool2D): Use an 8 by 8 filter size and an 8 by 8 stride, padding is "SAME"
 - **Conv2D**: Use 16 2 by 2 filters, stride 1, padding is "SAME"
 - **ReLU**
 - **MaxPool2D**: Use a 4 by 4 filter size and a 4 by 4 stride, padding is "SAME"
 - [Flatten](https://www.tensorflow.org/api_docs/python/tf/keras/layers/Flatten) the previous output.
 - Fully-connected ([Dense](https://www.tensorflow.org/api_docs/python/tf/keras/layers/Dense)) layer: Apply a fully connected layer with 6 neurons and a softmax activation. 


```python
# GRADED FUNCTION: convolutional_model

def convolutional_model(input_shape):
    """
    Implements the forward propagation for the model:
    CONV2D -> RELU -> MAXPOOL -> CONV2D -> RELU -> MAXPOOL -> FLATTEN -> DENSE
    
    Note that for simplicity and grading purposes, you'll hard-code some values
    such as the stride and kernel (filter) sizes. 
    Normally, functions should take these values as function parameters.
    
    Arguments:
    input_img -- input dataset, of shape (input_shape)

    Returns:
    model -- TF Keras model (object containing the information for the entire training process) 
    """

    input_img = tf.keras.Input(shape=input_shape)
    # YOUR CODE STARTS HERE
    ## CONV2D: 8 filters 4x4, stride of 1, padding 'SAME'
    Z1 = tfl.Conv2D(filters = 8,kernel_size = 4, strides = 1, padding = 'same')(input_img)
    ## RELU
    A1 = tfl.ReLU()(Z1)
    ## MAXPOOL: window 8x8, stride 8, padding 'SAME'
    P1 = tfl.MaxPool2D(pool_size = (8, 8), strides = 8, padding = 'same')(A1)
    ## CONV2D: 16 filters 2x2, stride 1, padding 'SAME'
    Z2 = tfl.Conv2D(filters = 16, kernel_size = 2, strides = 1, padding = 'same')(P1)
    ## RELU
    A2 = tfl.ReLU()(Z2)
    ## MAXPOOL: window 4x4, stride 4, padding 'SAME'
    P2 = tfl.MaxPool2D(pool_size = (4, 4), strides = 4, padding = 'same')(A2)
    ## FLATTEN
    F = tfl.Flatten()(P2)
    ## Dense layer
    ## 6 neurons in output layer. Hint: one of the arguments should be "activation='softmax'" 
    outputs = tfl.Dense(6, activation = 'softmax')(F)
    # YOUR CODE ENDS HERE
    model = tf.keras.Model(inputs = input_img, outputs = outputs)
    return model
```


```python
conv_model = convolutional_model((64, 64, 3))
conv_model.compile(optimizer='adam',
                  loss='categorical_crossentropy',
                  metrics=['accuracy'])
conv_model.summary()
    
output = [['InputLayer', [(None, 64, 64, 3)], 0],
        ['Conv2D', (None, 64, 64, 8), 392, 'same', 'linear', 'GlorotUniform'],
        ['ReLU', (None, 64, 64, 8), 0],
        ['MaxPooling2D', (None, 8, 8, 8), 0, (8, 8), (8, 8), 'same'],
        ['Conv2D', (None, 8, 8, 16), 528, 'same', 'linear', 'GlorotUniform'],
        ['ReLU', (None, 8, 8, 16), 0],
        ['MaxPooling2D', (None, 2, 2, 16), 0, (4, 4), (4, 4), 'same'],
        ['Flatten', (None, 64), 0],
        ['Dense', (None, 6), 390, 'softmax']]
    
comparator(summary(conv_model), output)
```

    Model: "functional_1"
    _________________________________________________________________
    Layer (type)                 Output Shape              Param #   
    =================================================================
    input_10 (InputLayer)        [(None, 64, 64, 3)]       0         
    _________________________________________________________________
    conv2d_8 (Conv2D)            (None, 64, 64, 8)         392       
    _________________________________________________________________
    re_lu_6 (ReLU)               (None, 64, 64, 8)         0         
    _________________________________________________________________
    max_pooling2d_2 (MaxPooling2 (None, 8, 8, 8)           0         
    _________________________________________________________________
    conv2d_9 (Conv2D)            (None, 8, 8, 16)          528       
    _________________________________________________________________
    re_lu_7 (ReLU)               (None, 8, 8, 16)          0         
    _________________________________________________________________
    max_pooling2d_3 (MaxPooling2 (None, 2, 2, 16)          0         
    _________________________________________________________________
    flatten_2 (Flatten)          (None, 64)                0         
    _________________________________________________________________
    dense_1 (Dense)              (None, 6)                 390       
    =================================================================
    Total params: 1,310
    Trainable params: 1,310
    Non-trainable params: 0
    _________________________________________________________________
    [32mAll tests passed![0m


Both the Sequential and Functional APIs return a TF Keras model object. The only difference is how inputs are handled inside the object model! 

<a name='4-4'></a>
### 4.4 - Train the Model


```python
train_dataset = tf.data.Dataset.from_tensor_slices((X_train, Y_train)).batch(64)
test_dataset = tf.data.Dataset.from_tensor_slices((X_test, Y_test)).batch(64)
history = conv_model.fit(train_dataset, epochs=100, validation_data=test_dataset)
```

    Epoch 1/100
    17/17 [==============================] - 2s 111ms/step - loss: 1.8077 - accuracy: 0.1694 - val_loss: 1.7900 - val_accuracy: 0.2250
    Epoch 2/100
    17/17 [==============================] - 2s 111ms/step - loss: 1.7843 - accuracy: 0.2333 - val_loss: 1.7831 - val_accuracy: 0.2583
    Epoch 3/100
    17/17 [==============================] - 2s 111ms/step - loss: 1.7771 - accuracy: 0.2250 - val_loss: 1.7755 - val_accuracy: 0.2333
    Epoch 4/100
    17/17 [==============================] - 2s 111ms/step - loss: 1.7716 - accuracy: 0.2704 - val_loss: 1.7698 - val_accuracy: 0.2833
    Epoch 5/100
    17/17 [==============================] - 2s 111ms/step - loss: 1.7647 - accuracy: 0.3056 - val_loss: 1.7621 - val_accuracy: 0.3167
    Epoch 6/100
    17/17 [==============================] - 2s 111ms/step - loss: 1.7575 - accuracy: 0.3213 - val_loss: 1.7551 - val_accuracy: 0.3333
    Epoch 7/100
    17/17 [==============================] - 2s 107ms/step - loss: 1.7498 - accuracy: 0.3509 - val_loss: 1.7480 - val_accuracy: 0.3750
    Epoch 8/100
    17/17 [==============================] - 2s 107ms/step - loss: 1.7409 - accuracy: 0.3593 - val_loss: 1.7378 - val_accuracy: 0.4000
    Epoch 9/100
    17/17 [==============================] - 2s 107ms/step - loss: 1.7283 - accuracy: 0.3944 - val_loss: 1.7269 - val_accuracy: 0.3833
    Epoch 10/100
    17/17 [==============================] - 2s 106ms/step - loss: 1.7141 - accuracy: 0.4148 - val_loss: 1.7141 - val_accuracy: 0.3917
    Epoch 11/100
    17/17 [==============================] - 2s 111ms/step - loss: 1.6969 - accuracy: 0.4324 - val_loss: 1.6993 - val_accuracy: 0.4333
    Epoch 12/100
    17/17 [==============================] - 2s 111ms/step - loss: 1.6752 - accuracy: 0.4454 - val_loss: 1.6810 - val_accuracy: 0.4500
    Epoch 13/100
    17/17 [==============================] - 2s 106ms/step - loss: 1.6478 - accuracy: 0.4546 - val_loss: 1.6574 - val_accuracy: 0.4583
    Epoch 14/100
    17/17 [==============================] - 2s 106ms/step - loss: 1.6163 - accuracy: 0.4657 - val_loss: 1.6286 - val_accuracy: 0.4750
    Epoch 15/100
    17/17 [==============================] - 2s 107ms/step - loss: 1.5794 - accuracy: 0.4694 - val_loss: 1.5959 - val_accuracy: 0.4917
    Epoch 16/100
    17/17 [==============================] - 2s 106ms/step - loss: 1.5398 - accuracy: 0.4806 - val_loss: 1.5579 - val_accuracy: 0.5083
    Epoch 17/100
    17/17 [==============================] - 2s 106ms/step - loss: 1.4977 - accuracy: 0.5009 - val_loss: 1.5195 - val_accuracy: 0.5167
    Epoch 18/100
    17/17 [==============================] - 2s 106ms/step - loss: 1.4554 - accuracy: 0.5083 - val_loss: 1.4806 - val_accuracy: 0.5250
    Epoch 19/100
    17/17 [==============================] - 2s 112ms/step - loss: 1.4139 - accuracy: 0.5250 - val_loss: 1.4431 - val_accuracy: 0.5250
    Epoch 20/100
    17/17 [==============================] - 2s 112ms/step - loss: 1.3732 - accuracy: 0.5463 - val_loss: 1.4035 - val_accuracy: 0.5333
    Epoch 21/100
    17/17 [==============================] - 2s 106ms/step - loss: 1.3347 - accuracy: 0.5602 - val_loss: 1.3672 - val_accuracy: 0.5250
    Epoch 22/100
    17/17 [==============================] - 2s 106ms/step - loss: 1.2971 - accuracy: 0.5722 - val_loss: 1.3286 - val_accuracy: 0.5583
    Epoch 23/100
    17/17 [==============================] - 2s 106ms/step - loss: 1.2603 - accuracy: 0.5944 - val_loss: 1.2934 - val_accuracy: 0.5750
    Epoch 24/100
    17/17 [==============================] - 2s 106ms/step - loss: 1.2235 - accuracy: 0.6065 - val_loss: 1.2575 - val_accuracy: 0.5750
    Epoch 25/100
    17/17 [==============================] - 2s 106ms/step - loss: 1.1889 - accuracy: 0.6296 - val_loss: 1.2246 - val_accuracy: 0.6000
    Epoch 26/100
    17/17 [==============================] - 2s 111ms/step - loss: 1.1551 - accuracy: 0.6380 - val_loss: 1.1913 - val_accuracy: 0.6000
    Epoch 27/100
    17/17 [==============================] - 2s 107ms/step - loss: 1.1222 - accuracy: 0.6463 - val_loss: 1.1589 - val_accuracy: 0.6000
    Epoch 28/100
    17/17 [==============================] - 2s 107ms/step - loss: 1.0899 - accuracy: 0.6556 - val_loss: 1.1285 - val_accuracy: 0.6000
    Epoch 29/100
    17/17 [==============================] - 2s 106ms/step - loss: 1.0599 - accuracy: 0.6648 - val_loss: 1.0976 - val_accuracy: 0.6000
    Epoch 30/100
    17/17 [==============================] - 2s 106ms/step - loss: 1.0314 - accuracy: 0.6713 - val_loss: 1.0699 - val_accuracy: 0.6167
    Epoch 31/100
    17/17 [==============================] - 2s 111ms/step - loss: 1.0042 - accuracy: 0.6833 - val_loss: 1.0425 - val_accuracy: 0.6250
    Epoch 32/100
    17/17 [==============================] - 2s 111ms/step - loss: 0.9780 - accuracy: 0.6907 - val_loss: 1.0171 - val_accuracy: 0.6667
    Epoch 33/100
    17/17 [==============================] - 2s 112ms/step - loss: 0.9534 - accuracy: 0.6981 - val_loss: 0.9933 - val_accuracy: 0.6667
    Epoch 34/100
    17/17 [==============================] - 2s 106ms/step - loss: 0.9301 - accuracy: 0.7028 - val_loss: 0.9721 - val_accuracy: 0.6417
    Epoch 35/100
    17/17 [==============================] - 2s 106ms/step - loss: 0.9085 - accuracy: 0.7028 - val_loss: 0.9490 - val_accuracy: 0.6667
    Epoch 36/100
    17/17 [==============================] - 2s 106ms/step - loss: 0.8883 - accuracy: 0.7139 - val_loss: 0.9297 - val_accuracy: 0.6583
    Epoch 37/100
    17/17 [==============================] - 2s 111ms/step - loss: 0.8687 - accuracy: 0.7213 - val_loss: 0.9102 - val_accuracy: 0.6583
    Epoch 38/100
    17/17 [==============================] - 2s 106ms/step - loss: 0.8504 - accuracy: 0.7241 - val_loss: 0.8929 - val_accuracy: 0.6667
    Epoch 39/100
    17/17 [==============================] - 2s 106ms/step - loss: 0.8331 - accuracy: 0.7324 - val_loss: 0.8773 - val_accuracy: 0.6667
    Epoch 40/100
    17/17 [==============================] - 2s 112ms/step - loss: 0.8169 - accuracy: 0.7444 - val_loss: 0.8622 - val_accuracy: 0.6750
    Epoch 41/100
    17/17 [==============================] - 2s 106ms/step - loss: 0.8020 - accuracy: 0.7472 - val_loss: 0.8486 - val_accuracy: 0.6750
    Epoch 42/100
    17/17 [==============================] - 2s 107ms/step - loss: 0.7875 - accuracy: 0.7519 - val_loss: 0.8353 - val_accuracy: 0.6750
    Epoch 43/100
    17/17 [==============================] - 2s 106ms/step - loss: 0.7737 - accuracy: 0.7574 - val_loss: 0.8237 - val_accuracy: 0.6833
    Epoch 44/100
    17/17 [==============================] - 2s 107ms/step - loss: 0.7610 - accuracy: 0.7657 - val_loss: 0.8107 - val_accuracy: 0.6917
    Epoch 45/100
    17/17 [==============================] - 2s 112ms/step - loss: 0.7491 - accuracy: 0.7676 - val_loss: 0.8004 - val_accuracy: 0.7000
    Epoch 46/100
    17/17 [==============================] - 2s 106ms/step - loss: 0.7374 - accuracy: 0.7704 - val_loss: 0.7884 - val_accuracy: 0.7000
    Epoch 47/100
    17/17 [==============================] - 2s 112ms/step - loss: 0.7263 - accuracy: 0.7713 - val_loss: 0.7793 - val_accuracy: 0.7000
    Epoch 48/100
    17/17 [==============================] - 2s 106ms/step - loss: 0.7155 - accuracy: 0.7750 - val_loss: 0.7678 - val_accuracy: 0.7000
    Epoch 49/100
    17/17 [==============================] - 2s 106ms/step - loss: 0.7053 - accuracy: 0.7759 - val_loss: 0.7594 - val_accuracy: 0.7083
    Epoch 50/100
    17/17 [==============================] - 2s 106ms/step - loss: 0.6952 - accuracy: 0.7759 - val_loss: 0.7495 - val_accuracy: 0.7083
    Epoch 51/100
    17/17 [==============================] - 2s 111ms/step - loss: 0.6858 - accuracy: 0.7806 - val_loss: 0.7414 - val_accuracy: 0.7000
    Epoch 52/100
    17/17 [==============================] - 2s 106ms/step - loss: 0.6766 - accuracy: 0.7880 - val_loss: 0.7317 - val_accuracy: 0.6917
    Epoch 53/100
    17/17 [==============================] - 2s 106ms/step - loss: 0.6681 - accuracy: 0.7870 - val_loss: 0.7250 - val_accuracy: 0.7083
    Epoch 54/100
    17/17 [==============================] - 2s 106ms/step - loss: 0.6597 - accuracy: 0.7917 - val_loss: 0.7163 - val_accuracy: 0.7083
    Epoch 55/100
    17/17 [==============================] - 2s 106ms/step - loss: 0.6514 - accuracy: 0.7944 - val_loss: 0.7103 - val_accuracy: 0.7083
    Epoch 56/100
    17/17 [==============================] - 2s 112ms/step - loss: 0.6443 - accuracy: 0.7935 - val_loss: 0.7027 - val_accuracy: 0.7083
    Epoch 57/100
    17/17 [==============================] - 2s 111ms/step - loss: 0.6366 - accuracy: 0.7944 - val_loss: 0.6965 - val_accuracy: 0.7083
    Epoch 58/100
    17/17 [==============================] - 2s 106ms/step - loss: 0.6291 - accuracy: 0.7963 - val_loss: 0.6899 - val_accuracy: 0.7167
    Epoch 59/100
    17/17 [==============================] - 2s 106ms/step - loss: 0.6221 - accuracy: 0.7963 - val_loss: 0.6842 - val_accuracy: 0.7250
    Epoch 60/100
    17/17 [==============================] - 2s 106ms/step - loss: 0.6152 - accuracy: 0.7991 - val_loss: 0.6778 - val_accuracy: 0.7333
    Epoch 61/100
    17/17 [==============================] - 2s 106ms/step - loss: 0.6087 - accuracy: 0.8093 - val_loss: 0.6733 - val_accuracy: 0.7417
    Epoch 62/100
    17/17 [==============================] - 2s 106ms/step - loss: 0.6019 - accuracy: 0.8111 - val_loss: 0.6673 - val_accuracy: 0.7417
    Epoch 63/100
    17/17 [==============================] - 2s 106ms/step - loss: 0.5956 - accuracy: 0.8139 - val_loss: 0.6625 - val_accuracy: 0.7500
    Epoch 64/100
    17/17 [==============================] - 2s 106ms/step - loss: 0.5897 - accuracy: 0.8139 - val_loss: 0.6569 - val_accuracy: 0.7500
    Epoch 65/100
    17/17 [==============================] - 2s 111ms/step - loss: 0.5839 - accuracy: 0.8148 - val_loss: 0.6520 - val_accuracy: 0.7583
    Epoch 66/100
    17/17 [==============================] - 2s 106ms/step - loss: 0.5783 - accuracy: 0.8148 - val_loss: 0.6479 - val_accuracy: 0.7583
    Epoch 67/100
    17/17 [==============================] - 2s 111ms/step - loss: 0.5726 - accuracy: 0.8167 - val_loss: 0.6424 - val_accuracy: 0.7667
    Epoch 68/100
    17/17 [==============================] - 2s 107ms/step - loss: 0.5672 - accuracy: 0.8194 - val_loss: 0.6387 - val_accuracy: 0.7750
    Epoch 69/100
    17/17 [==============================] - 2s 106ms/step - loss: 0.5617 - accuracy: 0.8194 - val_loss: 0.6324 - val_accuracy: 0.7833
    Epoch 70/100
    17/17 [==============================] - 2s 106ms/step - loss: 0.5564 - accuracy: 0.8185 - val_loss: 0.6307 - val_accuracy: 0.7917
    Epoch 71/100
    17/17 [==============================] - 2s 106ms/step - loss: 0.5509 - accuracy: 0.8222 - val_loss: 0.6229 - val_accuracy: 0.7833
    Epoch 72/100
    17/17 [==============================] - 2s 106ms/step - loss: 0.5458 - accuracy: 0.8231 - val_loss: 0.6224 - val_accuracy: 0.7917
    Epoch 73/100
    17/17 [==============================] - 2s 107ms/step - loss: 0.5399 - accuracy: 0.8259 - val_loss: 0.6150 - val_accuracy: 0.8167
    Epoch 74/100
    17/17 [==============================] - 2s 106ms/step - loss: 0.5355 - accuracy: 0.8259 - val_loss: 0.6138 - val_accuracy: 0.8000
    Epoch 75/100
    17/17 [==============================] - 2s 106ms/step - loss: 0.5304 - accuracy: 0.8287 - val_loss: 0.6070 - val_accuracy: 0.8250
    Epoch 76/100
    17/17 [==============================] - 2s 106ms/step - loss: 0.5259 - accuracy: 0.8287 - val_loss: 0.6060 - val_accuracy: 0.8083
    Epoch 77/100
    17/17 [==============================] - 2s 106ms/step - loss: 0.5208 - accuracy: 0.8333 - val_loss: 0.6002 - val_accuracy: 0.8250
    Epoch 78/100
    17/17 [==============================] - 2s 106ms/step - loss: 0.5163 - accuracy: 0.8343 - val_loss: 0.5981 - val_accuracy: 0.8167
    Epoch 79/100
    17/17 [==============================] - 2s 106ms/step - loss: 0.5111 - accuracy: 0.8352 - val_loss: 0.5936 - val_accuracy: 0.8250
    Epoch 80/100
    17/17 [==============================] - 2s 106ms/step - loss: 0.5069 - accuracy: 0.8352 - val_loss: 0.5914 - val_accuracy: 0.8167
    Epoch 81/100
    17/17 [==============================] - 2s 106ms/step - loss: 0.5021 - accuracy: 0.8370 - val_loss: 0.5869 - val_accuracy: 0.8250
    Epoch 82/100
    17/17 [==============================] - 2s 107ms/step - loss: 0.4974 - accuracy: 0.8426 - val_loss: 0.5836 - val_accuracy: 0.8167
    Epoch 83/100
    17/17 [==============================] - 2s 112ms/step - loss: 0.4929 - accuracy: 0.8426 - val_loss: 0.5802 - val_accuracy: 0.8250
    Epoch 84/100
    17/17 [==============================] - 2s 112ms/step - loss: 0.4885 - accuracy: 0.8500 - val_loss: 0.5768 - val_accuracy: 0.8167
    Epoch 85/100
    17/17 [==============================] - 2s 112ms/step - loss: 0.4844 - accuracy: 0.8509 - val_loss: 0.5741 - val_accuracy: 0.8167
    Epoch 86/100
    17/17 [==============================] - 2s 112ms/step - loss: 0.4800 - accuracy: 0.8519 - val_loss: 0.5705 - val_accuracy: 0.8167
    Epoch 87/100
    17/17 [==============================] - 2s 111ms/step - loss: 0.4761 - accuracy: 0.8509 - val_loss: 0.5677 - val_accuracy: 0.8167
    Epoch 88/100
    17/17 [==============================] - 2s 113ms/step - loss: 0.4722 - accuracy: 0.8546 - val_loss: 0.5638 - val_accuracy: 0.8167
    Epoch 89/100
    17/17 [==============================] - 2s 106ms/step - loss: 0.4682 - accuracy: 0.8537 - val_loss: 0.5610 - val_accuracy: 0.8167
    Epoch 90/100
    17/17 [==============================] - 2s 106ms/step - loss: 0.4647 - accuracy: 0.8546 - val_loss: 0.5577 - val_accuracy: 0.8167
    Epoch 91/100
    17/17 [==============================] - 2s 106ms/step - loss: 0.4610 - accuracy: 0.8565 - val_loss: 0.5544 - val_accuracy: 0.8167
    Epoch 92/100
    17/17 [==============================] - 2s 112ms/step - loss: 0.4572 - accuracy: 0.8574 - val_loss: 0.5520 - val_accuracy: 0.8167
    Epoch 93/100
    17/17 [==============================] - 2s 106ms/step - loss: 0.4536 - accuracy: 0.8574 - val_loss: 0.5491 - val_accuracy: 0.8167
    Epoch 94/100
    17/17 [==============================] - 2s 106ms/step - loss: 0.4501 - accuracy: 0.8583 - val_loss: 0.5464 - val_accuracy: 0.8167
    Epoch 95/100
    17/17 [==============================] - 2s 106ms/step - loss: 0.4463 - accuracy: 0.8620 - val_loss: 0.5434 - val_accuracy: 0.8167
    Epoch 96/100
    17/17 [==============================] - 2s 106ms/step - loss: 0.4428 - accuracy: 0.8630 - val_loss: 0.5408 - val_accuracy: 0.8167
    Epoch 97/100
    17/17 [==============================] - 2s 111ms/step - loss: 0.4392 - accuracy: 0.8648 - val_loss: 0.5384 - val_accuracy: 0.8167
    Epoch 98/100
    17/17 [==============================] - 2s 112ms/step - loss: 0.4360 - accuracy: 0.8648 - val_loss: 0.5359 - val_accuracy: 0.8167
    Epoch 99/100
    17/17 [==============================] - 2s 102ms/step - loss: 0.4328 - accuracy: 0.8667 - val_loss: 0.5331 - val_accuracy: 0.8167
    Epoch 100/100
    17/17 [==============================] - 2s 106ms/step - loss: 0.4297 - accuracy: 0.8685 - val_loss: 0.5307 - val_accuracy: 0.8167


<a name='5'></a>
## 5 - History Object 

The history object is an output of the `.fit()` operation, and provides a record of all the loss and metric values in memory. It's stored as a dictionary that you can retrieve at `history.history`: 


```python
history.history
```




    {'loss': [1.8077107667922974,
      1.7843059301376343,
      1.777050256729126,
      1.7716481685638428,
      1.7647199630737305,
      1.7574676275253296,
      1.749789834022522,
      1.7408784627914429,
      1.7282952070236206,
      1.7141027450561523,
      1.6968799829483032,
      1.675165057182312,
      1.647820234298706,
      1.6162561178207397,
      1.5794436931610107,
      1.5397711992263794,
      1.4976555109024048,
      1.4554048776626587,
      1.4138692617416382,
      1.3732352256774902,
      1.3347351551055908,
      1.297084093093872,
      1.2602636814117432,
      1.2234899997711182,
      1.1888903379440308,
      1.1550554037094116,
      1.1222474575042725,
      1.089920163154602,
      1.0598630905151367,
      1.0313711166381836,
      1.0041813850402832,
      0.9780466556549072,
      0.9534288048744202,
      0.9301109910011292,
      0.9084556102752686,
      0.8882690668106079,
      0.8687163591384888,
      0.8503583669662476,
      0.8331432938575745,
      0.8169047832489014,
      0.8020068407058716,
      0.7874693274497986,
      0.7737299799919128,
      0.761035144329071,
      0.7491327524185181,
      0.7374001741409302,
      0.726344883441925,
      0.7154701352119446,
      0.7052901387214661,
      0.6952370405197144,
      0.6857917904853821,
      0.6766330599784851,
      0.668056845664978,
      0.6596876382827759,
      0.6514376401901245,
      0.644254207611084,
      0.6365578770637512,
      0.6290701627731323,
      0.6220855712890625,
      0.6152279376983643,
      0.6087433695793152,
      0.6018692255020142,
      0.5956130027770996,
      0.5897279977798462,
      0.583937406539917,
      0.5783450603485107,
      0.572567343711853,
      0.5671623945236206,
      0.5617371201515198,
      0.5563691258430481,
      0.5508897304534912,
      0.5457871556282043,
      0.5398880839347839,
      0.5354908108711243,
      0.5303788185119629,
      0.5259019732475281,
      0.5207567811012268,
      0.5163209438323975,
      0.5110712051391602,
      0.506945788860321,
      0.5021193027496338,
      0.4974181056022644,
      0.49286702275276184,
      0.48852619528770447,
      0.484394371509552,
      0.4799822270870209,
      0.4761027693748474,
      0.47218552231788635,
      0.4682154655456543,
      0.4646962881088257,
      0.4609568417072296,
      0.4572160542011261,
      0.4536379873752594,
      0.45013031363487244,
      0.4462766945362091,
      0.4427957236766815,
      0.43922510743141174,
      0.43596819043159485,
      0.432788610458374,
      0.42968857288360596],
     'accuracy': [0.16944444179534912,
      0.23333333432674408,
      0.22499999403953552,
      0.27037036418914795,
      0.3055555522441864,
      0.3212963044643402,
      0.35092592239379883,
      0.3592592477798462,
      0.39444443583488464,
      0.4148148000240326,
      0.432407408952713,
      0.4453703761100769,
      0.4546296298503876,
      0.46574074029922485,
      0.4694444537162781,
      0.48055556416511536,
      0.5009258985519409,
      0.5083333253860474,
      0.5249999761581421,
      0.5462962985038757,
      0.5601851940155029,
      0.5722222328186035,
      0.5944444537162781,
      0.6064814925193787,
      0.6296296119689941,
      0.6379629373550415,
      0.6462963223457336,
      0.6555555462837219,
      0.664814829826355,
      0.6712962985038757,
      0.6833333373069763,
      0.6907407641410828,
      0.6981481313705444,
      0.7027778029441833,
      0.7027778029441833,
      0.7138888835906982,
      0.7212963104248047,
      0.7240740656852722,
      0.7324073910713196,
      0.7444444298744202,
      0.7472222447395325,
      0.7518518567085266,
      0.7574074268341064,
      0.7657407522201538,
      0.7675926089286804,
      0.770370364189148,
      0.7712963223457336,
      0.7749999761581421,
      0.7759259343147278,
      0.7759259343147278,
      0.7805555462837219,
      0.7879629731178284,
      0.7870370149612427,
      0.7916666865348816,
      0.7944444417953491,
      0.7935185432434082,
      0.7944444417953491,
      0.7962962985038757,
      0.7962962985038757,
      0.7990740537643433,
      0.8092592358589172,
      0.8111110925674438,
      0.8138889074325562,
      0.8138889074325562,
      0.8148148059844971,
      0.8148148059844971,
      0.8166666626930237,
      0.8194444179534912,
      0.8194444179534912,
      0.8185185194015503,
      0.8222222328186035,
      0.8231481313705444,
      0.8259259462356567,
      0.8259259462356567,
      0.8287037014961243,
      0.8287037014961243,
      0.8333333134651184,
      0.8342592716217041,
      0.835185170173645,
      0.835185170173645,
      0.8370370268821716,
      0.8425925970077515,
      0.8425925970077515,
      0.8500000238418579,
      0.8509259223937988,
      0.8518518805503845,
      0.8509259223937988,
      0.854629635810852,
      0.8537036776542664,
      0.854629635810852,
      0.8564814925193787,
      0.8574073910713196,
      0.8574073910713196,
      0.8583333492279053,
      0.8620370626449585,
      0.8629629611968994,
      0.864814817905426,
      0.864814817905426,
      0.8666666746139526,
      0.8685185313224792],
     'val_loss': [1.7899671792984009,
      1.783064603805542,
      1.7754501104354858,
      1.7697901725769043,
      1.7620741128921509,
      1.7550829648971558,
      1.7479690313339233,
      1.7378402948379517,
      1.7268826961517334,
      1.7141374349594116,
      1.6993072032928467,
      1.6810246706008911,
      1.6574255228042603,
      1.6285960674285889,
      1.5959445238113403,
      1.5579090118408203,
      1.519477128982544,
      1.4806348085403442,
      1.443123459815979,
      1.403464913368225,
      1.367233157157898,
      1.3285852670669556,
      1.2934386730194092,
      1.257458209991455,
      1.2246342897415161,
      1.1912676095962524,
      1.1589387655258179,
      1.128521203994751,
      1.0976378917694092,
      1.0699279308319092,
      1.0424772500991821,
      1.017143964767456,
      0.993313193321228,
      0.9721413850784302,
      0.9489920139312744,
      0.9296999573707581,
      0.9102175235748291,
      0.8928824067115784,
      0.8773161172866821,
      0.8621730804443359,
      0.8485637903213501,
      0.8352816700935364,
      0.8236747980117798,
      0.8107016086578369,
      0.8004320859909058,
      0.7884399890899658,
      0.7793113589286804,
      0.7677852511405945,
      0.7594248056411743,
      0.7494542598724365,
      0.7413794994354248,
      0.7316615581512451,
      0.7250298857688904,
      0.7162936925888062,
      0.7103164792060852,
      0.7026539444923401,
      0.6965198516845703,
      0.6899275183677673,
      0.6842264533042908,
      0.6778482794761658,
      0.6733269095420837,
      0.6673301458358765,
      0.6624678373336792,
      0.6569281816482544,
      0.6520318388938904,
      0.6479383111000061,
      0.642422080039978,
      0.6386691927909851,
      0.6323591470718384,
      0.6306580305099487,
      0.6228959560394287,
      0.6224337816238403,
      0.6150347590446472,
      0.6138465404510498,
      0.6070091128349304,
      0.605958878993988,
      0.6002120971679688,
      0.5981414914131165,
      0.5935503840446472,
      0.5913523435592651,
      0.5868738293647766,
      0.5835561752319336,
      0.5801799893379211,
      0.5768246054649353,
      0.5740537047386169,
      0.5704647302627563,
      0.5677310824394226,
      0.5638368725776672,
      0.5609622597694397,
      0.5576741695404053,
      0.5543953776359558,
      0.5519732236862183,
      0.5490577816963196,
      0.5464398264884949,
      0.5434485077857971,
      0.5407573580741882,
      0.5383802652359009,
      0.5358864665031433,
      0.5330765843391418,
      0.5307280421257019],
     'val_accuracy': [0.22499999403953552,
      0.25833332538604736,
      0.23333333432674408,
      0.28333333134651184,
      0.3166666626930237,
      0.3333333432674408,
      0.375,
      0.4000000059604645,
      0.38333332538604736,
      0.3916666805744171,
      0.4333333373069763,
      0.44999998807907104,
      0.4583333432674408,
      0.4749999940395355,
      0.49166667461395264,
      0.5083333253860474,
      0.5166666507720947,
      0.5249999761581421,
      0.5249999761581421,
      0.5333333611488342,
      0.5249999761581421,
      0.5583333373069763,
      0.574999988079071,
      0.574999988079071,
      0.6000000238418579,
      0.6000000238418579,
      0.6000000238418579,
      0.6000000238418579,
      0.6000000238418579,
      0.6166666746139526,
      0.625,
      0.6666666865348816,
      0.6666666865348816,
      0.6416666507720947,
      0.6666666865348816,
      0.6583333611488342,
      0.6583333611488342,
      0.6666666865348816,
      0.6666666865348816,
      0.675000011920929,
      0.675000011920929,
      0.675000011920929,
      0.6833333373069763,
      0.6916666626930237,
      0.699999988079071,
      0.699999988079071,
      0.699999988079071,
      0.699999988079071,
      0.7083333134651184,
      0.7083333134651184,
      0.699999988079071,
      0.6916666626930237,
      0.7083333134651184,
      0.7083333134651184,
      0.7083333134651184,
      0.7083333134651184,
      0.7083333134651184,
      0.7166666388511658,
      0.7250000238418579,
      0.7333333492279053,
      0.7416666746139526,
      0.7416666746139526,
      0.75,
      0.75,
      0.7583333253860474,
      0.7583333253860474,
      0.7666666507720947,
      0.7749999761581421,
      0.7833333611488342,
      0.7916666865348816,
      0.7833333611488342,
      0.7916666865348816,
      0.8166666626930237,
      0.800000011920929,
      0.824999988079071,
      0.8083333373069763,
      0.824999988079071,
      0.8166666626930237,
      0.824999988079071,
      0.8166666626930237,
      0.824999988079071,
      0.8166666626930237,
      0.824999988079071,
      0.8166666626930237,
      0.8166666626930237,
      0.8166666626930237,
      0.8166666626930237,
      0.8166666626930237,
      0.8166666626930237,
      0.8166666626930237,
      0.8166666626930237,
      0.8166666626930237,
      0.8166666626930237,
      0.8166666626930237,
      0.8166666626930237,
      0.8166666626930237,
      0.8166666626930237,
      0.8166666626930237,
      0.8166666626930237,
      0.8166666626930237]}



Now visualize the loss over time using `history.history`: 


```python
# The history.history["loss"] entry is a dictionary with as many values as epochs that the
# model was trained on. 
df_loss_acc = pd.DataFrame(history.history)
df_loss= df_loss_acc[['loss','val_loss']]
df_loss.rename(columns={'loss':'train','val_loss':'validation'},inplace=True)
df_acc= df_loss_acc[['accuracy','val_accuracy']]
df_acc.rename(columns={'accuracy':'train','val_accuracy':'validation'},inplace=True)
df_loss.plot(title='Model loss',figsize=(12,8)).set(xlabel='Epoch',ylabel='Loss')
df_acc.plot(title='Model Accuracy',figsize=(12,8)).set(xlabel='Epoch',ylabel='Accuracy')
```




    [Text(0, 0.5, 'Accuracy'), Text(0.5, 0, 'Epoch')]




![png](output_41_1.png)



![png](output_41_2.png)


**Congratulations**! You've finished the assignment and built two models: One that recognizes  smiles, and another that recognizes SIGN language with almost 80% accuracy on the test set. In addition to that, you now also understand the applications of two Keras APIs: Sequential and Functional. Nicely done! 

By now, you know a bit about how the Functional API works and may have glimpsed the possibilities. In your next assignment, you'll really get a feel for its power when you get the opportunity to build a very deep ConvNet, using ResNets! 

<a name='6'></a>
## 6 - Bibliography

You're always encouraged to read the official documentation. To that end, you can find the docs for the Sequential and Functional APIs here: 

https://www.tensorflow.org/guide/keras/sequential_model

https://www.tensorflow.org/guide/keras/functional
