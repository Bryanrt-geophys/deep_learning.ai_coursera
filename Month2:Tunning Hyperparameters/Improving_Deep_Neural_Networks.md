# Improving Deep Neural Networks

## Week 1

\#\#Setting up your ML applications

Historically, it has been acceptable to split avaible data into a 70/30
or 60/20/20 for train/test or train/dev/test. With larger data sets such
as 1M samples, it is acceptable to set up mach more skewed ratios. We
are now seeing training on up to 98% and dev and test on 1% each. We
will dive deaper into how to determine the ratios further on. In general
it appears that smaller dat sets require a more distributed
train/dev/test ratio.

## Bies/Variance

We can track the error for both the training set and the dev set to aid
in determining model fit. If there is low training error but high error
in the dev/test then this is a strong indicator of model overfitting.
The biggest issue is variance between the error in training and
dev/test. If there is high error in the training set then it is termed
as high bias. This means that the agent is not being trained well. There
is an optimal error called Bayes Error and that is  ≈ 0 in most cases
based upon how well a human eye can perform.

## Basic Recipe for Machine Learning

Does your network have high bias? 1. Make the network bigger 2. Make the
training partition larger

Does the network have high variance? 1. Use more data 2. Perform
regularization (will be discussed later on)

## Regularization

We we work up to regularization founding from logistic regression:

$$J(w,b) =\\frac{1}{m} \\sum\\limits\_{i = 1}^{m} L \\large{(} \\hat{y}^i,y^i \\large{)} \\small\\tag{1}$$

to regularize this we add a euclidean norm to the term *w*,

$$J(w,b) =\\frac{1}{m} \\sum\\limits\_{i = 1}^{m} L \\large{(} \\hat{y}^i,y^i \\large{)} + \\frac{\\lambda}{2m} \* \|\|w\|\|\_{2}^2 \\small\\tag{2}$$

where
$$\|\|w\|\|\_{2}^2 = \\sum\\limits\_{j = 1}^{nx} w\_{j}^2 = w^T \* w \\small\\tag{3}$$

This is termed the *L*<sub>2</sub> regularization. We could add a
similar addition to the equation in regards to *b*, but the value of *b*
is usualy a very small scaler in comparison to *w* that this becomes
negligible. There is also an *L*<sub>1</sub> regularization:

$$\\frac{\\lambda}{2m}\\sum\\limits\_{j = 1}^{nx}\|w_j\|=\\frac{\\lambda}{2m}\|\|w\|\|\_l \\small\\tag{4}$$

*L*<sub>1</sub> regularization is often used to speed up the training of
a model but only in particular circumstances and *L*<sub>2</sub>
regularization is used much more often.

There is an implementation of *L*<sub>*n*</sub> regularization the to
the backwards pass that we will discuss later on. Now we will cover how
regularization helps avoid over fitting.

## Why does regularization redcuce overfitting

The over all concept is that we increase the value of *λ* which shrinks
the value of *d**w*, this then makes the iteration of each node a linear
function in the activation function.

## Droupout regularization

Drop out basically goes through your network and randomly kills nodes
making the over all network smaller.

# Assignment 1

## Initialization

Welcome to the first assignment of Improving Deep Neural Networks!

Training your neural network requires specifying an initial value of the
weights. A well-chosen initialization method helps the learning process.

If you completed the previous course of this specialization, you
probably followed the instructions for weight initialization, and seen
that it’s worked pretty well so far. But how do you choose the
initialization for a new neural network? In this notebook, you’ll try
out a few different initializations, including random, zeros, and He
initialization, and see how each leads to different results.

A well-chosen initialization can: - Speed up the convergence of gradient
descent - Increase the odds of gradient descent converging to a lower
training (and generalization) error

Let’s get started!

``` python
# import numpy as np
# import matplotlib.pyplot as plt
# import sklearn
# import sklearn.datasets
# from public_tests import *
# from init_utils import sigmoid, relu, compute_loss, forward_propagation, backward_propagation
# from init_utils import update_parameters, predict, load_dataset, plot_decision_boundary, predict_dec
# 
# %matplotlib inline
# plt.rcParams['figure.figsize'] = (7.0, 4.0) # set default size of plots
# plt.rcParams['image.interpolation'] = 'nearest'
# plt.rcParams['image.cmap'] = 'gray'
# 
# %load_ext autoreload
# %autoreload 2

# load image dataset: blue/red dots in circles
# train_X, train_Y, test_X, test_Y = load_dataset()
```

``` r
r1 <- c(-0.07741354,  0.4318696 ,  0.07916502, -0.63092101, -0.27309979,
         0.56041045,  0.3004088 , -1.02619383,  0.4091552 , -0.57893745,
         0.08883958,  1.08049475,  0.79621216, -0.75325643,  0.40141942,
        -1.06363963,  0.97105429,  0.62636161, -0.74454495,  0.40696389,
         0.31794403, -0.18986535, -0.3202672 ,  0.89684523, -0.36046502,
         0.6356461 ,  0.80846993, -0.79511529,  0.7199568 ,  0.12140435,
         0.91180088, -0.44948709, -0.77000839, -0.88770182, -0.54477653,
        -0.8316329 , -0.65033638,  0.60691411, -1.08300963, -0.73197312,
        -0.76135268, -0.80494813, -0.12265116,  1.07446596, -1.01147061,
        -0.8069398 , -0.49609826,  0.71846716,  0.72364227, -0.56610138,
         0.95366545,  0.58716011,  0.72179418,  0.45491191,  0.76716688,
         0.33177932, -0.90111592,  0.28401209, -0.66035814, -0.10968083,
         0.77733173,  0.87450385, -0.44392507,  0.73199223,  0.03410239,
        -0.5223324 , -0.05452236,  0.5028093 , -0.8694272 , -0.61710145,
         0.50897307,  0.72492835, -0.0590278 ,  0.68122582, -0.18367368,
        -0.1590936 ,  0.74024244,  0.96003791, -0.88005402,  0.03776895,
         0.48029581, -0.89671317, -0.15258015, -0.27811292,  0.14253938,
         0.1857096 ,  0.71514702, -0.61960592, -0.23979509,  0.5515506 ,
        -0.29517626,  0.7007139 ,  0.81433048, -0.74479175,  0.8103016 ,
         0.50999505, -0.28700212,  0.57399869, -0.67191598,  0.33837064,
        -0.1658805 , -0.11004509, -1.08753284,  0.61463961,  0.85302562,
        -0.05510018, -0.04895929,  0.19636794, -0.54294525,  0.91017996,
        -0.8062305 ,  0.14639117,  0.7482129 ,  0.33156866,  0.53901877,
        -0.64755912,  0.36319894, -0.681617  ,  0.73839863, -0.41222929,
        -0.64936564,  0.75396149, -0.67037719,  0.20012277, -0.69682989,
         1.05830261,  0.08857835, -0.51589466, -0.88813747, -0.85295037,
        -0.83302402, -0.84831712,  0.73588257, -0.01436795, -0.42774855,
        -0.08102955, -0.41305539, -0.7630302 , -0.56004587, -0.09403781,
         0.70447567, -0.54441998, -0.87379359, -0.55742589,  0.8863315 ,
         0.58407116, -0.69992603,  0.72455152, -0.49426957,  0.19137628,
         0.76321015,  0.92450263, -0.79690752,  0.78218459,  0.33521523,
        -0.68800513, -0.92450666,  0.47194606,  0.46727993, -0.98346024,
         0.32478679,  0.00888484,  0.23802817, -0.33621059,  0.2514115 ,
        -0.39223466,  0.89774478,  0.84581646, -0.10478863, -0.66172529,
         0.13758305,  0.24751441, -0.23048789, -0.14914559, -0.61010367,
        -0.67436094,  0.68319297, -0.59844286,  0.21384268, -1.03042551,
         0.09043719, -0.79619304, -0.80374527,  0.95380068, -0.77755079,
         1.08993706,  0.76876138,  0.25267253,  0.59443175, -0.95302953,
         0.43955864, -0.79100692, -0.73810879,  0.81936253, -0.59642055,
        -0.67096895, -0.24613473, -0.67666035, -0.44231394, -0.03457233,
         0.22792863,  0.74189658,  0.44295887, -0.65685464, -0.98977222,
        -0.39860383, -0.70444375,  0.46263378,  0.78391871,  1.01994366,
         0.05884382,  1.0286041 , -0.2868607 , -0.58218065,  0.98186621,
        -0.61578055,  0.59944524,  0.08259224, -1.00892939, -0.02432911,
        -0.98255682,  0.21936859,  0.87263697,  0.36080072,  0.81744523,
        -0.37567843, -0.10032117, -1.02532387, -0.70722744, -0.4402178 ,
        -0.19861158, -0.94539348,  0.90367331, -0.5656519 ,  0.87271263,
         0.71805915,  0.22208188,  0.82268494,  0.7064093 , -0.91126765,
        -0.67086734,  0.62233709,  0.80480676, -0.44676233,  0.54212919,
         0.55061585,  0.46281699, -0.76679431, -0.20562559,  0.87790637,
         0.68669626, -0.35741066,  0.6738096 , -0.07920862,  0.74659379,
         0.73626137, -0.70349914,  0.50158846, -0.24078764, -0.83967975,
         0.80485307, -0.19506866,  0.50963354, -1.12388444, -0.17409384,
         1.01624181, -0.48049357,  0.75708405,  0.31663919,  0.29375988,
         0.13422331,  0.53920887, -0.59060646,  0.05368671, -0.68765197,
         0.89935198,  0.62453904,  1.02011913,  0.97848163, -0.95924174,
        -0.62310911, -0.72950845, -0.95387034, -0.47692855,  0.86463496,
        -0.34204923, -0.33829108,  0.37965629,  0.30108182,  0.60930598,
        -0.82434521, -1.00060825,  0.61816404,  0.97015292,  0.7569548 ,
        -0.50891254, -0.25642463, -1.02049886, -0.76910863, -0.06114541)

r2 <- c(0.85260226, -0.87214504,  0.8406611 ,  0.39628707, -1.05506135,
        -0.81904144,  0.80794739,  0.13308867, -0.64662067, -0.55199314,
        -0.78437921, -0.16472192, -0.02157918,  0.67033584, -0.86066486,
        -0.08261109,  0.47080188,  0.55190586,  0.18696386,  0.8766575 ,
         0.85989367, -1.07177575, -0.78440865,  0.47873001, -0.9207095 ,
         0.64088108, -0.08869139, -0.55328367, -0.27381731,  0.84952799,
        -0.37756929, -0.57005013, -0.05950221,  0.5984764 ,  0.81267211,
        -0.54225516, -0.7057424 ,  0.4548938 , -0.23034322,  0.19316441,
         0.12103925, -0.08128199,  0.78408686,  0.16660781,  0.16931989,
         0.1556307 , -0.6459172 ,  0.30807194,  0.61719558,  0.63204536,
         0.02009226, -0.4609066 , -0.46516935,  0.69857809, -0.336245  ,
        -0.94003396,  0.56214643,  0.69327711,  0.32638799,  0.76495923,
         0.2650044 ,  0.12988954, -0.95067378,  0.39685277, -0.94423554,
        -0.68283183,  0.99940395,  0.55637524, -0.44867776, -0.55093215,
        -0.54544382,  0.70066647, -1.01705102, -0.66119181,  1.06543448,
        -0.97976582, -0.17180463,  0.19783286, -0.56199655,  1.02570727,
         0.71363377,  0.30715461, -0.78428247, -0.95592575,  0.95220053,
         0.84185343, -0.24398812, -0.70787134,  0.73146942, -0.62644698,
        -0.78435489,  0.70115583,  0.53302118, -0.60940508, -0.12123592,
         0.51863882,  0.97916652, -0.62309822, -0.50023613, -0.96431462,
        -0.88990955,  0.99028171, -0.15390161, -0.46717846, -0.38781869,
         0.84374734, -0.80655967, -1.0804179 , -0.80401293,  0.26175142,
        -0.07290262,  0.72555194, -0.74299002,  0.94604961,  0.57105477,
         0.66768021,  0.91357462, -0.41346846,  0.54574092,  0.84249953,
        -0.43934839,  0.3370899 ,  0.40633248,  0.97894836, -0.65803477,
        -0.10993927,  0.99376454,  0.5059924 ,  0.56852498,  0.06038779,
        -0.05728115,  0.37576139,  0.12831757,  0.70083076, -0.60123095,
        -0.78158995,  1.0158679 ,  0.2329609 ,  0.56156315,  1.00593075,
         0.82809028, -0.85801078,  0.28210058,  0.77696608, -0.55621162,
         0.80725522, -0.24670893, -0.65140286,  0.70957607, -0.9460593 ,
        -0.10006048, -0.34560326, -0.17006576,  0.19483032, -0.74146754,
         0.73749696,  0.57863707,  0.69795474,  0.67892754, -0.3712797 ,
         0.65542363, -1.01264551, -0.71974424, -0.65672593, -1.0396553 ,
         0.74106604, -0.16972767, -0.21766826, -0.85771092,  0.75894303,
        -0.74743147,  0.91335388,  0.75129261,  0.74519881, -0.88685995,
         0.40088633, -0.80584586, -0.86500991, -0.73795939, -0.27585252,
         0.84516973,  0.57632585, -0.65112434, -0.35872132,  0.06751113,
        -0.07614258,  0.13068917, -0.73004313, -0.35976061, -0.23790416,
        -0.90252064, -0.31327717, -0.24381186,  0.49679419, -0.84607939,
        -0.56820357,  0.94034739,  0.50301472, -0.88743752,  0.98081048,
        -0.76365232,  0.72976335, -0.75205055,  0.41726614,  0.03968898,
         0.77205756,  0.23087721, -0.69268125, -0.13160683, -0.06015894,
        -0.85190289,  0.08655825,  0.7436401 ,  0.79392502,  0.26055783,
         0.57469157, -0.72606171, -0.74735411, -0.02919451, -0.91835936,
         0.40801307,  0.80103893,  0.03527981, -0.70170655,  0.27057861,
         0.88856937,  0.73577936, -0.03091382, -0.67080995,  0.7425196 ,
        -0.7092934 , -0.53975461, -0.24326524, -0.54881519,  0.55589421,
         0.32248648,  1.01990804, -0.53385222,  0.54431178, -0.33515462,
         0.65194495,  0.41903193,  0.12978639, -0.80973456, -0.92003465,
         0.74841353, -0.90895441, -0.22607534,  0.83674963,  0.39143249,
        -0.47389432, -0.92678537,  0.03758088, -0.80032472, -0.62908954,
        -0.33311845, -0.09917238,  0.80744359,  0.72833191,  0.51667169,
         0.5346606 , -0.96886573,  0.98438486, -0.09619976,  0.98896903,
         0.20167879,  0.81930245, -0.33659181,  0.93461649, -0.86123609,
        -1.03200709, -0.66723963,  0.22228326, -0.80971383,  0.4634437 ,
        -0.01424933,  0.79527356,  0.25799428, -0.36151996, -0.48084547,
        -0.46618721,  0.29028551,  0.26630891,  0.80973265,  0.1782114 ,
        -0.64714634, -0.74496944, -0.6933507 ,  0.82564066, -0.57814385,
        -0.34794367,  0.15887834, -0.77383787, -0.34210418, -0.55041192,
         0.65684298, -0.79893889,  0.15267141, -0.27651787,  0.92982641)

train_X <- as.data.frame(r1, .rows = 1) %>% mutate(r2 = r2)
```

``` r
r1= c(-0.52423739, -0.85089226,  0.55408395, -1.00796424, -0.34669891,
        -0.87955774,  0.91083038, -0.9633434 , -0.90812984,  0.00505111,
         0.81876616,  0.38848922, -0.70201694, -0.20107371,  1.03096393,
        -0.86358175,  0.8313737 , -0.9529605 ,  0.14688495, -0.72055438,
        -0.30602062,  0.2010009 , -0.42844865,  0.69365889, -0.88010462,
         0.78610504, -0.97444882, -0.76774242, -0.75019672,  0.03222771,
        -0.20825378,  0.9683679 , -0.5393082 ,  0.96744687, -0.14248613,
        -0.65935517,  0.68947439,  0.11056662,  0.31809833,  0.36697835,
         0.05747104,  0.73495737, -0.03241976,  0.59550423,  0.65635678,
        -0.57384001, -0.67317837, -0.97234046,  0.22836302,  0.67363603,
         0.49729124,  0.76296644, -0.49092098,  0.30516018,  0.50070708,
         0.40445966, -0.09275565,  0.8825487 ,  0.7761214 ,  0.61229504,
        -0.71562153,  0.60760444,  0.27593752,  0.83695342, -0.21134389,
        -0.66204846,  0.77257346,  0.7387058 , -0.26795427,  0.03257218,
        -0.61022046,  0.81208283, -0.5590253 , -0.53184926, -0.55767686,
         0.45651028, -0.54903165,  0.02237129,  0.80784519,  0.41637853,
         0.87151392, -0.47725915,  0.14441319, -0.03146913, -0.83929396,
        -0.6956208 ,  0.07799864,  0.87779014, -0.29131909,  0.49487107,
         1.00552343, -0.4426311 ,  0.67287945, -0.84344274, -0.61252941,
         0.65734324, -0.86344404, -0.86781094, -0.27136661,  0.34384837)
      r2 <- c(-0.60910361, -0.52145127,  0.48711084,  0.16920318,  0.90577515,
         0.18864212,  0.17151744, -0.23220728, -0.38357248,  1.01995445,
        -0.09854543, -0.54543244, -0.17330788,  0.9412882 , -0.02864398,
         0.55133332,  0.3882002 , -0.51714119,  0.82216753, -0.36147523,
        -1.01656975,  1.0004595 , -0.71228123, -0.70689621,  0.20240547,
         0.56017235,  0.03902201, -0.22487214,  0.07054547,  0.80013588,
         0.75046483,  0.12280635,  0.69594334, -0.24001988, -0.99884018,
        -0.29967654,  0.74148259, -0.75638307, -0.6933004 ,  1.00996269,
         1.00954725,  0.38023541, -0.81665179,  0.41413379, -0.50106171,
        -0.73870357,  0.67542512, -0.13036794,  0.83222426,  0.36644591,
         0.88742276, -0.33161235,  0.75178915,  0.71719543,  0.60283577,
        -0.86970349,  0.74710372, -0.61381526, -0.31477192,  0.76301802,
         0.36975415, -0.55154942, -0.79804142, -0.26483757, -0.75451842,
        -0.38938463, -0.03631208,  0.27378697,  0.68874276, -0.86696403,
         0.49315322, -0.5518834 ,  0.53076141,  0.55011684, -0.58527725,
         0.65200933, -0.88352622, -0.8751386 ,  0.09623264, -0.80453245,
         0.45022116,  0.63627868, -0.94930644, -1.04727573,  0.56654668,
        -0.73765882,  0.75345288, -0.37998424, -0.78185025, -0.55807654,
        -0.13423404, -1.00162078,  0.7218244 , -0.00547225, -0.6089971 ,
        -0.74087879,  0.33222701,  0.30237648,  0.9211622 , -0.91927359)
        
test_X <- as.data.frame(r1, .rows = 1) %>% mutate(r2 = r2)
```

``` r
train_Y = c(1, 0, 1, 1, 0, 0, 1, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0, 1, 1, 0, 0, 0,
        1, 0, 0, 0, 1, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 0,
        0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1,
        0, 1, 0, 1, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1, 0, 0, 1, 1, 1,
        1, 1, 1, 0, 0, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 1, 0, 1, 1, 0, 0, 0,
        1, 1, 0, 0, 1, 0, 0, 1, 1, 0, 1, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1, 0,
        1, 1, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 1,
        1, 0, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1,
        0, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 1, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1,
        0, 0, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 1, 0, 0, 1, 0, 1, 0, 0,
        0, 1, 1, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 0, 1, 0, 0, 1,
        1, 0, 0, 0, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 1, 0, 1, 0, 0, 0, 0, 0,
        0, 0, 0, 1, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 1, 1,
        1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 0, 1, 0)
```

``` r
test_Y = c(1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0,
        1, 0, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 1,
        1, 0, 0, 0, 1, 1, 0, 1, 0, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0,
        1, 1, 0, 0, 0, 1, 1, 0, 0, 1, 0, 0)
```

``` r
train_Xr <- train_X %>% 
  mutate(classifier = train_Y) %>%
  as.data.frame()

test_Xr <- test_X %>% 
  mutate(classifier = test_Y) %>%
  as.data.frame()

ggplot() +
  geom_point(aes(x = train_Xr[,1], y = train_Xr[,2], color = train_Xr[,3])) + 
  geom_point(aes(x = test_Xr[,1], y = test_Xr[,2], color = test_Xr[,3]))
```

![](Improving_Deep_Neural_Networks_files/figure-markdown_github/unnamed-chunk-6-1.png)

## Neural Networks

You’ll use a 3-layer neural network (already implemented for you). These
are the initialization methods you’ll experiment with: - *Zeros
initialization* – setting `initialization = "zeros"` in the input
argument. - *Random initialization* – setting
`initialization = "random"` in the input argument. This initializes the
weights to large random values.  
- *He initialization* – setting `initialization = "he"` in the input
argument. This initializes the weights to random values scaled according
to a paper by He et al., 2015.

**Instructions**: Instructions: Read over the code below, and run it. In
the next part, you’ll implement the three initialization methods that
this `model()` calls.

``` python
def model(X, Y, learning_rate = 0.01, num_iterations = 15000, print_cost = True, initialization = "he"):
    """
    Implements a three-layer neural network: LINEAR->RELU->LINEAR->RELU->LINEAR->SIGMOID.
    Arguments:
    X -- input data, of shape (2, number of examples)
    Y -- true "label" vector (containing 0 for red dots; 1 for blue dots), of shape (1, number of examples)
    learning_rate -- learning rate for gradient descent 
    num_iterations -- number of iterations to run gradient descent
    print_cost -- if True, print the cost every 1000 iterations
    initialization -- flag to choose which initialization to use ("zeros","random" or "he")
    Returns:
    parameters -- parameters learnt by the model
    """
    grads = {}
    costs = [] # to keep track of the loss
    m = X.shape[1] # number of examples
    layers_dims = [X.shape[0], 10, 5, 1]
    # Initialize parameters dictionary.
    if initialization == "zeros":
        parameters = initialize_parameters_zeros(layers_dims)
    elif initialization == "random":
        parameters = initialize_parameters_random(layers_dims)
    elif initialization == "he":
        parameters = initialize_parameters_he(layers_dims)
    # Loop (gradient descent)
    for i in range(num_iterations):
        # Forward propagation: LINEAR -> RELU -> LINEAR -> RELU -> LINEAR -> SIGMOID.
        a3, cache = forward_propagation(X, parameters)
        # Loss
        cost = compute_loss(a3, Y)
        # Backward propagation.
        grads = backward_propagation(X, Y, cache)
        # Update parameters.
        parameters = update_parameters(parameters, grads, learning_rate)
        # Print the loss every 1000 iterations
        if print_cost and i % 1000 == 0:
            print("Cost after iteration {}: {}".format(i, cost))
            costs.append(cost)
    # plot the loss
    plt.plot(costs)
    plt.ylabel('cost')
    plt.xlabel('iterations (per hundreds)')
    plt.title("Learning rate =" + str(learning_rate))
    plt.show()
    return parameters
```

Zero Initialization

There are two types of parameters to initialize in a neural network: -
the weight matrices
(*W*<sup>\[1\]</sup>, *W*<sup>\[2\]</sup>, *W*<sup>\[3\]</sup>, ..., *W*<sup>\[*L* − 1\]</sup>, *W*<sup>\[*L*\]</sup>)
- the bias vectors
(*b*<sup>\[1\]</sup>, *b*<sup>\[2\]</sup>, *b*<sup>\[3\]</sup>, ..., *b*<sup>\[*L* − 1\]</sup>, *b*<sup>\[*L*\]</sup>)

<a name='ex-1'></a> \#\#\# Exercise 1 - initialize_parameters_zeros

Implement the following function to initialize all parameters to zeros.
You’ll see later that this does not work well since it fails to “break
symmetry,” but try it anyway and see what happens. Use
`np.zeros((..,..))` with the correct shapes.

``` python
# GRADED FUNCTION: initialize_parameters_zeros 
def initialize_parameters_zeros(layers_dims):
    """
    Arguments:
    layer_dims -- python array (list) containing the size of each layer.
    Returns:
    parameters -- python dictionary containing your parameters "W1", "b1", ..., "WL", "bL":
                    W1 -- weight matrix of shape (layers_dims[1], layers_dims[0])
                    b1 -- bias vector of shape (layers_dims[1], 1)
                    ...
                    WL -- weight matrix of shape (layers_dims[L], layers_dims[L-1])
                    bL -- bias vector of shape (layers_dims[L], 1)
    """
    parameters = {}
    L = len(layers_dims)            # number of layers in the network
    for l in range(1, L):
        #(≈ 2 lines of code)
        # parameters['W' + str(l)] = 
        # parameters['b' + str(l)] = 
        # YOUR CODE STARTS HERE
        parameters['W' + str(l)] = np.zeros((layers_dims[l], layers_dims[l-1]))
        parameters['b' + str(l)] = np.zeros((layers_dims[l],1)) 
        # YOUR CODE ENDS HERE
    return parameters
```

Running a training with the parameters initialized to zero will yield
zero cost variation over time.

![](/Users/camithomas/Desktop/Bryan/deep_learning.ai_coursera/Month2:Tunning%20Hyperparameters/zero_init.png)
**Note**: For sake of simplicity calculations below are done using only
one example at a time.

Since the weights and biases are zero, multiplying by the weights
creates the zero vector which gives 0 when the activation function is
ReLU. As `z = 0`

*a* = *R**e**L**U*(*z*) = *m**a**x*(0, *z*) = 0

At the classification layer, where the activation function is sigmoid
you then get (for either input):

$$\\sigma(z) = \\frac{1}{ 1 + e^{-(z)}} = \\frac{1}{2} = y\_{pred}$$

As for every example you are getting a 0.5 chance of it being true our
cost function becomes helpless in adjusting the weights.

Your loss function:
ℒ(*a*, *y*) =  − *y*ln (*y*<sub>*p**r**e**d*</sub>) − (1 − *y*)ln (1 − *y*<sub>*p**r**e**d*</sub>)

For `y=1`, `y_pred=0.5` it becomes:

$$ \\mathcal{L}(0, 1) =  - (1)  \\ln(\\frac{1}{2}) = 0.6931471805599453$$

For `y=0`, `y_pred=0.5` it becomes:

$$ \\mathcal{L}(0, 0) =  - (1)  \\ln(\\frac{1}{2}) = 0.6931471805599453$$

As you can see with the prediction being 0.5 whether the actual (`y`)
value is 1 or 0 you get the same loss value for both, so none of the
weights get adjusted and you are stuck with the same old value of the
weights.

This is why you can see that the model is predicting 0 for every
example! No wonder it’s doing so badly.

In general, initializing all the weights to zero results in the network
failing to break symmetry. This means that every neuron in each layer
will learn the same thing, so you might as well be training a neural
network with *n*<sup>\[*l*\]</sup> = 1 for every layer. This way, the
network is no more powerful than a linear classifier like logistic
regression.

Random Initialization

To break symmetry, initialize the weights randomly. Following random
initialization, each neuron can then proceed to learn a different
function of its inputs. In this exercise, you’ll see what happens when
the weights are initialized randomly, but to very large values.

<a name='ex-2'></a> \#\#\# Exercise 2 - initialize_parameters_random

Implement the following function to initialize your weights to large
random values (scaled by \*10) and your biases to zeros. Use
`np.random.randn(..,..) * 10` for weights and `np.zeros((.., ..))` for
biases. You’re using a fixed `np.random.seed(..)` to make sure your
“random” weights match ours, so don’t worry if running your code several
times always gives you the same initial values for the parameters.

``` python
# GRADED FUNCTION: initialize_parameters_random
def initialize_parameters_random(layers_dims):
    """
    Arguments:
    layer_dims -- python array (list) containing the size of each layer.
    Returns:
    parameters -- python dictionary containing your parameters "W1", "b1", ..., "WL", "bL":
                    W1 -- weight matrix of shape (layers_dims[1], layers_dims[0])
                    b1 -- bias vector of shape (layers_dims[1], 1)
                    ...
                    WL -- weight matrix of shape (layers_dims[L], layers_dims[L-1])
                    bL -- bias vector of shape (layers_dims[L], 1)
    """
    np.random.seed(3)               # This seed makes sure your "random" numbers will be the as ours
    parameters = {}
    L = len(layers_dims)            # integer representing the number of layers
    for l in range(1, L):
        #(≈ 2 lines of code)
        # parameters['W' + str(l)] = 
        # parameters['b' + str(l)] =
        # YOUR CODE STARTS HERE
        parameters['W' + str(l)] = np.random.randn(layers_dims[l],layers_dims[l-1]) * 10
        parameters['b' + str(l)] = np.zeros((layers_dims[l],1))
        # YOUR CODE ENDS HERE
    return parameters
```

![](/Users/camithomas/Desktop/Bryan/deep_learning.ai_coursera/Month2:Tunning%20Hyperparameters/rand_init.png)

This model attempts to predict the boundaries and does a semi-decent job

![](/Users/camithomas/Desktop/Bryan/deep_learning.ai_coursera/Month2:Tunning%20Hyperparameters/first_bound.png)

He Initialization

Finally, try “He Initialization”; this is named for the first author of
He et al., 2015. (If you have heard of “Xavier initialization”, this is
similar except Xavier initialization uses a scaling factor for the
weights *W*<sup>\[*l*\]</sup> of `sqrt(1./layers_dims[l-1])` where He
initialization would use `sqrt(2./layers_dims[l-1])`.)

<a name='ex-3'></a> \#\#\# Exercise 3 - initialize_parameters_he

Implement the following function to initialize your parameters with He
initialization. This function is similar to the previous
`initialize_parameters_random(...)`. The only difference is that instead
of multiplying `np.random.randn(..,..)` by 10, you will multiply it by
$\\sqrt{\\frac{2}{\\text{dimension of the previous layer}}}$, which is
what He initialization recommends for layers with a ReLU activation.

``` python
# GRADED FUNCTION: initialize_parameters_he
def initialize_parameters_he(layers_dims):
    """
    Arguments:
    layer_dims -- python array (list) containing the size of each layer.
    Returns:
    parameters -- python dictionary containing your parameters "W1", "b1", ..., "WL", "bL":
                    W1 -- weight matrix of shape (layers_dims[1], layers_dims[0])
                    b1 -- bias vector of shape (layers_dims[1], 1)
                    ...
                    WL -- weight matrix of shape (layers_dims[L], layers_dims[L-1])
                    bL -- bias vector of shape (layers_dims[L], 1)
    """
    np.random.seed(3)
    parameters = {}
    L = len(layers_dims) - 1 # integer representing the number of layers
    for l in range(1, L + 1):
        #(≈ 2 lines of code)
        # parameters['W' + str(l)] = 
        # parameters['b' + str(l)] =
        # YOUR CODE STARTS HERE
        parameters['W' + str(l)] = np.random.randn(layers_dims[l],layers_dims[l-1]) * np.sqrt(2./layers_dims[l-1])
        parameters['b' + str(l)] = np.zeros((layers_dims[l],1))
        # YOUR CODE ENDS HERE
    return parameters
```

The provided training function yields the fallowing loss curve

![](/Users/camithomas/Desktop/Bryan/deep_learning.ai_coursera/Month2:Tunning%20Hyperparameters/He_init.png)

Plotting the prediction boundary from the He initialization shows a much
more accurate classification. This method is prefered to random
initialization.

![](/Users/camithomas/Desktop/Bryan/deep_learning.ai_coursera/Month2:Tunning%20Hyperparameters/He_bound.png)

# Assigment 2: Regularization

Welcome to the second assignment of this week. Deep Learning models have
so much flexibility and capacity that **overfitting can be a serious
problem**, if the training dataset is not big enough. Sure it does well
on the training set, but the learned network **doesn’t generalize to new
examples** that it has never seen!

**You will learn to:** Use regularization in your deep learning models.

Let’s get started!

``` python
# import packages
# import numpy as np
# import matplotlib.pyplot as plt
# import sklearn
# import sklearn.datasets
# import scipy.io
# from reg_utils import sigmoid, relu, plot_decision_boundary, initialize_parameters, load_2D_dataset, predict_dec
# from reg_utils import compute_cost, predict, forward_propagation, backward_propagation, update_parameters
# from testCases import *
# from public_tests import *
# 
# %matplotlib inline
# plt.rcParams['figure.figsize'] = (7.0, 4.0) # set default size of plots
# plt.rcParams['image.interpolation'] = 'nearest'
# plt.rcParams['image.cmap'] = 'gray'
# 
# %load_ext autoreload
# %autoreload 2
```

## Problem Statement

You have just been hired as an AI expert by the French Football
Corporation. They would like you to recommend positions where France’s
goal keeper should kick the ball so that the French team’s players can
then hit it with their head.

![](/Users/camithomas/Desktop/Bryan/deep_learning.ai_coursera/Month2:Tunning%20Hyperparameters/)

The goal keeper kicks the ball in the air, the players of each team are
fighting to hit the ball with their head They give you the following 2D
dataset from France’s past 10 games.

## Loading the Dataset

Each dot corresponds to a position on the football field where a
football player has hit the ball with his/her head after the French goal
keeper has shot the ball from the left side of the football field. - If
the dot is blue, it means the French player managed to hit the ball with
his/her head - If the dot is red, it means the other team’s player hit
the ball with their head

**Your goal**: Use a deep learning model to find the positions on the
field where the goalkeeper should kick the ball.

**Analysis of the dataset**: This dataset is a little noisy, but it
looks like a diagonal line separating the upper left half (blue) from
the lower right half (red) would work well.

You will first try a non-regularized model. Then you’ll learn how to
regularize it and decide which model you will choose to solve the French
Football Corporation’s problem.

``` r
r1 <- c(-1.58986e-01, -3.47926e-01, -5.04608e-01, -5.96774e-01,
        -5.18433e-01, -2.92627e-01, -1.58986e-01, -5.76037e-02,
        -7.14286e-02, -2.97235e-01, -4.17051e-01, -4.40092e-01,
        -3.24885e-01, -2.46544e-01, -2.18894e-01, -3.43318e-01,
        -5.09217e-01, -3.84793e-01, -1.49770e-01, -1.95853e-01,
        -3.91705e-02, -1.08295e-01, -1.86636e-01, -2.18894e-01,
        -8.06452e-02,  6.68203e-02,  9.44700e-02,  1.86636e-01,
         6.22120e-02,  2.07373e-02,  2.99539e-02, -9.90783e-02,
        -6.91244e-03,  1.31336e-01,  2.32719e-01,  8.52535e-02,
        -1.31336e-01,  2.30415e-03,  1.22120e-01, -3.47926e-01,
        -2.28111e-01, -7.60369e-02,  4.37788e-02,  1.15207e-02,
        -4.17051e-01, -3.15668e-01,  1.26728e-01,  2.05069e-01,
         2.18894e-01,  7.14286e-02, -1.31336e-01, -2.09677e-01,
        -2.28111e-01, -1.45161e-01, -6.68203e-02,  1.35945e-01,
         2.69585e-01,  2.97235e-01,  2.74194e-01,  2.55760e-01,
         2.23502e-01,  1.82028e-01,  1.58986e-01,  7.14286e-02,
         1.61290e-02, -2.53456e-02, -1.15207e-02, -2.30415e-03,
         2.53456e-02,  2.53456e-02,  1.15207e-02, -4.83871e-02,
        -8.52535e-02, -9.90783e-02, -1.61290e-02,  1.31336e-01,
         2.23502e-01,  2.92627e-01,  2.60369e-01,  2.00461e-01,
         1.72811e-01, -1.31336e-01, -1.49770e-01, -2.41935e-01,
        -3.01843e-01, -2.97235e-01, -2.74194e-01, -3.24885e-01,
        -3.98618e-01, -4.35484e-01, -4.72350e-01, -3.38710e-01,
        -2.69585e-01, -2.55760e-01, -1.68203e-01, -1.12903e-01,
        -3.91705e-02, -1.26728e-01, -2.32719e-01, -3.38710e-01,
        -4.12442e-01, -5.09217e-01, -5.41475e-01, -5.04608e-01,
        -4.90783e-01, -3.61751e-01, -2.69585e-01, -2.23502e-01,
        -1.86636e-01, -1.54378e-01, -1.12903e-01, -8.52535e-02,
        -8.52535e-02, -1.68203e-01, -1.91244e-01, -1.40553e-01,
        -2.99539e-02, -2.00461e-01, -1.08295e-01,  3.45622e-02,
         8.06452e-02, -3.85369e-01, -3.81221e-01, -3.52189e-01,
        -3.54263e-01, -4.14401e-01, -4.99424e-01, -2.98272e-01,
        -3.16935e-01, -3.68779e-01, -3.56336e-01, -2.71313e-01,
        -1.77995e-01, -2.46429e-01, -2.50576e-01, -2.21544e-01,
        -2.15323e-01, -1.30300e-01, -2.07028e-01, -9.71198e-02,
        -3.90553e-02,  1.90092e-02, -3.69816e-02, -6.39401e-02,
        -1.30300e-01, -3.75000e-01, -3.95737e-01, -3.54263e-01,
        -4.37212e-01, -4.80760e-01, -4.10253e-01, -2.48502e-01,
        -2.27765e-01, -2.83756e-01, -2.92051e-01, -3.37673e-01,
        -2.77535e-01, -2.07028e-01, -1.86290e-01, -1.32373e-01,
        -1.77995e-01, -1.65553e-01, -1.61406e-01,  3.45622e-04,
         7.91475e-02, -2.66129e-02, -5.35714e-02, -1.41705e-02,
        -7.01613e-02, -6.39401e-02, -3.07604e-02, -5.77189e-02,
        -5.35714e-02,  5.21889e-02, -1.62442e-02, -6.39401e-02,
        -6.18664e-02, -3.80184e-03,  4.18203e-02,  7.91475e-02,
         4.59677e-02,  1.18548e-01,  1.10253e-01,  1.08180e-01,
         1.66244e-01,  1.41359e-01,  1.43433e-01,  1.70392e-01,
         1.08180e-01,  1.18548e-01,  1.26843e-01, -8.67512e-02,
        -4.73502e-02,  2.52304e-02,  6.25576e-02, -5.87558e-03,
        -5.14977e-02, -8.05300e-02, -1.53111e-01, -1.11636e-01,
        -1.63479e-01, -2.52650e-01, -2.46429e-01, -3.21083e-01,
        -3.31452e-01, -3.85369e-01, -3.99885e-01, -1.24078e-01,
        -3.16935e-01, -2.94124e-01, -1.53111e-01)
       r2 <- c(4.23977e-01,  4.70760e-01,  3.53801e-01,  1.14035e-01,
        -1.72515e-01, -2.07602e-01, -4.38596e-02,  1.43275e-01,
         2.71930e-01,  3.47953e-01,  2.01754e-01,  8.77193e-03,
        -3.21637e-02,  5.55556e-02,  2.01754e-01,  1.60819e-01,
         7.89474e-02, -9.06433e-02,  1.25731e-01,  3.24561e-01,
        -2.19298e-01, -3.01170e-01, -3.30409e-01, -4.23977e-01,
        -5.64327e-01, -5.17544e-01, -3.24561e-01, -1.66667e-01,
        -7.30994e-02, -1.95906e-01, -3.42105e-01, -3.77193e-01,
        -4.64912e-01, -4.29825e-01, -1.95906e-01, -8.47953e-02,
        -2.36842e-01, -1.25731e-01, -2.92398e-03, -3.12865e-01,
        -1.25731e-01,  1.46199e-02,  2.04678e-02,  1.54971e-01,
        -1.60819e-01, -3.18713e-01, -2.19298e-01, -3.12865e-01,
        -4.59064e-01, -6.46199e-01, -6.05263e-01, -5.81871e-01,
        -4.29825e-01, -4.12281e-01, -4.82456e-01, -5.11696e-01,
        -4.06433e-01, -2.95322e-01, -1.72515e-01, -4.97076e-02,
        -4.97076e-02, -8.47953e-02, -1.54971e-01, -2.13450e-01,
        -2.66082e-01, -3.83041e-01, -4.82456e-01, -5.05848e-01,
        -5.11696e-01, -5.58480e-01, -6.57895e-01, -6.46199e-01,
        -5.52632e-01, -5.00000e-01, -4.23977e-01, -3.59649e-01,
        -3.71345e-01, -3.01170e-01, -2.07602e-01, -2.25146e-01,
        -2.71930e-01,  9.06433e-02,  7.30994e-02,  6.14035e-02,
         1.78363e-01,  1.95906e-01,  3.07018e-01,  2.95322e-01,
         2.66082e-01,  1.60819e-01,  7.89474e-02,  4.38596e-02,
         4.38596e-02,  1.02339e-01,  2.66082e-01,  3.01170e-01,
         3.47953e-01,  4.41520e-01,  4.41520e-01,  4.18129e-01,
         3.53801e-01,  2.19298e-01,  1.46199e-02, -1.25731e-01,
        -1.43275e-01, -1.37427e-01, -8.47953e-02, -7.89474e-02,
        -3.80117e-02, -8.77193e-03,  5.55556e-02,  1.37427e-01,
         2.77778e-01,  3.01170e-01,  1.95906e-01, -4.97076e-02,
         6.72515e-02, -2.30994e-01, -8.47953e-02,  6.72515e-02,
         1.19883e-01,  3.30409e-02,  1.31287e-01,  2.58187e-01,
         3.64620e-01, -6.92982e-02, -3.24561e-02, -9.79532e-02,
        -1.83918e-01, -2.90351e-01, -3.96784e-01,  4.38596e-03,
         8.62573e-02,  1.43567e-01,  2.29532e-01,  3.76901e-01,
         2.95029e-01,  2.17251e-01,  2.89474e-02,  2.13158e-01,
         2.58187e-01,  4.01462e-01,  4.21930e-01,  3.31871e-01,
         3.76901e-01, -4.37719e-01, -3.51754e-01, -2.08480e-01,
        -3.76316e-01, -5.03216e-01, -4.66374e-01, -2.57602e-01,
        -3.14912e-01, -3.84503e-01, -4.54094e-01, -5.19591e-01,
        -5.48246e-01, -5.35965e-01, -4.78655e-01, -5.07310e-01,
        -2.98538e-01, -1.75731e-01, -1.26608e-01,  2.58187e-01,
         3.56433e-01,  1.80409e-01,  7.80702e-02, -5.29240e-02,
        -1.63450e-01, -2.94444e-01, -4.66374e-01, -5.27778e-01,
        -3.96784e-01, -4.17251e-01, -1.67544e-01, -8.56725e-02,
        -1.60819e-02,  4.38596e-03,  2.04971e-01,  1.92690e-01,
         2.54094e-01,  1.92690e-01,  8.62573e-02, -6.92982e-02,
        -2.42690e-02,  6.57895e-02,  1.68129e-01,  1.92690e-01,
         2.99123e-01,  3.19591e-01,  3.93275e-01,  4.21930e-01,
         5.07895e-01,  5.20175e-01,  5.52924e-01,  4.42398e-01,
         5.73392e-01,  5.07895e-01,  5.52924e-01,  5.48830e-01,
         4.91520e-01, -1.88012e-01, -3.65497e-02, -4.33626e-01,
        -6.05556e-01, -5.15497e-01, -6.21930e-01, -1.26608e-01,
        -2.28947e-01, -1.34795e-01,  1.84503e-01)
       
train_X <- as.data.frame(r1, .rows = 1) %>% mutate(r2 = r2)
```

``` r
train_Y  <- c(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1,
        1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0)
```

``` r
r1 <- c(-0.35306235, -0.2271258 ,  0.09289767,  0.14824252, -0.00151249,
         0.04533501,  0.14434613, -0.13807064, -0.16385952, -0.19535742,
         0.1863405 , -0.49340321, -0.0918527 , -0.10428124, -0.20888058,
         0.24239325,  0.14986252, -0.49949386,  0.14708703, -0.20265487,
         0.09335189,  0.11339545,  0.25152129, -0.07166737,  0.18055064,
        -0.18110124,  0.27785389, -0.01327092, -0.01502107, -0.00308691,
         0.2141149 ,  0.04526842, -0.13417062, -0.05519283, -0.45642962,
        -0.44930938, -0.33460047,  0.14155288, -0.24788379, -0.04867248,
        -0.19644304, -0.49903366, -0.04108153, -0.28101764, -0.1756489 ,
        -0.46788726, -0.41606452,  0.29091938,  0.02219132, -0.42158567,
         0.1312701 , -0.41008228, -0.35553717, -0.35760857, -0.30571571,
        -0.02607286, -0.11757221, -0.24149786, -0.18033413,  0.26392833,
        -0.32814337, -0.29850456, -0.11643577, -0.46780346,  0.06911927,
        -0.04271503, -0.3385095 , -0.16095543, -0.07511389, -0.42933428,
        -0.16383887, -0.45116398,  0.0477326 , -0.20718875,  0.12999743,
        -0.17031431,  0.16669776,  0.10966868,  0.02952713, -0.38108519,
        -0.03730684,  0.23863706,  0.1794064 , -0.44541368, -0.13173944,
         0.25816653, -0.17144475,  0.0223521 ,  0.2175178 ,  0.16482247,
         0.10561683, -0.4963785 , -0.2907698 , -0.35518369, -0.11756326,
         0.2124443 , -0.33588692, -0.21834424,  0.2664948 ,  0.01073067,
         0.14320849, -0.49639587,  0.01985078, -0.49432988, -0.10664757,
        -0.00319048, -0.42560461,  0.01205282, -0.42333424, -0.18530875,
        -0.21421786, -0.31768577,  0.14291722, -0.32157713, -0.08101309,
        -0.25938858, -0.01728897, -0.00865689,  0.0123751 ,  0.2679647 ,
        -0.2220084 ,  0.17236739,  0.13126105,  0.24179202,  0.1222105 ,
         0.05346095, -0.2122828 , -0.40454458, -0.37627313, -0.40516269,
        -0.3576407 , -0.05349795, -0.29150432,  0.20878861, -0.02555617,
        -0.44039164,  0.19021425,  0.12623856, -0.33099112, -0.31851586,
        -0.16257604,  0.00302448, -0.09090669,  0.16334486,  0.11745137,
         0.05169295, -0.43946652,  0.20343101,  0.24735643, -0.28638208,
         0.03336897, -0.22278369, -0.27930754,  0.10390813, -0.22047998,
        -0.22122989,  0.10595997,  0.00536441, -0.18630331,  0.07871049,
        -0.03904464,  0.02897219, -0.24701344,  0.26144865, -0.1457531 ,
         0.0298899 , -0.20212221,  0.13433263, -0.40741669, -0.40943778,
         0.25701656,  0.11003269,  0.25896689,  0.15383051, -0.14735839,
         0.14736828, -0.03874009,  0.2255192 ,  0.26950824,  0.12482879,
         0.17162781,  0.005052  , -0.35458825, -0.47738659, -0.24116903,
         0.09375793, -0.29988892, -0.16247809,  0.19682557, -0.34801539,
         0.14035793, -0.04634861, -0.23929465, -0.45785597, -0.10819348,
         0.00520265,  0.17635156,  0.127651  ,  0.24868221, -0.31689909)
       r2 <- c(-0.67390181,  0.44731976, -0.75352419, -0.71847308,  0.16292786,
         0.20982573, -0.68720754,  0.55241732,  0.11134314,  0.4011457 ,
         0.52186234,  0.06007882, -0.12178156, -0.54557988,  0.15980217,
        -0.44476841, -0.10482911,  0.01644946,  0.17669485, -0.18450312,
        -0.08481748,  0.20626961, -0.24962124,  0.38027844, -0.49845242,
        -0.7543908 ,  0.24844372, -0.48747145,  0.36967342,  0.21570428,
         0.4517632 , -0.62350648, -0.75042241, -0.08569443,  0.30457343,
         0.2521395 ,  0.28108967, -0.68735638, -0.44652464, -0.79481614,
        -0.60949512, -0.48378051, -0.33717187, -0.02116391, -0.16079769,
        -0.03606064,  0.50923811,  0.00281817,  0.10162396,  0.39027243,
        -0.19991088, -0.67280407,  0.27731659,  0.52170388,  0.38303523,
         0.37125026, -0.02881462, -0.04582353,  0.00628885, -0.19920115,
        -0.62828486, -0.71993032, -0.41548515, -0.11866615, -0.51905734,
         0.33006558,  0.07544364,  0.15532539,  0.52723462, -0.32823455,
         0.59636143, -0.36885502,  0.08964546,  0.029608  , -0.76144357,
         0.26024428, -0.66180432, -0.02214623,  0.1205507 , -0.68066679,
        -0.16967073, -0.33979185,  0.36614806,  0.48773358, -0.37301885,
         0.3214765 , -0.05436293,  0.46044783, -0.64838015, -0.56009307,
        -0.47356917, -0.53039871,  0.39282998, -0.19033134,  0.45045063,
         0.24409336, -0.42208135,  0.42598976, -0.41708775,  0.1157563 ,
         0.57290223,  0.24764736, -0.3142498 ,  0.40264768, -0.00867353,
         0.50973211,  0.21494326,  0.43847113, -0.22920959, -0.21966031,
         0.57175744, -0.34148012,  0.08564325,  0.15996528,  0.01944172,
        -0.05106505, -0.57578078,  0.40091322, -0.27619353, -0.49294225,
        -0.28870081, -0.47234967, -0.23841632, -0.23915142, -0.18139411,
        -0.05415873, -0.49059782, -0.53062928, -0.36278139,  0.00920269,
        -0.18299568, -0.53677784, -0.03586948, -0.21603988,  0.26925529,
        -0.7207657 , -0.7314299 , -0.4287631 , -0.01338599,  0.06686597,
        -0.72654767, -0.71336776, -0.48357423,  0.29270667, -0.10436159,
        -0.49777696,  0.24581507,  0.00870498,  0.06879417,  0.12963342,
        -0.67034231, -0.14869216,  0.43238638,  0.10306382, -0.76702369,
         0.02037055, -0.12451613,  0.10208984,  0.08037445,  0.04847465,
         0.18299267, -0.02251376,  0.35259805,  0.38051718, -0.46868559,
         0.10126281, -0.3266953 , -0.63985405,  0.55315944,  0.46177788,
        -0.52299972, -0.52541811, -0.45826105,  0.10749309,  0.44665931,
        -0.41425849, -0.48927081,  0.27762013, -0.66565116,  0.50814405,
        -0.42429453, -0.04760931,  0.37126309, -0.68693223,  0.28161654,
        -0.77221995, -0.67487155,  0.17209416, -0.14566765, -0.2592839 ,
        -0.68428925, -0.14915378,  0.56220974,  0.25374027,  0.59753216,
        -0.54444942, -0.57245363, -0.34093757, -0.49750183, -0.42941273)
       
test_X <- as.data.frame(r1, .rows = 1) %>% mutate(r2 = r2)
```

``` r
test_Y <- c(0, 1, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0,
        0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 1, 1, 0, 0, 0, 0, 1, 0, 0,
        0, 1, 1, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 1,
        1, 1, 1, 0, 1, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 1,
        0, 0, 0, 1, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0,
        1, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 0,
        1, 0, 1, 1, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 1, 1, 0,
        0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 1, 1,
        0, 0, 0, 1, 0, 0, 1, 1, 1, 0, 0, 1, 0, 1, 0, 0, 1, 1, 1, 1, 0, 0,
        0, 0)
```

``` r
train_Xr <- train_X %>% 
  mutate(classifier = train_Y) %>%
  as.data.frame()

test_Xr <- test_X %>% 
  mutate(classifier = test_Y) %>%
  as.data.frame()

ggplot() +
  geom_point(aes(x = train_Xr[,1], y = train_Xr[,2], color = train_Xr[,3])) + 
  geom_point(aes(x = test_Xr[,1], y = test_Xr[,2], color = test_Xr[,3]))
```

![](Improving_Deep_Neural_Networks_files/figure-markdown_github/unnamed-chunk-16-1.png)

Each dot corresponds to a position on the football field where a
football player has hit the ball with his/her head after the French goal
keeper has shot the ball from the left side of the football field. - If
the dot is blue, it means the French player managed to hit the ball with
his/her head - If the dot is red, it means the other team’s player hit
the ball with their head

**Your goal**: Use a deep learning model to find the positions on the
field where the goalkeeper should kick the ball.

**Analysis of the dataset**: This dataset is a little noisy, but it
looks like a diagonal line separating the upper left half (blue) from
the lower right half (red) would work well.

You will first try a non-regularized model. Then you’ll learn how to
regularize it and decide which model you will choose to solve the French
Football Corporation’s problem.

## Non-Regularized Model

You will use the following neural network (already implemented for you
below). This model can be used: - in *regularization mode* – by setting
the `lambd` input to a non-zero value. We use “`lambd`” instead of
“`lambda`” because “`lambda`” is a reserved keyword in Python. - in
*dropout mode* – by setting the `keep_prob` to a value less than one

You will first try the model without any regularization. Then, you will
implement: - *L2 regularization* – functions:
“`compute_cost_with_regularization()`” and
“`backward_propagation_with_regularization()`” - *Dropout* – functions:
“`forward_propagation_with_dropout()`” and
“`backward_propagation_with_dropout()`”

In each part, you will run this model with the correct inputs so that it
calls the functions you’ve implemented. Take a look at the code below to
familiarize yourself with the model.

``` python
def model(X, Y, learning_rate = 0.3, num_iterations = 30000, print_cost = True, lambd = 0, keep_prob = 1):
    """
    Implements a three-layer neural network: LINEAR->RELU->LINEAR->RELU->LINEAR->SIGMOID.
    Arguments:
    X -- input data, of shape (input size, number of examples)
    Y -- true "label" vector (1 for blue dot / 0 for red dot), of shape (output size, number of examples)
    learning_rate -- learning rate of the optimization
    num_iterations -- number of iterations of the optimization loop
    print_cost -- If True, print the cost every 10000 iterations
    lambd -- regularization hyperparameter, scalar
    keep_prob - probability of keeping a neuron active during drop-out, scalar.
    Returns:
    parameters -- parameters learned by the model. They can then be used to predict.
    """
    grads = {}
    costs = []                            # to keep track of the cost
    m = X.shape[1]                        # number of examples
    layers_dims = [X.shape[0], 20, 3, 1]
    # Initialize parameters dictionary.
    parameters = initialize_parameters(layers_dims)
    # Loop (gradient descent)
    for i in range(0, num_iterations):
        # Forward propagation: LINEAR -> RELU -> LINEAR -> RELU -> LINEAR -> SIGMOID.
        if keep_prob == 1:
            a3, cache = forward_propagation(X, parameters)
        elif keep_prob < 1:
            a3, cache = forward_propagation_with_dropout(X, parameters, keep_prob)
        # Cost function
        if lambd == 0:
            cost = compute_cost(a3, Y)
        else:
            cost = compute_cost_with_regularization(a3, Y, parameters, lambd)
        # Backward propagation.
        assert (lambd == 0 or keep_prob == 1)   # it is possible to use both L2 regularization and dropout, 
                                                # but this assignment will only explore one at a time
        if lambd == 0 and keep_prob == 1:
            grads = backward_propagation(X, Y, cache)
        elif lambd != 0:
            grads = backward_propagation_with_regularization(X, Y, cache, lambd)
        elif keep_prob < 1:
            grads = backward_propagation_with_dropout(X, Y, cache, keep_prob)
        # Update parameters.
        parameters = update_parameters(parameters, grads, learning_rate)
        # Print the loss every 10000 iterations
        if print_cost and i % 10000 == 0:
            print("Cost after iteration {}: {}".format(i, cost))
        if print_cost and i % 1000 == 0:
            costs.append(cost)
    # plot the cost
    plt.plot(costs)
    plt.ylabel('cost')
    plt.xlabel('iterations (x1,000)')
    plt.title("Learning rate =" + str(learning_rate))
    plt.show()
    return parameters
```

![](/Users/jolante/Bryan/ML_work/deep_learning.ai_coursera/Month2:Tunning%20Hyperparameters/a2_loss_noreg.png)

![](/Users/jolante/Bryan/ML_work/deep_learning.ai_coursera/Month2:Tunning%20Hyperparameters/a2_noreg_bound.png)

The non-regularized model is obviously overfitting the training set. It
is fitting the noisy points! Lets now look at two techniques to reduce
overfitting.

## L2 Regularization

The standard way to avoid overfitting is called **L2 regularization**.
It consists of appropriately modifying your cost function, from:
$$J = -\\frac{1}{m} \\sum\\limits\_{i = 1}^{m} \\large{(}\\small  y^{(i)}\\log\\left(a^{\[L\](i)}\\right) + (1-y^{(i)})\\log\\left(1- a^{\[L\](i)}\\right) \\large{)} \\tag{1}$$
To:
$$J\_{regularized} = \\small \\underbrace{-\\frac{1}{m} \\sum\\limits\_{i = 1}^{m} \\large{(}\\small y^{(i)}\\log\\left(a^{\[L\](i)}\\right) + (1-y^{(i)})\\log\\left(1- a^{\[L\](i)}\\right) \\large{)} }\_\\text{cross-entropy cost} + \\underbrace{\\frac{1}{m} \\frac{\\lambda}{2} \\sum\\limits_l\\sum\\limits_k\\sum\\limits_j W\_{k,j}^{\[l\]2} }\_\\text{L2 regularization cost} \\tag{2}$$

Let’s modify your cost and observe the consequences.

### Exercise 1 - compute_cost_with_regularization

Implement `compute_cost_with_regularization()` which computes the cost
given by formula (2). To calculate
$\\sum\\limits_k\\sum\\limits_j W\_{k,j}^{\[l\]2}$ , use :

np.sum(np.square(Wl))

Note that you have to do this for *W*<sup>\[1\]</sup>,
*W*<sup>\[2\]</sup> and *W*<sup>\[3\]</sup>, then sum the three terms
and multiply by $ $.

``` python
# GRADED FUNCTION: compute_cost_with_regularization
def compute_cost_with_regularization(A3, Y, parameters, lambd):
    """
    Implement the cost function with L2 regularization. See formula (2) above.
    Arguments:
    A3 -- post-activation, output of forward propagation, of shape (output size, number of examples)
    Y -- "true" labels vector, of shape (output size, number of examples)
    parameters -- python dictionary containing parameters of the model
    Returns:
    cost - value of the regularized loss function (formula (2))
    """
    m = Y.shape[1]
    W1 = parameters["W1"]
    W2 = parameters["W2"]
    W3 = parameters["W3"]
    cross_entropy_cost = compute_cost(A3, Y) # This gives you the cross-entropy part of the cost
    #(≈ 1 lines of code)
    # L2_regularization_cost = 
    # YOUR CODE STARTS HERE
    L2_regularization_cost = lambd * (np.sum(np.square(W1)) + np.sum(np.square(W2)) + np.sum(np.square(W3))) / (m * 2)
    
    # YOUR CODE ENDS HERE
    cost = cross_entropy_cost + L2_regularization_cost
    return cost
```

Of course, because you changed the cost, you have to change backward
propagation as well! All the gradients have to be computed with respect
to this new cost.

### Exercise 2 - backward_propagation_with_regularization

Implement the changes needed in backward propagation to take into
account regularization. The changes only concern dW1, dW2 and dW3. For
each, you have to add the regularization term’s gradient
($\\frac{d}{dW} ( \\frac{1}{2}\\frac{\\lambda}{m} W^2) = \\frac{\\lambda}{m} W$).

``` python
# GRADED FUNCTION: backward_propagation_with_regularization
def backward_propagation_with_regularization(X, Y, cache, lambd):
    """
    Implements the backward propagation of our baseline model to which we added an L2 regularization
    Arguments:
    X -- input dataset, of shape (input size, number of examples)
    Y -- "true" labels vector, of shape (output size, number of examples)
    cache -- cache output from forward_propagation()
    lambd -- regularization hyperparameter, scalar
    Returns:
    gradients -- A dictionary with the gradients with respect to each parameter, activation and pre-activation variables
    """
    m = X.shape[1]
    (Z1, A1, W1, b1, Z2, A2, W2, b2, Z3, A3, W3, b3) = cache
    dZ3 = A3 - Y
    #(≈ 1 lines of code)
    # dW3 = 1./m * np.dot(dZ3, A2.T) + None
    # YOUR CODE STARTS HERE
    dW3 = 1./m * np.dot(dZ3, A2.T) + (lambd * W3) / m
    # YOUR CODE ENDS HERE
    db3 = 1. / m * np.sum(dZ3, axis=1, keepdims=True)
    dA2 = np.dot(W3.T, dZ3)
    dZ2 = np.multiply(dA2, np.int64(A2 > 0))
    #(≈ 1 lines of code)
    # dW2 = 1./m * np.dot(dZ2, A1.T) + None
    # YOUR CODE STARTS HERE
    dW2 = 1./m * np.dot(dZ2, A1.T) + (lambd * W2) / m
    # YOUR CODE ENDS HERE
    db2 = 1. / m * np.sum(dZ2, axis=1, keepdims=True)
    dA1 = np.dot(W2.T, dZ2)
    dZ1 = np.multiply(dA1, np.int64(A1 > 0))
    #(≈ 1 lines of code)
    # dW1 = 1./m * np.dot(dZ1, X.T) + None
    # YOUR CODE STARTS HERE
    dW1 = 1./m * np.dot(dZ1, X.T) + (lambd * W1) / m
    # YOUR CODE ENDS HERE
    db1 = 1. / m * np.sum(dZ1, axis=1, keepdims=True)
    gradients = {"dZ3": dZ3, "dW3": dW3, "db3": db3,"dA2": dA2,
                 "dZ2": dZ2, "dW2": dW2, "db2": db2, "dA1": dA1, 
                 "dZ1": dZ1, "dW1": dW1, "db1": db1}
    return gradients
```

![](/Users/jolante/Bryan/ML_work/deep_learning.ai_coursera/Month2:Tunning%20Hyperparameters/a2_reg_curv.png)

![](/Users/jolante/Bryan/ML_work/deep_learning.ai_coursera/Month2:Tunning%20Hyperparameters/a2_reg_bound.png)

## Dropout

Finally, **dropout** is a widely used regularization technique that is
specific to deep learning. **It randomly shuts down some neurons in each
iteration.**

When you shut some neurons down, you actually modify your model. The
idea behind drop-out is that at each iteration, you train a different
model that uses only a subset of your neurons. With dropout, your
neurons thus become less sensitive to the activation of one other
specific neuron, because that other neuron might be shut down at any
time.

### Exercise 3 - forward_propagation_with_dropout

Implement the forward propagation with dropout. You are using a 3 layer
neural network, and will add dropout to the first and second hidden
layers. We will not apply dropout to the input layer or output layer.

**Instructions**: You would like to shut down some neurons in the first
and second layers. To do that, you are going to carry out 4 Steps: 1. In
lecture, we dicussed creating a variable *d*<sup>\[1\]</sup> with the
same shape as *a*<sup>\[1\]</sup> using `np.random.rand()` to randomly
get numbers between 0 and 1. Here, you will use a vectorized
implementation, so create a random matrix $D^{\[1\]} = \[d^{[1](1)}
d^{[1](2)} … d^{[1](m)}\] $ of the same dimension as
*A*<sup>\[1\]</sup>. 2. Set each entry of *D*<sup>\[1\]</sup> to be 1
with probability (`keep_prob`), and 0 otherwise.

**Hint:** Let’s say that keep_prob = 0.8, which means that we want to
keep about 80% of the neurons and drop out about 20% of them. We want to
generate a vector that has 1’s and 0’s, where about 80% of them are 1
and about 20% are 0. This python statement:  
`X = (X < keep_prob).astype(int)`

is conceptually the same as this if-else statement (for the simple case
of a one-dimensional array) :

for i,v in enumerate(x): if v \< keep_prob: x\[i\] = 1 else: \# v \>=
keep_prob x\[i\] = 0

Note that the `X = (X < keep_prob).astype(int)` works with
multi-dimensional arrays, and the resulting output preserves the
dimensions of the input array.

Also note that without using `.astype(int)`, the result is an array of
booleans `True` and `False`, which Python automatically converts to 1
and 0 if we multiply it with numbers. (However, it’s better practice to
convert data into the data type that we intend, so try using
`.astype(int)`.)

1.  Set *A*<sup>\[1\]</sup> to
    *A*<sup>\[1\]</sup> \* *D*<sup>\[1\]</sup>. (You are shutting down
    some neurons). You can think of *D*<sup>\[1\]</sup> as a mask, so
    that when it is multiplied with another matrix, it shuts down some
    of the values.
2.  Divide *A*<sup>\[1\]</sup> by `keep_prob`. By doing this you are
    assuring that the result of the cost will still have the same
    expected value as without drop-out. (This technique is also called
    inverted dropout.)

``` python
# GRADED FUNCTION: forward_propagation_with_dropout
def forward_propagation_with_dropout(X, parameters, keep_prob = 0.5):
    """
    Implements the forward propagation: LINEAR -> RELU + DROPOUT -> LINEAR -> RELU + DROPOUT -> LINEAR -> SIGMOID.
    Arguments:
    X -- input dataset, of shape (2, number of examples)
    parameters -- python dictionary containing your parameters "W1", "b1", "W2", "b2", "W3", "b3":
                    W1 -- weight matrix of shape (20, 2)
                    b1 -- bias vector of shape (20, 1)
                    W2 -- weight matrix of shape (3, 20)
                    b2 -- bias vector of shape (3, 1)
                    W3 -- weight matrix of shape (1, 3)
                    b3 -- bias vector of shape (1, 1)
    keep_prob - probability of keeping a neuron active during drop-out, scalar
    Returns:
    A3 -- last activation value, output of the forward propagation, of shape (1,1)
    cache -- tuple, information stored for computing the backward propagation
    """
    np.random.seed(1)
    # retrieve parameters
    W1 = parameters["W1"]
    b1 = parameters["b1"]
    W2 = parameters["W2"]
    b2 = parameters["b2"]
    W3 = parameters["W3"]
    b3 = parameters["b3"]
    # LINEAR -> RELU -> LINEAR -> RELU -> LINEAR -> SIGMOID
    Z1 = np.dot(W1, X) + b1
    A1 = relu(Z1)
    #(≈ 4 lines of code)         # Steps 1-4 below correspond to the Steps 1-4 described above. 
    # D1 =                                           # Step 1: initialize matrix D1 = np.random.rand(..., ...)
    # D1 =                                           # Step 2: convert entries of D1 to 0 or 1 (using keep_prob as the threshold)
    # A1 =                                           # Step 3: shut down some neurons of A1
    # A1 =                                           # Step 4: scale the value of neurons that haven't been shut down
    # YOUR CODE STARTS HERE
    D1 = np.random.rand(A1.shape[0], A1.shape[1])
    D1 = (D1 < keep_prob).astype(int)
    A1 = A1 * D1
    A1 = A1 / keep_prob
    # YOUR CODE ENDS HERE
    Z2 = np.dot(W2, A1) + b2
    A2 = relu(Z2)
    #(≈ 4 lines of code)
    # D2 =                                           # Step 1: initialize matrix D2 = np.random.rand(..., ...)
    # D2 =                                           # Step 2: convert entries of D2 to 0 or 1 (using keep_prob as the threshold)
    # A2 =                                           # Step 3: shut down some neurons of A2
    # A2 =                                           # Step 4: scale the value of neurons that haven't been shut down
    # YOUR CODE STARTS HERE
    D2 = np.random.rand(A2.shape[0], A1.shape[1])
    D2 = (D2 < keep_prob).astype(int)
    A2 = A2 * D2
    A2 = A2 / keep_prob
    # YOUR CODE ENDS HERE
    Z3 = np.dot(W3, A2) + b3
    A3 = sigmoid(Z3)
    cache = (Z1, D1, A1, W1, b1, Z2, D2, A2, W2, b2, Z3, A3, W3, b3)
    return A3, cache
```

### Exercise 4 - backward_propagation_with_dropout

Implement the backward propagation with dropout. As before, you are
training a 3 layer network. Add dropout to the first and second hidden
layers, using the masks *D*<sup>\[1\]</sup> and *D*<sup>\[2\]</sup>
stored in the cache.

**Instruction**: Backpropagation with dropout is actually quite easy.
You will have to carry out 2 Steps: 1. You had previously shut down some
neurons during forward propagation, by applying a mask
*D*<sup>\[1\]</sup> to `A1`. In backpropagation, you will have to shut
down the same neurons, by reapplying the same mask *D*<sup>\[1\]</sup>
to `dA1`. 2. During forward propagation, you had divided `A1` by
`keep_prob`. In backpropagation, you’ll therefore have to divide `dA1`
by `keep_prob` again (the calculus interpretation is that if
*A*<sup>\[1\]</sup> is scaled by `keep_prob`, then its derivative
*d**A*<sup>\[1\]</sup> is also scaled by the same `keep_prob`).

``` python
# GRADED FUNCTION: backward_propagation_with_dropout
def backward_propagation_with_dropout(X, Y, cache, keep_prob):
    """
    Implements the backward propagation of our baseline model to which we added dropout.
    Arguments:
    X -- input dataset, of shape (2, number of examples)
    Y -- "true" labels vector, of shape (output size, number of examples)
    cache -- cache output from forward_propagation_with_dropout()
    keep_prob - probability of keeping a neuron active during drop-out, scalar
    Returns:
    gradients -- A dictionary with the gradients with respect to each parameter, activation and pre-activation variables
    """
    m = X.shape[1]
    (Z1, D1, A1, W1, b1, Z2, D2, A2, W2, b2, Z3, A3, W3, b3) = cache
    dZ3 = A3 - Y
    dW3 = 1./m * np.dot(dZ3, A2.T)
    db3 = 1./m * np.sum(dZ3, axis=1, keepdims=True)
    dA2 = np.dot(W3.T, dZ3)
    #(≈ 2 lines of code)
    # dA2 =                # Step 1: Apply mask D2 to shut down the same neurons as during the forward propagation
    # dA2 =                # Step 2: Scale the value of neurons that haven't been shut down
    # YOUR CODE STARTS HERE
    dA2 = dA2 * D2
    dA2 = dA2 / keep_prob
    # YOUR CODE ENDS HERE
    dZ2 = np.multiply(dA2, np.int64(A2 > 0))
    dW2 = 1./m * np.dot(dZ2, A1.T)
    db2 = 1./m * np.sum(dZ2, axis=1, keepdims=True)
    dA1 = np.dot(W2.T, dZ2)
    #(≈ 2 lines of code)
    # dA1 =                # Step 1: Apply mask D1 to shut down the same neurons as during the forward propagation
    # dA1 =                # Step 2: Scale the value of neurons that haven't been shut down
    # YOUR CODE STARTS HERE
    dA1 = dA1 * D1
    dA1 = dA1 / keep_prob
    # YOUR CODE ENDS HERE
    dZ1 = np.multiply(dA1, np.int64(A1 > 0))
    dW1 = 1./m * np.dot(dZ1, X.T)
    db1 = 1./m * np.sum(dZ1, axis=1, keepdims=True)
    gradients = {"dZ3": dZ3, "dW3": dW3, "db3": db3,"dA2": dA2,
                 "dZ2": dZ2, "dW2": dW2, "db2": db2, "dA1": dA1, 
                 "dZ1": dZ1, "dW1": dW1, "db1": db1}
    return gradients
```

# Assigment 3: Gradient Checking

Welcome to the final assignment for this week! In this assignment you’ll
be implementing gradient checking.

By the end of this notebook, you’ll be able to:

Implement gradient checking to verify the accuracy of your backprop
implementation

## Python Libraries

``` python
# import numpy as np
# from testCases import *
# from public_tests import *
# from gc_utils import sigmoid, relu, dictionary_to_vector, vector_to_dictionary, gradients_to_vector
# 
# %load_ext autoreload
# %autoreload 2
```

## Problem Statement

You are part of a team working to make mobile payments available
globally, and are asked to build a deep learning model to detect
fraud–whenever someone makes a payment, you want to see if the payment
might be fraudulent, such as if the user’s account has been taken over
by a hacker.

You already know that backpropagation is quite challenging to implement,
and sometimes has bugs. Because this is a mission-critical application,
your company’s CEO wants to be really certain that your implementation
of backpropagation is correct. Your CEO says, “Give me proof that your
backpropagation is actually working!” To give this reassurance, you are
going to use “gradient checking.”

Let’s do it!

## How does Gradient Checking work?

Backpropagation computes the gradients
$\\frac{\\partial J}{\\partial \\theta}$, where *θ* denotes the
parameters of the model. *J* is computed using forward propagation and
your loss function.

Because forward propagation is relatively easy to implement, you’re
confident you got that right, and so you’re almost 100% sure that you’re
computing the cost *J* correctly. Thus, you can use your code for
computing *J* to verify the code for computing
$\\frac{\\partial J}{\\partial \\theta}$.

Let’s look back at the definition of a derivative (or gradient):
$$ \\frac{\\partial J}{\\partial \\theta} = \\lim\_{\\varepsilon \\to 0} \\frac{J(\\theta + \\varepsilon) - J(\\theta - \\varepsilon)}{2 \\varepsilon} $$

If you’re not familiar with the “lim<sub>*ε* → 0</sub>” notation, it’s
just a way of saying “when *ε* is really, really small.”

You know the following:

$\\frac{\\partial J}{\\partial \\theta}$ is what you want to make sure
you’re computing correctly. You can compute *J*(*θ* + *ε*) and
*J*(*θ* − *ε*) (in the case that *θ* is a real number), since you’re
confident your implementation for *J* is correct. Let’s use equation (1)
and a small value for *ε* to convince your CEO that your code for
computing $\\frac{\\partial J}{\\partial \\theta}$ is correct!

### Exercise 1 - forward_propagation

Implement `forward propagation`. For this simple function compute *J*(.)

``` python
# GRADED FUNCTION: forward_propagation
def forward_propagation(x, theta):
    """
    Implement the linear forward propagation (compute J) presented in Figure 1 (J(theta) = theta * x)
    Arguments:
    x -- a real-valued input
    theta -- our parameter, a real number as well
    Returns:
    J -- the value of function J, computed using the formula J(theta) = theta * x
    """
    # (approx. 1 line)
    # J = 
    # YOUR CODE STARTS HERE
    J = x * theta
    # YOUR CODE ENDS HERE
    return J
```

### Exercise 2 - backward_propagation

Now, implement the `backward propagation` step (derivative computation)
of Figure 1. That is, compute the derivative of *J*(*θ*) = *θ**x* with
respect to *θ*. To save you from doing the calculus, you should get
$dtheta = \\frac { \\partial J }{ \\partial \\theta} = x$.

``` ptyhon
# GRADED FUNCTION: backward_propagation
def backward_propagation(x, theta):
    """
    Computes the derivative of J with respect to theta (see Figure 1).
    Arguments:
    x -- a real-valued input
    theta -- our parameter, a real number as well
    Returns:
    dtheta -- the gradient of the cost with respect to theta
    """
    # (approx. 1 line)
    # dtheta = 
    # YOUR CODE STARTS HERE
    dtheta = x
    # YOUR CODE ENDS HERE
    return dtheta
```

### Exercise 3 - gradient_check

​ To show that the `backward_propagation()` function is correctly
computing the gradient $\\frac{\\partial J}{\\partial \\theta}$, let’s
implement gradient checking. ​ **Instructions**: - First compute
“gradapprox” using the formula above (1) and a small value of *ε*. Here
are the Steps to follow: 1. *θ*<sup>+</sup> = *θ* + *ε* 2.
*θ*<sup>−</sup> = *θ* − *ε* 3. *J*<sup>+</sup> = *J*(*θ*<sup>+</sup>) 4.
*J*<sup>−</sup> = *J*(*θ*<sup>−</sup>) 5.
$gradapprox = \\frac{J^{+} - J^{-}}{2 \\varepsilon}$ - Then compute the
gradient using backward propagation, and store the result in a variable
“grad” - Finally, compute the relative difference between “gradapprox”
and the “grad” using the following formula:
$$ difference = \\frac {\\mid\\mid grad - gradapprox \\mid\\mid_2}{\\mid\\mid grad \\mid\\mid_2 + \\mid\\mid gradapprox \\mid\\mid_2} \\tag{2}$$
You will need 3 Steps to compute this formula: - 1’. compute the
numerator using np.linalg.norm(…) - 2’. compute the denominator. You
will need to call np.linalg.norm(…) twice. - 3’. divide them. - If this
difference is small (say less than 10<sup> − 7</sup>), you can be quite
confident that you have computed your gradient correctly. Otherwise,
there may be a mistake in the gradient computation. ​

``` python
# GRADED FUNCTION: gradient_check
def gradient_check(x, theta, epsilon=1e-7, print_msg=False):
    """
    Implement the backward propagation presented in Figure 1.
    Arguments:
    x -- a float input
    theta -- our parameter, a float as well
    epsilon -- tiny shift to the input to compute approximated gradient with formula(1)
    Returns:
    difference -- difference (2) between the approximated gradient and the backward propagation gradient. Float output
    """
    # Compute gradapprox using left side of formula (1). epsilon is small enough, you don't need to worry about the limit.
    # (approx. 5 lines)
    # theta_plus =                                 # Step 1
    # theta_minus =                                # Step 2
    # J_plus =                                    # Step 3
    # J_minus =                                   # Step 4
    # gradapprox =                                # Step 5
    # YOUR CODE STARTS HERE
    theta_plus = theta + epsilon
    theta_minus = theta - epsilon
    J_plus = forward_propagation(x, theta_plus)
    J_minus = forward_propagation(x, theta_minus)
    gradapprox = (J_plus - J_minus) / (2 * epsilon)
    # YOUR CODE ENDS HERE
    # Check if gradapprox is close enough to the output of backward_propagation()
    #(approx. 1 line)
    # grad =
    # YOUR CODE STARTS HERE
    grad = backward_propagation(x, theta)
    # YOUR CODE ENDS HERE
    #(approx. 1 line)
    # numerator =                                 # Step 1'
    # denominator =                               # Step 2'
    # difference =                                # Step 3'
    # YOUR CODE STARTS HERE
    numerator = np.linalg.norm((grad - gradapprox))
    denominator = np.linalg.norm(grad) + np.linalg.norm(gradapprox)
    difference = numerator / denominator
    # YOUR CODE ENDS HERE
    if print_msg:
        if difference > 2e-7:
            print ("\033[93m" + "There is a mistake in the backward propagation! difference = " + str(difference) + "\033[0m")
        else:
            print ("\033[92m" + "Your backward propagation works perfectly fine! difference = " + str(difference) + "\033[0m")
    return difference
```

Congrats, the difference is smaller than the 10<sup> − 7</sup>
threshold. So you can have high confidence that you’ve correctly
computed the gradient in `backward_propagation()`.

Now, in the more general case, your cost function *J* has more than a
single 1D input. When you are training a neural network, *θ* actually
consists of multiple matrices *W*<sup>\[*l*\]</sup> and biases
*b*<sup>\[*l*\]</sup>! It is important to know how to do a gradient
check with higher-dimensional inputs. Let’s do it!

## N-Dimensional Gradient Checking

The following figure describes the forward and backward propagation of
your fraud detection model.

![](/Users/jolante/Bryan/ML_work/deep_learning.ai_coursera/Month2:Tunning%20Hyperparameters/multi_dim_backprop.png)

Let’s look at your implementations for forward propagation and backward
propagation.

``` python
def forward_propagation_n(X, Y, parameters):
    """
    Implements the forward propagation (and computes the cost) presented in Figure 3.
    Arguments:
    X -- training set for m examples
    Y -- labels for m examples 
    parameters -- python dictionary containing your parameters "W1", "b1", "W2", "b2", "W3", "b3":
                    W1 -- weight matrix of shape (5, 4)
                    b1 -- bias vector of shape (5, 1)
                    W2 -- weight matrix of shape (3, 5)
                    b2 -- bias vector of shape (3, 1)
                    W3 -- weight matrix of shape (1, 3)
                    b3 -- bias vector of shape (1, 1)
    Returns:
    cost -- the cost function (logistic cost for one example)
    cache -- a tuple with the intermediate values (Z1, A1, W1, b1, Z2, A2, W2, b2, Z3, A3, W3, b3)

    """
    # retrieve parameters
    m = X.shape[1]
    W1 = parameters["W1"]
    b1 = parameters["b1"]
    W2 = parameters["W2"]
    b2 = parameters["b2"]
    W3 = parameters["W3"]
    b3 = parameters["b3"]
    # LINEAR -> RELU -> LINEAR -> RELU -> LINEAR -> SIGMOID
    Z1 = np.dot(W1, X) + b1
    A1 = relu(Z1)
    Z2 = np.dot(W2, A1) + b2
    A2 = relu(Z2)
    Z3 = np.dot(W3, A2) + b3
    A3 = sigmoid(Z3)
    # Cost
    log_probs = np.multiply(-np.log(A3),Y) + np.multiply(-np.log(1 - A3), 1 - Y)
    cost = 1. / m * np.sum(log_probs)
    cache = (Z1, A1, W1, b1, Z2, A2, W2, b2, Z3, A3, W3, b3)
    return cost, cache
```

``` python
def backward_propagation_n(X, Y, cache):
    """
    Implement the backward propagation presented in figure 2.
    Arguments:
    X -- input datapoint, of shape (input size, 1)
    Y -- true "label"
    cache -- cache output from forward_propagation_n()
    Returns:
    gradients -- A dictionary with the gradients of the cost with respect to each parameter, activation and pre-activation variables.
    """
    m = X.shape[1]
    (Z1, A1, W1, b1, Z2, A2, W2, b2, Z3, A3, W3, b3) = cache
    dZ3 = A3 - Y
    dW3 = 1. / m * np.dot(dZ3, A2.T)
    db3 = 1. / m * np.sum(dZ3, axis=1, keepdims=True)
    dA2 = np.dot(W3.T, dZ3)
    dZ2 = np.multiply(dA2, np.int64(A2 > 0))
    dW2 = 1. / m * np.dot(dZ2, A1.T) * 2
    db2 = 1. / m * np.sum(dZ2, axis=1, keepdims=True)
    dA1 = np.dot(W2.T, dZ2)
    dZ1 = np.multiply(dA1, np.int64(A1 > 0))
    dW1 = 1. / m * np.dot(dZ1, X.T)
    db1 = 4. / m * np.sum(dZ1, axis=1, keepdims=True)
    gradients = {"dZ3": dZ3, "dW3": dW3, "db3": db3,
                 "dA2": dA2, "dZ2": dZ2, "dW2": dW2, "db2": db2,
                 "dA1": dA1, "dZ1": dZ1, "dW1": dW1, "db1": db1}
    return gradients
```

**How does gradient checking work?**.

As in Section 3 and 4, you want to compare “gradapprox” to the gradient
computed by backpropagation. The formula is still:

$$ \\frac{\\partial J}{\\partial \\theta} = \\lim\_{\\varepsilon \\to 0} \\frac{J(\\theta + \\varepsilon) - J(\\theta - \\varepsilon)}{2 \\varepsilon} \\tag{1}$$

However, *θ* is not a scalar anymore. It is a dictionary called
“parameters”. The function “`dictionary_to_vector()`” has been
implemented for you. It converts the “parameters” dictionary into a
vector called “values”, obtained by reshaping all parameters (W1, b1,
W2, b2, W3, b3) into vectors and concatenating them.

The inverse function is “`vector_to_dictionary`” which outputs back the
“parameters” dictionary.

![](/Users/jolante/Bryan/ML_work/deep_learning.ai_coursera/Month2:Tunning%20Hyperparameters/vis_vec%3C%3Edict.png)

The “gradients” dictionary has also been converted into a vector “grad”
using gradients_to_vector(), so you don’t need to worry about that.

Now, for every single parameter in your vector, you will apply the same
procedure as for the gradient_check exercise. You will store each
gradient approximation in a vector `gradapprox`. If the check goes as
expected, each value in this approximation must match the real gradient
values stored in the `grad` vector.

Note that `grad` is calculated using the function `gradients_to_vector`,
which uses the gradients outputs of the `backward_propagation_n`
function.

### Exercise 4 - gradient_check_n

Implement the function below.

**Instructions**: Here is pseudo-code that will help you implement the
gradient check.

For each i in num_parameters: - To compute `J_plus[i]`: 1. Set
*θ*<sup>+</sup> to `np.copy(parameters_values)` 2. Set
*θ*<sub>*i*</sub><sup>+</sup> to *θ*<sub>*i*</sub><sup>+</sup> + *ε* 3.
Calculate *J*<sub>*i*</sub><sup>+</sup> using to
`forward_propagation_n(x, y, vector_to_dictionary(`*θ*<sup>+</sup>
`))`.  
- To compute `J_minus[i]`: do the same thing with *θ*<sup>−</sup> -
Compute $gradapprox\[i\] = \\frac{J^{+}\_i - J^{-}\_i}{2 \\varepsilon}$

Thus, you get a vector gradapprox, where gradapprox\[i\] is an
approximation of the gradient with respect to `parameter_values[i]`. You
can now compare this gradapprox vector to the gradients vector from
backpropagation. Just like for the 1D case (Steps 1’, 2’, 3’), compute:
$$ difference = \\frac {\\\| grad - gradapprox \\\|\_2}{\\\| grad \\\|\_2 + \\\| gradapprox \\\|\_2 } \\tag{3}$$

**Note**: Use `np.linalg.norm` to get the norms

``` python
# GRADED FUNCTION: gradient_check_n
def gradient_check_n(parameters, gradients, X, Y, epsilon=1e-7, print_msg=False):
    """
    Checks if backward_propagation_n computes correctly the gradient of the cost output by forward_propagation_n
    Arguments:
    parameters -- python dictionary containing your parameters "W1", "b1", "W2", "b2", "W3", "b3":
    grad -- output of backward_propagation_n, contains gradients of the cost with respect to the parameters. 
    x -- input datapoint, of shape (input size, 1)
    y -- true "label"
    epsilon -- tiny shift to the input to compute approximated gradient with formula(1)
    Returns:
    difference -- difference (2) between the approximated gradient and the backward propagation gradient
    """
    # Set-up variables
    parameters_values, _ = dictionary_to_vector(parameters)
    grad = gradients_to_vector(gradients)
    num_parameters = parameters_values.shape[0]
    J_plus = np.zeros((num_parameters, 1))
    J_minus = np.zeros((num_parameters, 1))
    gradapprox = np.zeros((num_parameters, 1))
    # Compute gradapprox
    for i in range(num_parameters):
        # Compute J_plus[i]. Inputs: "parameters_values, epsilon". Output = "J_plus[i]".
        # "_" is used because the function you have to outputs two parameters but we only care about the first one
        #(approx. 3 lines)
        # theta_plus =                                        # Step 1
        # theta_plus[i] =                                     # Step 2
        # J_plus[i], _ =                                     # Step 3
        # YOUR CODE STARTS HERE
        theta_plus = np.copy(parameters_values)
        theta_plus[i] = theta_plus[i] + epsilon
        J_plus[i], _ = forward_propagation_n(X, Y, vector_to_dictionary(theta_plus))
        # YOUR CODE ENDS HERE
        # Compute J_minus[i]. Inputs: "parameters_values, epsilon". Output = "J_minus[i]".
        #(approx. 3 lines)
        # theta_minus =                                    # Step 1
        # theta_minus[i] =                                 # Step 2        
        # J_minus[i], _ =                                 # Step 3
        # YOUR CODE STARTS HERE
        theta_minus = np.copy(parameters_values)
        theta_minus[i] = theta_minus[i] - epsilon
        J_minus[i], _ = forward_propagation_n(X, Y, vector_to_dictionary(theta_minus))
        # YOUR CODE ENDS HERE
        # Compute gradapprox[i]
        # (approx. 1 line)
        # gradapprox[i] = 
        # YOUR CODE STARTS HERE
        gradapprox[i] = (J_plus[i] - J_minus[i]) / (2 * epsilon)
        # YOUR CODE ENDS HERE
    # Compare gradapprox to backward propagation gradients by computing difference.
    # (approx. 1 line)
    # numerator =                                             # Step 1'
    # denominator =                                           # Step 2'
    # difference =                                            # Step 3'
    # YOUR CODE STARTS HERE
    numerator = np.linalg.norm(grad - gradapprox)
    denominator = np.linalg.norm(grad) + np.linalg.norm(gradapprox)
    difference = numerator / denominator
    # YOUR CODE ENDS HERE
    if print_msg:
        if difference > 2e-7:
            print ("\033[93m" + "There is a mistake in the backward propagation! difference = " + str(difference) + "\033[0m")
        else:
            print ("\033[92m" + "Your backward propagation works perfectly fine! difference = " + str(difference) + "\033[0m")
    return difference
```

# Week 2

## Mini-batch Gradient Descent

Batching splits the training set into small partitions to allow gradient
descent to be calculated in chunks rather than once for each iteration
over the entire training set. Batch conventionally refers to the entire
training set while mini-batch is the partitioned training set.

### How Mini-Batch Differs from Batch

We perform the forward steps of vectorized calculations
*Z* = *W**X* + *b*, *A* = *g*(*Z*),
$L=\\frac{1}{batch size}\\sum L(\\hat{y},y) + \\frac{\\lambda}{2\*batch size}\*\\sum \|\|w\|\|\_{F}^2$
just like we would for the entire training set but using the mini-batch
size. We fallow a similar approach for the backwards propagation. One
iteration through every mini-batch is called an **Epoch**. This can also
make a huge difference in managing computer memory. This will need to be
looked into further when considering parallelization.

#### Q:

Is batching and non-batched gradient descent calculations linear? Does
this only effect speed of calculation? Taking smaller steps may allow
for better convergence than larger batch steps.

#### Note:

This is likely what the batch is referring to in EqT

Plotting cost versus epoch while using the mini-batch methods will yield
a jittered curve rather than a smooth curve. This is because for each
iteration you are parsing through a list of which batch of training data
to calculate the cost for.

### Gradient descent with batch size between 1 & m

If the training set is very large, *m* may take much to long to
converge. Larger training sets will smoothly head towards the global
minimum. If the batch size is particularly small, for example near 1,
stochastic gradient descent kicks in and the gradient descent will not
be smooth. The descent will never actually converge on a minimum.

### how to determine mini-batch size

Because of the way computer memory is structured, it is typically faster
to choose a batch size that is a power of 2, 2<sup>*n*</sup>.

#### Q:

Does mini-batch size need to be a division of the total training data
size?

#### A:

No, the last mini-batch will be the size of what ever is left over. See
assignment one for expanded information on this.

# Assignmnet 1: Optimization Methods

Until now, you’ve always used Gradient Descent to update the parameters
and minimize the cost. In this notebook, you’ll gain skills with some
more advanced optimization methods that can speed up learning and
perhaps even get you to a better final value for the cost function.
Having a good optimization algorithm can be the difference between
waiting days vs. just a few hours to get a good result.

By the end of this notebook, you’ll be able to:

-   Apply optimization methods such as (Stochastic) Gradient Descent,
    Momentum, RMSProp and Adam
-   Use random minibatches to accelerate convergence and improve
    optimization

Minimizing the cost is like finding the lowest point in a hilly
landscape. At each step of the training, you update your parameters
following a certain direction to try to get to the lowest possible
point.

**Notations**: As usual, $ = $ `da` for any variable `a`.

Let’s get started!

## Packages

``` python
# import numpy as np
# import matplotlib.pyplot as plt
# import scipy.io
# import math
# import sklearn
# import sklearn.datasets
# 
# from opt_utils_v1a import load_params_and_grads, initialize_parameters, forward_propagation, backward_propagation
# from opt_utils_v1a import compute_cost, predict, predict_dec, plot_decision_boundary, load_dataset
# from copy import deepcopy
# from testCases import *
# from public_tests import *
# 
# %matplotlib inline
# plt.rcParams['figure.figsize'] = (7.0, 4.0) # set default size of plots
# plt.rcParams['image.interpolation'] = 'nearest'
# plt.rcParams['image.cmap'] = 'gray'
# 
# %load_ext autoreload
# %autoreload 2
```

## Gradient Descent

A simple optimization method in machine learning is gradient descent
(GD). When you take gradient steps with respect to all *m* examples on
each step, it is also called Batch Gradient Descent.

<a name='ex-1'></a> \#\#\# Exercise 1 - update_parameters_with_gd

Implement the gradient descent update rule. The gradient descent rule
is, for *l* = 1, ..., *L*:
$$ W^{\[l\]} = W^{\[l\]} - \\alpha \\text{ } dW^{\[l\]} \\tag{1}$$
$$ b^{\[l\]} = b^{\[l\]} - \\alpha \\text{ } db^{\[l\]} \\tag{2}$$

where L is the number of layers and *α* is the learning rate. All
parameters should be stored in the `parameters` dictionary. Note that
the iterator `l` starts at 1 in the `for` loop as the first parameters
are *W*<sup>\[1\]</sup> and *b*<sup>\[1\]</sup>.

``` python
# GRADED FUNCTION: update_parameters_with_gd
def update_parameters_with_gd(parameters, grads, learning_rate):
    """
    Update parameters using one step of gradient descent
    Arguments:
    parameters -- python dictionary containing your parameters to be updated:
                    parameters['W' + str(l)] = Wl
                    parameters['b' + str(l)] = bl
    grads -- python dictionary containing your gradients to update each parameters:
                    grads['dW' + str(l)] = dWl
                    grads['db' + str(l)] = dbl
    learning_rate -- the learning rate, scalar.
    Returns:
    parameters -- python dictionary containing your updated parameters 
    """
    L = len(parameters) // 2 # number of layers in the neural networks
    # Update rule for each parameter
    for l in range(1, L + 1):
        # (approx. 2 lines)
        # parameters["W" + str(l)] =  
        # parameters["b" + str(l)] = 
        # YOUR CODE STARTS HERE
        parameters["W" + str(l)] = parameters["W" + str(l)] - learning_rate *  grads['dW' + str(l)]
        parameters["b" + str(l)] = parameters["b" + str(l)] - learning_rate *  grads['db' + str(l)]
        # YOUR CODE ENDS HERE
    return parameters
```

A variant of this is Stochastic Gradient Descent (SGD), which is
equivalent to mini-batch gradient descent, where each mini-batch has
just 1 example. The update rule that you have just implemented does not
change. What changes is that you would be computing gradients on just
one training example at a time, rather than on the whole training set.
The code examples below illustrate the difference between stochastic
gradient descent and (batch) gradient descent.

-   **(Batch) Gradient Descent**:

``` python
X = data_input
Y = labels
parameters = initialize_parameters(layers_dims)
for i in range(0, num_iterations):
    # Forward propagation
    a, caches = forward_propagation(X, parameters)
    # Compute cost.
    cost += compute_cost(a, Y)
    # Backward propagation.
    grads = backward_propagation(a, caches, parameters)
    # Update parameters.
    parameters = update_parameters(parameters, grads)
        
```

-   **Stochastic Gradient Descent**:

X = data_input Y = labels parameters =
initialize_parameters(layers_dims) for i in range(0, num_iterations):
for j in range(0, m): \# Forward propagation a, caches =
forward_propagation(X\[:,j\], parameters) \# Compute cost cost +=
compute_cost(a, Y\[:,j\]) \# Backward propagation grads =
backward_propagation(a, caches, parameters) \# Update parameters.
parameters = update_parameters(parameters, grads)

In Stochastic Gradient Descent, you use only 1 training example before
updating the gradients. When the training set is large, SGD can be
faster. But the parameters will “oscillate” toward the minimum rather
than converge smoothly.

**Note** also that implementing SGD requires 3 for-loops in total: 1.
Over the number of iterations 2. Over the *m* training examples 3. Over
the layers (to update all parameters, from
(*W*<sup>\[1\]</sup>, *b*<sup>\[1\]</sup>) to
(*W*<sup>\[*L*\]</sup>, *b*<sup>\[*L*\]</sup>))

In practice, you’ll often get faster results if you don’t use the entire
training set, or just one training example, to perform each update.
Mini-batch gradient descent uses an intermediate number of examples for
each step. With mini-batch gradient descent, you loop over the
mini-batches instead of looping over individual training examples.

## Mini-Batch Gradient Descent

Now you’ll build some mini-batches from the training set (X, Y).

There are two steps: - **Shuffle**: Create a shuffled version of the
training set (X, Y) as shown below. Each column of X and Y represents a
training example. Note that the random shuffling is done synchronously
between X and Y. Such that after the shuffling the *i*<sup>*t**h*</sup>
column of X is the example corresponding to the *i*<sup>*t**h*</sup>
label in Y. The shuffling step ensures that examples will be split
randomly into different mini-batches.

-   **Partition**: Partition the shuffled (X, Y) into mini-batches of
    size `mini_batch_size` (here 64). Note that the number of training
    examples is not always divisible by `mini_batch_size`. The last mini
    batch might be smaller, but you don’t need to worry about this. When
    the final mini-batch is smaller than the full `mini_batch_size`, it
    will look like this:

### Exercise 2 - random_mini_batches

Implement `random_mini_batches`. The shuffling part has already been
coded for you! To help with the partitioning step, you’ve been provided
the following code that selects the indexes for the 1<sup>*s**t*</sup>
and 2<sup>*n**d*</sup> mini-batches:

first_mini_batch_X = shuffled_X\[:, 0 : mini_batch_size\]
second_mini_batch_X = shuffled_X\[:, mini_batch_size : 2 \*
mini_batch_size\]

Note that the last mini-batch might end up smaller than
`mini_batch_size=64`. Let ⌊*s*⌋ represents *s* rounded down to the
nearest integer (this is `math.floor(s)` in Python). If the total number
of examples is not a multiple of `mini_batch_size=64` then there will be
$\\left\\lfloor \\frac{m}{mini\\\_batch\\\_size}\\right\\rfloor$
mini-batches with a full 64 examples, and the number of examples in the
final mini-batch will be
$\\left(m-mini\_\\\_batch\_\\\_size \\times \\left\\lfloor \\frac{m}{mini\\\_batch\\\_size}\\right\\rfloor\\right)$.

**Hint:**

*m**i**n**i*\_*b**a**t**c**h*\_*X* = *s**h**u**f**f**l**e**d*\_*X*\[:,*i* : *j*\]

Think of a way in which you can use the for loop variable `k` help you
increment `i` and `j` in multiples of mini_batch_size.

As an example, if you want to increment in multiples of 3, you could the
following:

n = 3 for k in (0 , 5): print(k \* n)

``` python
# GRADED FUNCTION: random_mini_batches
def random_mini_batches(X, Y, mini_batch_size = 64, seed = 0):
    """
    Creates a list of random minibatches from (X, Y)
    Arguments:
    X -- input data, of shape (input size, number of examples)
    Y -- true "label" vector (1 for blue dot / 0 for red dot), of shape (1, number of examples)
    mini_batch_size -- size of the mini-batches, integer
    Returns:
    mini_batches -- list of synchronous (mini_batch_X, mini_batch_Y)
    """
    np.random.seed(seed)            # To make your "random" minibatches the same as ours
    m = X.shape[1]                  # number of training examples
    mini_batches = []
    # Step 1: Shuffle (X, Y)
    permutation = list(np.random.permutation(m))
    shuffled_X = X[:, permutation]
    shuffled_Y = Y[:, permutation].reshape((1, m))
    inc = mini_batch_size
    # Step 2 - Partition (shuffled_X, shuffled_Y).
    # Cases with a complete mini batch size only i.e each of 64 examples.
    num_complete_minibatches = math.floor(m / mini_batch_size) # number of mini batches of size mini_batch_size in your partitionning
    for k in range(0, num_complete_minibatches):
        # (approx. 2 lines)
        # mini_batch_X =  
        # mini_batch_Y =
        # YOUR CODE STARTS HERE
        mini_batch_X = shuffled_X[:, k * mini_batch_size:(k + 1) * mini_batch_size]
        mini_batch_Y = shuffled_Y[:, k * mini_batch_size:(k + 1) * mini_batch_size]
        # YOUR CODE ENDS HERE
        mini_batch = (mini_batch_X, mini_batch_Y)
        mini_batches.append(mini_batch)
    # For handling the end case (last mini-batch < mini_batch_size i.e less than 64)
    if m % mini_batch_size != 0:
        #(approx. 2 lines)
        # mini_batch_X =
        # mini_batch_Y =
        # YOUR CODE STARTS HERE
        mini_batch_X = shuffled_X[:,num_complete_minibatches * mini_batch_size:]
        mini_batch_Y = shuffled_Y[:,num_complete_minibatches * mini_batch_size:]
        # YOUR CODE ENDS HERE
        mini_batch = (mini_batch_X, mini_batch_Y)
        mini_batches.append(mini_batch)
    return mini_batches
```

## Momentum

Because mini-batch gradient descent makes a parameter update after
seeing just a subset of examples, the direction of the update has some
variance, and so the path taken by mini-batch gradient descent will
“oscillate” toward convergence. Using momentum can reduce these
oscillations.

Momentum takes into account the past gradients to smooth out the update.
The ‘direction’ of the previous gradients is stored in the variable *v*.
Formally, this will be the exponentially weighted average of the
gradient on previous steps. You can also think of *v* as the “velocity”
of a ball rolling downhill, building up speed (and momentum) according
to the direction of the gradient/slope of the hill.

### Exercise 3 - initialize_velocity

Initialize the velocity. The velocity, *v*, is a python dictionary that
needs to be initialized with arrays of zeros. Its keys are the same as
those in the `grads` dictionary, that is: for *l* = 1, ..., *L*:

v\[“dW” + str(l)\] = … \#(numpy array of zeros with the same shape as
parameters\[“W” + str(l)\]) v\[“db” + str(l)\] = … \#(numpy array of
zeros with the same shape as parameters\[“b” + str(l)\])

**Note** that the iterator l starts at 1 in the for loop as the first
parameters are v\[“dW1”\] and v\[“db1”\] (that’s a “one” on the
superscript).

``` python
# GRADED FUNCTION: initialize_velocity
def initialize_velocity(parameters):
    """
    Initializes the velocity as a python dictionary with:
                - keys: "dW1", "db1", ..., "dWL", "dbL" 
                - values: numpy arrays of zeros of the same shape as the corresponding gradients/parameters.
    Arguments:
    parameters -- python dictionary containing your parameters.
                    parameters['W' + str(l)] = Wl
                    parameters['b' + str(l)] = bl
    Returns:
    v -- python dictionary containing the current velocity.
                    v['dW' + str(l)] = velocity of dWl
                    v['db' + str(l)] = velocity of dbl
    """
    L = len(parameters) // 2 # number of layers in the neural networks
    v = {}
    # Initialize velocity
    for l in range(1, L + 1):
        # (approx. 2 lines)
        # v["dW" + str(l)] =
        # v["db" + str(l)] =
        # YOUR CODE STARTS HERE
        v["dW" + str(l)] = np.zeros(parameters['W' + str(l)].shape)
        v["db" + str(l)] = np.zeros(parameters['b' + str(l)].shape)
        # YOUR CODE ENDS HERE
    return v
```

### Exercise 4 - update_parameters_with_momentum

Now, implement the parameters update with momentum. The momentum update
rule is, for *l* = 1, ..., *L*:

$$ \\begin{cases}
v\_{dW^{\[l\]}} = \\beta v\_{dW^{\[l\]}} + (1 - \\beta) dW^{\[l\]} \\\\
W^{\[l\]} = W^{\[l\]} - \\alpha v\_{dW^{\[l\]}}
\\end{cases}\\tag{3}$$

$$\\begin{cases}
v\_{db^{\[l\]}} = \\beta v\_{db^{\[l\]}} + (1 - \\beta) db^{\[l\]} \\\\
b^{\[l\]} = b^{\[l\]} - \\alpha v\_{db^{\[l\]}} 
\\end{cases}\\tag{4}$$

where L is the number of layers, *β* is the momentum and *α* is the
learning rate. All parameters should be stored in the `parameters`
dictionary. Note that the iterator `l` starts at 1 in the `for` loop as
the first parameters are *W*<sup>\[1\]</sup> and *b*<sup>\[1\]</sup>
(that’s a “one” on the superscript).

``` python
# GRADED FUNCTION: update_parameters_with_momentum
def update_parameters_with_momentum(parameters, grads, v, beta, learning_rate):
    """
    Update parameters using Momentum
    Arguments:
    parameters -- python dictionary containing your parameters:
                    parameters['W' + str(l)] = Wl
                    parameters['b' + str(l)] = bl
    grads -- python dictionary containing your gradients for each parameters:
                    grads['dW' + str(l)] = dWl
                    grads['db' + str(l)] = dbl
    v -- python dictionary containing the current velocity:
                    v['dW' + str(l)] = ...
                    v['db' + str(l)] = ...
    beta -- the momentum hyperparameter, scalar
    learning_rate -- the learning rate, scalar
    Returns:
    parameters -- python dictionary containing your updated parameters 
    v -- python dictionary containing your updated velocities
    """
    L = len(parameters) // 2 # number of layers in the neural networks
    # Momentum update for each parameter
    for l in range(1, L + 1):
        # (approx. 4 lines)
        # compute velocities
        # v["dW" + str(l)] = ...
        # v["db" + str(l)] = ...
        # update parameters
        # parameters["W" + str(l)] = ...
        # parameters["b" + str(l)] = ...
        # YOUR CODE STARTS HERE
        v["dW" + str(l)] = (beta * v['dW' + str(l)]) + (1-beta) * grads["dW" + str(l)]
        v["db" + str(l)] = beta * v['db' + str(l)] + (1-beta) * grads["db" + str(l)]
        parameters["W" + str(l)] = parameters["W" + str(l)] - learning_rate * v['dW' + str(l)]
        parameters["b" + str(l)] = parameters["b" + str(l)] - learning_rate * v['db' + str(l)]
        # YOUR CODE ENDS HERE
    return parameters, v
```

## Adam

Adam is one of the most effective optimization algorithms for training
neural networks. It combines ideas from RMSProp (described in lecture)
and Momentum.

**How does Adam work?** 1. It calculates an exponentially weighted
average of past gradients, and stores it in variables *v* (before bias
correction) and *v*<sup>*c**o**r**r**e**c**t**e**d*</sup> (with bias
correction). 2. It calculates an exponentially weighted average of the
squares of the past gradients, and stores it in variables *s* (before
bias correction) and *s*<sup>*c**o**r**r**e**c**t**e**d*</sup> (with
bias correction). 3. It updates parameters in a direction based on
combining information from “1” and “2”.

The update rule is, for *l* = 1, ..., *L*:

$$\\begin{cases}
v\_{dW^{\[l\]}} = \\beta_1 v\_{dW^{\[l\]}} + (1 - \\beta_1) \\frac{\\partial \\mathcal{J} }{ \\partial W^{\[l\]} } \\\\
v^{corrected}\_{dW^{\[l\]}} = \\frac{v\_{dW^{\[l\]}}}{1 - (\\beta_1)^t} \\\\
s\_{dW^{\[l\]}} = \\beta_2 s\_{dW^{\[l\]}} + (1 - \\beta_2) (\\frac{\\partial \\mathcal{J} }{\\partial W^{\[l\]} })^2 \\\\
s^{corrected}\_{dW^{\[l\]}} = \\frac{s\_{dW^{\[l\]}}}{1 - (\\beta_2)^t} \\\\
W^{\[l\]} = W^{\[l\]} - \\alpha \\frac{v^{corrected}\_{dW^{\[l\]}}}{\\sqrt{s^{corrected}\_{dW^{\[l\]}}} + \\varepsilon}
\\end{cases}$$
where: - t counts the number of steps taken of Adam - L is the number of
layers - *β*<sub>1</sub> and *β*<sub>2</sub> are hyperparameters that
control the two exponentially weighted averages. - *α* is the learning
rate - *ε* is a very small number to avoid dividing by zero

As usual, all parameters are stored in the `parameters` dictionary

### Exercise 5 - initialize_adam

Initialize the Adam variables *v*, *s* which keep track of the past
information.

**Instruction**: The variables *v*, *s* are python dictionaries that
need to be initialized with arrays of zeros. Their keys are the same as
for `grads`, that is: for *l* = 1, ..., *L*:

``` python
v["dW" + str(l)] = ... #(numpy array of zeros with the same shape as parameters["W" + str(l)])
v["db" + str(l)] = ... #(numpy array of zeros with the same shape as parameters["b" + str(l)])
s["dW" + str(l)] = ... #(numpy array of zeros with the same shape as parameters["W" + str(l)])
s["db" + str(l)] = ... #(numpy array of zeros with the same shape as parameters["b" + str(l)])
```

``` python
# GRADED FUNCTION: initialize_adam
def initialize_adam(parameters) :
    """
    Initializes v and s as two python dictionaries with:
                - keys: "dW1", "db1", ..., "dWL", "dbL" 
                - values: numpy arrays of zeros of the same shape as the corresponding gradients/parameters.
    Arguments:
    parameters -- python dictionary containing your parameters.
                    parameters["W" + str(l)] = Wl
                    parameters["b" + str(l)] = bl
    Returns: 
    v -- python dictionary that will contain the exponentially weighted average of the gradient. Initialized with zeros.
                    v["dW" + str(l)] = ...
                    v["db" + str(l)] = ...
    s -- python dictionary that will contain the exponentially weighted average of the squared gradient. Initialized with zeros.
                    s["dW" + str(l)] = ...
                    s["db" + str(l)] = ...

    """
    L = len(parameters) // 2 # number of layers in the neural networks
    v = {}
    s = {}
    # Initialize v, s. Input: "parameters". Outputs: "v, s".
    for l in range(1, L + 1):
    # (approx. 4 lines)
        # v["dW" + str(l)] = ...
        # v["db" + str(l)] = ...
        # s["dW" + str(l)] = ...
        # s["db" + str(l)] = ...
    # YOUR CODE STARTS HERE
        v["dW" + str(l)] = np.zeros(parameters["W" + str(l)].shape)
        v["db" + str(l)] = np.zeros(parameters["b" + str(l)].shape)
        s["dW" + str(l)] = np.zeros(parameters["W" + str(l)].shape)
        s["db" + str(l)] = np.zeros(parameters["b" + str(l)].shape)
    # YOUR CODE ENDS HERE
    return v, s
```

### Exercise 6 - update_parameters_with_adam

Now, implement the parameters update with Adam. Recall the general
update rule is, for *l* = 1, ..., *L*:

$$\\begin{cases}
v\_{dW^{\[l\]}} = \\beta_1 v\_{dW^{\[l\]}} + (1 - \\beta_1) \\frac{\\partial \\mathcal{J} }{ \\partial W^{\[l\]} } \\\\
v^{corrected}\_{dW^{\[l\]}} = \\frac{v\_{dW^{\[l\]}}}{1 - (\\beta_1)^t} \\\\
s\_{dW^{\[l\]}} = \\beta_2 s\_{dW^{\[l\]}} + (1 - \\beta_2) (\\frac{\\partial \\mathcal{J} }{\\partial W^{\[l\]} })^2 \\\\
s^{corrected}\_{dW^{\[l\]}} = \\frac{s\_{dW^{\[l\]}}}{1 - (\\beta_2)^t} \\\\
W^{\[l\]} = W^{\[l\]} - \\alpha \\frac{v^{corrected}\_{dW^{\[l\]}}}{\\sqrt{s^{corrected}\_{dW^{\[l\]}}} + \\varepsilon}
\\end{cases}$$

**Note** that the iterator `l` starts at 1 in the `for` loop as the
first parameters are *W*<sup>\[1\]</sup> and *b*<sup>\[1\]</sup>.

``` python
# GRADED FUNCTION: update_parameters_with_adam
def update_parameters_with_adam(parameters, grads, v, s, t, learning_rate = 0.01,
                                beta1 = 0.9, beta2 = 0.999,  epsilon = 1e-8):
    """
    Update parameters using Adam
    Arguments:
    parameters -- python dictionary containing your parameters:
                    parameters['W' + str(l)] = Wl
                    parameters['b' + str(l)] = bl
    grads -- python dictionary containing your gradients for each parameters:
                    grads['dW' + str(l)] = dWl
                    grads['db' + str(l)] = dbl
    v -- Adam variable, moving average of the first gradient, python dictionary
    s -- Adam variable, moving average of the squared gradient, python dictionary
    t -- Adam variable, counts the number of taken steps
    learning_rate -- the learning rate, scalar.
    beta1 -- Exponential decay hyperparameter for the first moment estimates 
    beta2 -- Exponential decay hyperparameter for the second moment estimates 
    epsilon -- hyperparameter preventing division by zero in Adam updates
    Returns:
    parameters -- python dictionary containing your updated parameters 
    v -- Adam variable, moving average of the first gradient, python dictionary
    s -- Adam variable, moving average of the squared gradient, python dictionary
    """
    L = len(parameters) // 2                 # number of layers in the neural networks
    v_corrected = {}                         # Initializing first moment estimate, python dictionary
    s_corrected = {}                         # Initializing second moment estimate, python dictionary
    # Perform Adam update on all parameters
    for l in range(1, L + 1):
        # Moving average of the gradients. Inputs: "v, grads, beta1". Output: "v".
        # (approx. 2 lines)
        # v["dW" + str(l)] = ...
        # v["db" + str(l)] = ...
        # YOUR CODE STARTS HERE
        v["dW" + str(l)] = beta1 * v["dW" + str(l)] + (1-beta1) * grads['dW' + str(l)]
        v["db" + str(l)] = beta1 * v["db" + str(l)] + (1-beta1) * grads['db' + str(l)]
        # YOUR CODE ENDS HERE
        # Compute bias-corrected first moment estimate. Inputs: "v, beta1, t". Output: "v_corrected".
        # (approx. 2 lines)
        # v_corrected["dW" + str(l)] = ...
        # v_corrected["db" + str(l)] = ...
        # YOUR CODE STARTS HERE
        v_corrected["dW" + str(l)] = v["dW" + str(l)] / (1 - np.power(beta1, t))
        v_corrected["db" + str(l)] = v["db" + str(l)] / (1 - np.power(beta1, t))
        # YOUR CODE ENDS HERE
        # Moving average of the squared gradients. Inputs: "s, grads, beta2". Output: "s".
        #(approx. 2 lines)
        # s["dW" + str(l)] = ...
        # s["db" + str(l)] = ...
        # YOUR CODE STARTS HERE
        s["dW" + str(l)] = beta2 * s["dW" + str(l)] + (1-beta2) * np.power(grads['dW' + str(l)],2)
        s["db" + str(l)] = beta2 * s["db" + str(l)] + (1-beta2) * np.power(grads['db' + str(l)], 2)
        # YOUR CODE ENDS HERE
        # Compute bias-corrected second raw moment estimate. Inputs: "s, beta2, t". Output: "s_corrected".
        # (approx. 2 lines)
        # s_corrected["dW" + str(l)] = ...
        # s_corrected["db" + str(l)] = ...
        # YOUR CODE STARTS HERE
        s_corrected["dW" + str(l)] = s["dW" + str(l)] / (1 - np.power(beta2, t))
        s_corrected["db" + str(l)] = s["db" + str(l)] / (1 - np.power(beta2, t))
        # YOUR CODE ENDS HERE
        # Update parameters. Inputs: "parameters, learning_rate, v_corrected, s_corrected, epsilon". Output: "parameters".
        # (approx. 2 lines)
        # parameters["W" + str(l)] = ...
        # parameters["b" + str(l)] = ...
        # YOUR CODE STARTS HERE
        parameters["W" + str(l)] = parameters["W" + str(l)] - learning_rate * (v_corrected["dW" + str(l)] / (np.sqrt( s_corrected["dW" + str(l)]) + epsilon))
        parameters["b" + str(l)] = parameters["b" + str(l)] - learning_rate * (v_corrected["db" + str(l)] / (np.sqrt( s_corrected["db" + str(l)]) + epsilon))
        # YOUR CODE ENDS HERE
    return parameters, v, s, v_corrected, s_corrected
```

## Model with different Optimization algorithms

Below, you’ll use the following “moons” dataset to test the different
optimization methods. (The dataset is named “moons” because the data
from each of the two classes looks a bit like a crescent-shaped moon.)

``` r
r1 <- c(-2.16870135e-01,  8.05050153e-01,  7.11274513e-01,
         2.10855562e+00,  9.07466356e-01, -6.99754482e-01,
         6.34769404e-01,  1.61148090e+00,  6.80478695e-01,
         1.19246568e-01,  6.87898975e-01, -3.79994196e-01,
         1.38239247e+00,  6.32238881e-02,  3.44336279e-01,
        -5.40369852e-01,  1.54524651e+00,  1.21620856e+00,
        -2.81968088e-01,  3.37818125e-01,  2.89572376e-01,
         1.22151229e+00,  6.17670618e-01, -8.21854477e-01,
         6.56270495e-02,  6.72513212e-01, -1.74085800e-02,
        -4.89767505e-01,  9.82532807e-01,  1.17044851e+00,
         1.87353038e+00,  7.49082806e-01, -2.92082604e-01,
         7.03428656e-01,  1.35221268e+00,  1.78563046e+00,
        -5.93922850e-02,  1.24883046e+00,  1.45633899e-01,
        -8.74555076e-01, -1.23907713e+00, -7.34564403e-01,
         2.12768023e+00,  8.60677104e-01, -8.75243737e-01,
        -5.87997143e-01,  5.49996407e-01,  1.44421582e+00,
        -1.04516149e+00,  1.05948597e+00,  2.47798782e-01,
         4.71222214e-01, -2.90242517e-01,  6.10769470e-01,
         2.54438719e-01,  4.42959120e-01,  4.39141265e-02,
         4.00819419e-01,  8.93424528e-01, -1.77159810e-01,
         1.13907848e-01,  4.88450820e-01,  8.46805305e-01,
        -1.01663250e+00,  4.94069660e-01,  2.86584342e-01,
         4.91055929e-01,  6.99034088e-01,  7.64703975e-01,
         1.03451364e+00, -1.37761046e-01,  9.05956757e-01,
        -8.29509766e-01,  2.05507463e-01, -1.00393011e+00,
         1.59794243e+00,  2.18388102e+00,  1.96376230e+00,
         1.24283666e+00,  2.71395313e-01,  4.83425649e-01,
        -2.21378063e-01, -5.86153698e-01,  1.42595368e-01,
         1.12169598e+00,  5.93933380e-01,  1.27344278e+00,
         2.16686053e+00, -3.74653683e-01,  2.49617687e-01,
         1.73097632e+00,  5.48453853e-01, -8.65915643e-01,
         2.06345508e+00,  3.54974731e-01,  7.48590362e-01,
         1.07966415e+00,  1.87211505e+00,  4.23953940e-01,
         1.04308469e+00,  1.14001900e+00,  1.03907095e+00,
        -1.29844754e-01,  1.99240955e+00,  1.23862427e+00,
         1.32936502e-01, -4.56884916e-01,  1.14225064e+00,
         1.45536171e+00,  2.12521801e+00, -5.98017062e-01,
         1.23747557e+00,  1.33308237e+00, -8.87618834e-01,
         1.71310515e+00, -9.47954031e-01,  1.11588212e+00,
        -6.69897489e-01,  1.37244930e-01,  1.78792529e+00,
        -7.84171268e-01, -1.01932439e+00,  2.83176861e-01,
        -9.93545784e-01,  8.48506490e-01,  1.74998367e+00,
        -6.90523553e-01,  1.68835318e+00,  1.16347014e+00,
        -4.93354750e-01, -1.67954124e-01,  1.62997765e+00,
        -5.62715873e-01,  9.76862761e-01,  2.38976771e-01,
         1.98425205e+00,  8.84044205e-01,  1.06895184e-01,
         7.40204573e-01,  7.15394581e-01,  9.24129985e-01,
         2.00208816e+00,  5.89712746e-01, -6.81140580e-01,
        -8.44283953e-01, -2.62944117e-01, -2.77387315e-01,
         2.03838355e-01,  5.71966387e-01,  1.16640825e+00,
        -3.04510734e-01, -4.77414593e-01, -1.04862575e+00,
        -8.18034835e-01,  4.21940186e-01,  1.59756758e+00,
         2.17892896e+00,  1.43709370e-01, -6.91839559e-01,
         1.45289405e+00,  7.22837237e-01,  2.27559062e-01,
         1.60843155e+00,  1.36888023e+00,  4.67933232e-01,
         8.64959704e-01, -3.41131125e-02,  2.19974674e+00,
        -4.88932102e-01, -1.30341704e+00,  1.31097192e+00,
         2.15849236e+00, -1.11091324e+00, -6.58677061e-01,
         6.45255467e-01,  7.82901227e-01,  2.00209912e+00,
         3.38370678e-01,  1.88430341e+00,  7.32743963e-01,
        -4.50294604e-01,  5.05220004e-01,  7.03832173e-01,
         6.87573251e-01,  5.49392241e-01, -7.00893049e-01,
         8.92249631e-01,  1.65918394e+00,  2.24523084e-01,
        -1.04151523e+00,  4.79792298e-01,  2.15008104e-01,
         1.99363538e+00,  1.27797042e+00,  4.59481662e-01,
         1.55297320e+00,  1.85862923e+00,  1.01769658e+00,
         3.41438578e-01,  7.10225674e-01,  3.77477808e-01,
        -2.58301102e-01,  4.49199197e-01, -9.07958648e-01,
         1.97912474e+00,  7.42635712e-01,  3.01358251e-01,
         5.35218194e-01, -1.46562353e-01,  3.03475663e-02,
         6.98232512e-01, -1.04494818e+00,  1.99421320e+00,
         5.26977500e-01,  8.46647375e-01,  8.03692382e-01,
         3.54931705e-01,  1.52939367e+00,  7.52688187e-01,
         1.44098035e+00,  1.64025582e+00,  6.39740487e-01,
         8.23890843e-01, -7.13246829e-01,  6.64936674e-01,
         1.34055132e+00,  1.18320976e+00, -2.03728248e-01,
         8.44640533e-01,  3.55185091e-01,  1.87670823e+00,
        -3.63944122e-02,  1.99222363e+00,  9.79012096e-02,
         9.01101619e-01, -3.65408277e-01,  5.29305831e-01,
         1.39918799e-01,  1.10178035e+00,  4.10818647e-01,
         1.73014740e+00,  4.50112960e-01,  6.79886818e-02,
        -2.91199517e-01,  1.74029979e+00, -8.81115193e-01,
         1.05808413e+00, -6.11485609e-02,  2.05813243e-01,
         9.80652437e-02,  1.15043824e+00,  1.36997106e+00,
         1.38465758e+00, -6.86185820e-02, -2.95259902e-02,
        -1.30260119e+00,  6.71076465e-01, -3.38104294e-01,
         1.19058425e+00, -7.00983811e-01,  7.63433945e-01,
        -7.29398143e-01,  2.02512615e+00, -1.61915027e-01,
         1.39714160e+00,  1.60699227e+00, -3.32702926e-02,
         1.68646136e-01,  2.68332656e-01,  8.96272070e-01,
         8.34584820e-01,  6.07553628e-01,  4.39755419e-01,
         2.95626404e-01,  7.95245262e-01, -2.32597515e-01,
         1.13026039e+00, -8.07312499e-02,  1.04985508e+00,
         2.08741891e+00, -8.32856358e-01, -8.05161553e-01,
        -5.22864195e-01, -1.34878701e-01, -9.81764880e-01,
         1.36562815e+00, -1.99293557e-01,  7.79079038e-01,
        -5.41884806e-01, -1.00265146e+00, -8.68221827e-01,
        -9.44233966e-01,  1.96018850e-01,  6.97623239e-01,
        -1.11938949e+00,  1.86277284e+00,  1.63238229e+00,
        -8.80516373e-01,  1.60879915e+00,  5.47725598e-01)

r2 <- c(1.01544937e+00, -5.57973395e-01, -4.10060241e-01,
         5.20814914e-01,  4.59349431e-01,  1.89195603e-01,
        -1.38994928e-01, -1.22931655e-01,  3.56746183e-01,
        -1.35143765e-01,  5.40099343e-01,  7.48716064e-01,
        -2.73135578e-01,  1.00753602e+00,  8.97523249e-01,
         6.00922628e-01, -2.84601480e-01, -3.91844343e-01,
         6.56767552e-01,  6.94651275e-01,  1.06866038e+00,
        -4.41381580e-01, -1.88074515e-01,  4.40047373e-01,
         2.73709682e-01, -6.21054526e-01,  1.08911514e+00,
         8.11800924e-01, -4.46309917e-01,  1.46216926e-01,
        -2.40278347e-01, -2.58569240e-01,  1.17665718e+00,
         7.49549129e-01, -3.84182689e-01, -1.59569114e-01,
        -3.51849158e-02, -7.42929172e-02,  1.52261566e-01,
         4.45815988e-01,  4.97125129e-01,  5.50117402e-01,
         1.66845202e-01, -1.79130385e-01,  4.84538693e-01,
         3.78067502e-01,  6.66105720e-01,  1.02851181e-01,
         4.73969996e-01, -7.17470329e-01,  5.66932315e-01,
         1.39228990e+00,  1.30763649e+00, -4.34472669e-01,
         9.62219293e-01,  7.73996279e-01,  7.34034915e-01,
         6.81964208e-01, -1.12317838e-01,  1.37892939e+00,
         4.77679215e-01,  1.04839370e+00, -2.87091040e-01,
        -1.25949422e-01, -4.07136374e-01,  1.10400079e+00,
        -4.44255111e-01,  4.67948433e-01,  4.58543189e-01,
         8.18690910e-01,  9.23081979e-01,  2.46562095e-01,
         6.53334865e-01,  5.12266102e-01,  3.85291607e-01,
        -5.66298309e-02,  5.97263340e-01,  2.25804614e-02,
        -2.53299858e-01, -3.60507102e-01,  5.21688138e-01,
         7.70792599e-02,  9.10317363e-01, -3.22941279e-01,
         7.48403796e-01, -3.47552202e-01, -2.91934463e-01,
         7.16773318e-01,  7.50109573e-01,  2.19123222e-01,
         2.98675452e-01,  6.51330765e-01, -1.27587457e-01,
         2.97559468e-01,  2.68738142e-01,  4.67303288e-01,
        -4.80578090e-01,  1.39647304e-01,  1.21551747e+00,
        -4.08905422e-01, -3.17798366e-01, -3.21310143e-01,
         9.10032935e-01,  2.97348578e-01, -5.94931410e-01,
         6.20488501e-01,  5.14128043e-01, -5.96361396e-01,
        -1.02595920e-01,  1.13418504e-02,  4.90658354e-01,
         3.80657745e-02, -4.69545470e-01,  8.54794238e-01,
        -2.96258152e-01,  5.78155576e-01,  5.07746917e-01,
         8.30605986e-01,  1.13105141e+00, -9.29501962e-02,
         4.32556432e-01, -4.51030734e-01, -1.81777179e-02,
         5.57437105e-01,  3.04172732e-01, -6.79468004e-05,
         7.35075196e-01, -2.09504837e-01,  3.05402879e-01,
         9.00492298e-01,  3.27698093e-01, -6.40578544e-01,
         2.57402548e-01,  4.55012646e-01,  9.21624599e-01,
         5.30588100e-01,  1.10975872e-01,  9.98806804e-02,
         2.59652762e-01, -2.15652559e-01, -5.07228436e-01,
         1.58168224e-01,  1.13375135e+00,  7.93125817e-01,
         5.69954717e-01,  1.13135244e+00,  3.97401912e-01,
         1.16933433e-01,  1.93918759e-01, -3.65180897e-01,
         1.25954564e+00,  4.88776303e-01,  7.03111770e-01,
         1.54737857e-01, -2.09030334e-01,  5.90954431e-02,
         4.32351830e-01, -8.13421157e-01,  3.85291079e-01,
        -2.32744275e-01, -4.43991363e-01,  1.11194995e+00,
        -3.30240940e-02, -4.41190276e-01, -1.69862057e-01,
        -7.32280605e-01,  1.01848005e+00,  4.30990304e-01,
         6.95002336e-01, -5.30664060e-02, -1.03635739e-01,
         3.83459985e-01,  1.06166297e-01,  8.69701210e-01,
        -5.94398823e-01, -6.80223595e-01,  1.09860534e-01,
         6.61293294e-02, -1.80527696e-01, -1.78341514e-01,
         8.49388035e-01,  7.73227594e-01,  3.26015882e-01,
        -3.10180916e-01,  6.51636320e-01,  9.43466684e-01,
         6.33818130e-01,  5.90838784e-01, -1.67560218e-01,
         6.36667275e-02,  2.97567013e-01,  1.06753719e+00,
         3.03894161e-01, -4.60215294e-01, -3.01836471e-01,
        -1.23068068e-01, -1.54044026e-01, -6.72469350e-01,
         3.98870041e-01,  5.12452945e-01, -5.01092338e-01,
         7.53552847e-02,  1.30161701e-01,  4.08446160e-01,
        -4.16776600e-01,  6.00316816e-01, -4.80539998e-01,
         7.10889419e-01,  1.14646022e+00,  6.64502207e-01,
         1.51993782e-01,  6.83668298e-01,  5.19545167e-02,
        -4.02098035e-01,  3.55474298e-01,  1.23641896e+00,
        -1.27674560e-01, -4.16844377e-01,  1.71519694e-01,
        -4.81916972e-01,  1.70722112e-01,  7.38886395e-01,
         8.40708485e-01,  9.23759772e-01,  1.06221681e+00,
        -2.89793585e-02,  4.46756227e-01,  5.78151143e-01,
        -4.17386011e-01, -2.41531124e-01,  4.56498732e-01,
         1.16575109e+00, -1.40847936e-01,  2.55055540e-01,
        -5.81911650e-01,  1.14999598e+00,  6.08676226e-01,
         7.31932028e-01, -6.46385532e-01,  1.06064049e+00,
        -1.47470149e-01, -3.75257475e-01,  7.09820530e-01,
        -1.35041969e-01, -8.05748129e-03,  1.02717736e-01,
        -2.28148343e-01,  8.24954420e-01, -1.04352199e-01,
         1.26893341e+00,  4.04475916e-01, -4.63652539e-01,
        -2.31568608e-01,  5.40220166e-01,  9.14930158e-01,
         4.43960816e-01, -7.43782327e-01,  1.04173670e+00,
        -4.80957481e-01,  6.64670218e-01, -1.50800932e-01,
         1.09370658e+00,  8.31939424e-02,  5.89342298e-02,
        -1.89091551e-02, -1.00026093e-02, -1.98403616e-01,
         2.02192391e-01, -4.53814246e-01, -3.11407586e-01,
         1.14375753e+00,  8.08732093e-01, -3.18364140e-02,
         1.08748076e+00,  5.61392793e-01,  2.80254410e-01,
         8.96621962e-01,  2.09292937e-01,  9.63820085e-02,
         5.51488004e-01,  9.41488263e-01,  1.00241391e+00,
         8.78248354e-01,  1.35887716e+00,  7.64083614e-01,
        -1.19816257e-01,  9.04415370e-01,  5.08765439e-01,
         8.10605668e-01,  2.11935141e-02,  3.03410213e-01,
         3.04560751e-01,  4.02221985e-01, -3.03356687e-01,
         5.18623653e-01, -7.26882281e-02, -5.13632389e-01,
         1.57830284e-01, -6.65823551e-01,  3.48174614e-01)

train_X <- as.data.frame(r1, .rows = 1) %>% mutate(r2 = r2)
```

``` r
train_Y <- c(0, 1, 1, 1, 0, 0, 1, 1, 0, 1, 0, 0, 1, 0, 0, 0, 1, 1, 1, 0, 0, 1,
        1, 0, 1, 1, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1,
        0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 1, 0,
        1, 0, 0, 0, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 0, 1, 0, 1, 0, 1, 1, 1,
        0, 1, 1, 0, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 0, 1, 1, 1,
        0, 0, 1, 0, 1, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 1, 0, 0, 1, 1,
        0, 0, 0, 1, 0, 1, 0, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 0, 1, 0, 0,
        1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 1,
        1, 1, 1, 0, 0, 0, 0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1,
        1, 0, 1, 1, 1, 0, 1, 0, 1, 0, 0, 0, 1, 0, 1, 1, 0, 0, 1, 1, 0, 1,
        1, 0, 0, 0, 0, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 0, 0, 1, 1,
        0, 1, 1, 0, 1, 0, 1, 0, 0, 1, 1, 1, 0, 0, 1, 0, 1, 0, 0, 0, 1, 1,
        1, 1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 1, 0, 1, 0, 1, 0, 0, 0, 0, 0, 1,
        1, 0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 0, 1, 1)
```

``` r
train_Xr <- train_X %>% 
  mutate(classifier = train_Y) %>%
  as.data.frame()

ggplot() +
  geom_point(aes(x = train_Xr[,1], y = train_Xr[,2], color = train_Xr[,3]))
```

![](Improving_Deep_Neural_Networks_files/figure-markdown_github/unnamed-chunk-38-1.png)

A 3-layer neural network has already been implemented for you! You’ll
train it with: - Mini-batch **Gradient Descent**: it will call your
function: - `update_parameters_with_gd()` - Mini-batch **Momentum**: it
will call your functions: - `initialize_velocity()` and
`update_parameters_with_momentum()` - Mini-batch **Adam**: it will call
your functions: - `initialize_adam()` and
`update_parameters_with_adam()`

``` python
def model(X, Y, layers_dims, optimizer, learning_rate = 0.0007, mini_batch_size = 64, beta = 0.9,
          beta1 = 0.9, beta2 = 0.999,  epsilon = 1e-8, num_epochs = 5000, print_cost = True):
    """
    3-layer neural network model which can be run in different optimizer modes.
    Arguments:
    X -- input data, of shape (2, number of examples)
    Y -- true "label" vector (1 for blue dot / 0 for red dot), of shape (1, number of examples)
    layers_dims -- python list, containing the size of each layer
    learning_rate -- the learning rate, scalar.
    mini_batch_size -- the size of a mini batch
    beta -- Momentum hyperparameter
    beta1 -- Exponential decay hyperparameter for the past gradients estimates 
    beta2 -- Exponential decay hyperparameter for the past squared gradients estimates 
    epsilon -- hyperparameter preventing division by zero in Adam updates
    num_epochs -- number of epochs
    print_cost -- True to print the cost every 1000 epochs
    Returns:
    parameters -- python dictionary containing your updated parameters 
    """
    L = len(layers_dims)             # number of layers in the neural networks
    costs = []                       # to keep track of the cost
    t = 0                            # initializing the counter required for Adam update
    seed = 10                        # For grading purposes, so that your "random" minibatches are the same as ours
    m = X.shape[1]                   # number of training examples
    # Initialize parameters
    parameters = initialize_parameters(layers_dims)
    # Initialize the optimizer
    if optimizer == "gd":
        pass # no initialization required for gradient descent
    elif optimizer == "momentum":
        v = initialize_velocity(parameters)
    elif optimizer == "adam":
        v, s = initialize_adam(parameters)
    # Optimization loop
    for i in range(num_epochs):
        # Define the random minibatches. We increment the seed to reshuffle differently the dataset after each epoch
        seed = seed + 1
        minibatches = random_mini_batches(X, Y, mini_batch_size, seed)
        cost_total = 0
        for minibatch in minibatches:
            # Select a minibatch
            (minibatch_X, minibatch_Y) = minibatch
            # Forward propagation
            a3, caches = forward_propagation(minibatch_X, parameters)
            # Compute cost and add to the cost total
            cost_total += compute_cost(a3, minibatch_Y)
            # Backward propagation
            grads = backward_propagation(minibatch_X, minibatch_Y, caches)
            # Update parameters
            if optimizer == "gd":
                parameters = update_parameters_with_gd(parameters, grads, learning_rate)
            elif optimizer == "momentum":
                parameters, v = update_parameters_with_momentum(parameters, grads, v, beta, learning_rate)
            elif optimizer == "adam":
                t = t + 1 # Adam counter
                parameters, v, s, _, _ = update_parameters_with_adam(parameters, grads, v, s,
                                                               t, learning_rate, beta1, beta2,  epsilon)
        cost_avg = cost_total / m
        # Print the cost every 1000 epoch
        if print_cost and i % 1000 == 0:
            print ("Cost after epoch %i: %f" %(i, cost_avg))
        if print_cost and i % 100 == 0:
            costs.append(cost_avg)
    # plot the cost
    plt.plot(costs)
    plt.ylabel('cost')
    plt.xlabel('epochs (per 100)')
    plt.title("Learning rate = " + str(learning_rate))
    plt.show()
    return parameters
```

### Mini-Batch Gradient Descent

Run the following code to see how the model does with mini-batch
gradient descent.

![](~/Desktop/Bryan/deep_learning.ai_coursera/Month2:Tunning%20Hyperparameters/b1.png)

### Mini-Batch Gradient Descent with Momentum

Next, run the following code to see how the model does with momentum.
Because this example is relatively simple, the gains from using momentum
are small - but for more complex problems you might see bigger gains.

![](~/Desktop/Bryan/deep_learning.ai_coursera/Month2:Tunning%20Hyperparameters/b2.png)

### Mini-Batch with Adam

Finally, run the following code to see how the model does with Adam.

![](~/Desktop/Bryan/deep_learning.ai_coursera/Month2:Tunning%20Hyperparameters/b3.png)

### Summary

Momentum usually helps, but given the small learning rate and the
simplistic dataset, its impact is almost negligible.

On the other hand, Adam clearly outperforms mini-batch gradient descent
and Momentum. If you run the model for more epochs on this simple
dataset, all three methods will lead to very good results. However,
you’ve seen that Adam converges a lot faster.

Some advantages of Adam include:

-   Relatively low memory requirements (though higher than gradient
    descent and gradient descent with momentum)
-   Usually works well even with little tuning of hyperparameters
    (except *α*)

\#\#Learning Rate Decay and Scheduling

Lastly, the learning rate is another hyperparameter that can help you
speed up learning.

During the first part of training, your model can get away with taking
large steps, but over time, using a fixed value for the learning rate
alpha can cause your model to get stuck in a wide oscillation that never
quite converges. But if you were to slowly reduce your learning rate
alpha over time, you could then take smaller, slower steps that bring
you closer to the minimum. This is the idea behind learning rate decay.

Learning rate decay can be achieved by using either adaptive methods or
pre-defined learning rate schedules.

Now, you’ll apply scheduled learning rate decay to a 3-layer neural
network in three different optimizer modes and see how each one differs,
as well as the effect of scheduling at different epochs.

This model is essentially the same as the one you used before, except in
this one you’ll be able to include learning rate decay. It includes two
new parameters, decay and decay_rate.

``` python
def model(X, Y, layers_dims, optimizer, learning_rate = 0.0007, mini_batch_size = 64, beta = 0.9,
          beta1 = 0.9, beta2 = 0.999,  epsilon = 1e-8, num_epochs = 5000, print_cost = True, decay=None, decay_rate=1):
    """
    3-layer neural network model which can be run in different optimizer modes.
    Arguments:
    X -- input data, of shape (2, number of examples)
    Y -- true "label" vector (1 for blue dot / 0 for red dot), of shape (1, number of examples)
    layers_dims -- python list, containing the size of each layer
    learning_rate -- the learning rate, scalar.
    mini_batch_size -- the size of a mini batch
    beta -- Momentum hyperparameter
    beta1 -- Exponential decay hyperparameter for the past gradients estimates 
    beta2 -- Exponential decay hyperparameter for the past squared gradients estimates 
    epsilon -- hyperparameter preventing division by zero in Adam updates
    num_epochs -- number of epochs
    print_cost -- True to print the cost every 1000 epochs
    Returns:
    parameters -- python dictionary containing your updated parameters 
    """
    L = len(layers_dims)             # number of layers in the neural networks
    costs = []                       # to keep track of the cost
    t = 0                            # initializing the counter required for Adam update
    seed = 10                        # For grading purposes, so that your "random" minibatches are the same as ours
    m = X.shape[1]                   # number of training examples
    lr_rates = []
    learning_rate0 = learning_rate   # the original learning rate
    # Initialize parameters
    parameters = initialize_parameters(layers_dims)
    # Initialize the optimizer
    if optimizer == "gd":
        pass # no initialization required for gradient descent
    elif optimizer == "momentum":
        v = initialize_velocity(parameters)
    elif optimizer == "adam":
        v, s = initialize_adam(parameters)
    # Optimization loop
    for i in range(num_epochs):
        # Define the random minibatches. We increment the seed to reshuffle differently the dataset after each epoch
        seed = seed + 1
        minibatches = random_mini_batches(X, Y, mini_batch_size, seed)
        cost_total = 0
        for minibatch in minibatches:
            # Select a minibatch
            (minibatch_X, minibatch_Y) = minibatch
            # Forward propagation
            a3, caches = forward_propagation(minibatch_X, parameters)
            # Compute cost and add to the cost total
            cost_total += compute_cost(a3, minibatch_Y)
            # Backward propagation
            grads = backward_propagation(minibatch_X, minibatch_Y, caches)
            # Update parameters
            if optimizer == "gd":
                parameters = update_parameters_with_gd(parameters, grads, learning_rate)
            elif optimizer == "momentum":
                parameters, v = update_parameters_with_momentum(parameters, grads, v, beta, learning_rate)
            elif optimizer == "adam":
                t = t + 1 # Adam counter
                parameters, v, s, _, _ = update_parameters_with_adam(parameters, grads, v, s,
                                                               t, learning_rate, beta1, beta2,  epsilon)
        cost_avg = cost_total / m
        if decay:
            learning_rate = decay(learning_rate0, i, decay_rate)
        # Print the cost every 1000 epoch
        if print_cost and i % 1000 == 0:
            print ("Cost after epoch %i: %f" %(i, cost_avg))
            if decay:
                print("learning rate after epoch %i: %f"%(i, learning_rate))
        if print_cost and i % 100 == 0:
            costs.append(cost_avg)
    # plot the cost
    plt.plot(costs)
    plt.ylabel('cost')
    plt.xlabel('epochs (per 100)')
    plt.title("Learning rate = " + str(learning_rate))
    plt.show()
    return parameters
```

### Decay on every iteration

For this portion of the assignment, you’ll try one of the pre-defined
schedules for learning rate decay, called exponential learning rate
decay. It takes this mathematical form:

$$\\alpha = \\frac{1}{1 + decayRate \\times epochNumber} \\alpha\_{0}$$

#### Exercise 7 - update_lr

Calculate the new learning rate using exponential weight decay.

``` python
# GRADED FUNCTION: update_lr
def update_lr(learning_rate0, epoch_num, decay_rate):
    """
    Calculates updated the learning rate using exponential weight decay.
    Arguments:
    learning_rate0 -- Original learning rate. Scalar
    epoch_num -- Epoch number. Integer
    decay_rate -- Decay rate. Scalar
    Returns:
    learning_rate -- Updated learning rate. Scalar 
    """
    #(approx. 1 line)
    # learning_rate = 
    # YOUR CODE STARTS HERE
    learning_rate = (1 / (1 + decay_rate * epoch_num)) * learning_rate0
    # YOUR CODE ENDS HERE
    return learning_rate
```

![](~/Desktop/Bryan/deep_learning.ai_coursera/Month2:Tunning%20Hyperparameters/b4.png)

### Fixed Interval Scheduling

You can help prevent the learning rate speeding to zero too quickly by
scheduling the exponential learning rate decay at a fixed time interval,
for example 1000. You can either number the intervals, or divide the
epoch by the time interval, which is the size of window with the
constant learning rate.

#### Exercise 8 - schedule_lr_decay

Calculate the new learning rate using exponential weight decay with
fixed interval scheduling.

**Instructions**: Implement the learning rate scheduling such that it
only changes when the epochNum is a multiple of the timeInterval.

**Note:** The fraction in the denominator uses the floor operation.

$$\\alpha = \\frac{1}{1 + decayRate \\times \\lfloor\\frac{epochNum}{timeInterval}\\rfloor} \\alpha\_{0}$$

**Hint:**
[numpy.floor](https://numpy.org/doc/stable/reference/generated/numpy.floor.html)

``` python
# GRADED FUNCTION: schedule_lr_decay
def schedule_lr_decay(learning_rate0, epoch_num, decay_rate, time_interval=1000):
    """
    Calculates updated the learning rate using exponential weight decay.
    Arguments:
    learning_rate0 -- Original learning rate. Scalar
    epoch_num -- Epoch number. Integer.
    decay_rate -- Decay rate. Scalar.
    time_interval -- Number of epochs where you update the learning rate.
    Returns:
    learning_rate -- Updated learning rate. Scalar 
    """
    # (approx. 1 lines)
    # learning_rate = ...
    # YOUR CODE STARTS HERE
    learning_rate = 1 / (1 + decay_rate * np.floor(epoch_num / time_interval)) * learning_rate0
    # YOUR CODE ENDS HERE
    return learning_rate
```

### Using Learning Rate Decay for each Optimization Method

Below, you’ll use the following “moons” dataset to test the different
optimization methods. (The dataset is named “moons” because the data
from each of the two classes looks a bit like a crescent-shaped moon.)

#### Gradient Descent with Learning Rate Decay

Run the following code to see how the model does gradient descent and
weight decay.

![](~/Desktop/Bryan/deep_learning.ai_coursera/Month2:Tunning%20Hyperparameters/b5.png)

#### Gradient Descent with Momentum and Learning Rate Decay

Run the following code to see how the model does gradient descent with
momentum and weight decay.

![](~/Desktop/Bryan/deep_learning.ai_coursera/Month2:Tunning%20Hyperparameters/b6.png)

#### Adam with Learning Rate Decay

Run the following code to see how the model does Adam and weight decay.

![](~/Desktop/Bryan/deep_learning.ai_coursera/Month2:Tunning%20Hyperparameters/b7.png)

### Achieving similar performance with different methods

With SGD or SGD with Momentum, the accuracy is significantly lower than
Adam, but when learning rate decay is added on top, either can achieve
performance at a speed and accuracy score that’s similar to Adam.

In the case of Adam, notice that the learning curve achieves a similar
accuracy but faster.

# Week 3: Tunning hyper parameters

## Tunning process

Of all hyperparamters the hierarchy of ones to tune typically is as
fallows:

1.  learning rate (*α*)

2.1. mini-batch size 2.2. momentum(*β*) 2.3. \# of hidden units

3.1. \# of layers 3.2. learning rate decay

It’s common practice to take a **Course to Fine** approach when tuning
parameters. What that means is that when you start to find parameters
that look more promising than others, zoom in on those values and do
more densely populated testing in a smaller window around those
parameters.

## Using a proper scale for picking hyperparameters

When determining a range of values to test over for hyperparameters it’s
important not to just let the random selection come from all possible
values.

### Learning Rate example

For example while searching for a learning rate, your scale ranges from
0.0001 and 1. This means 90% of the values sampled, if chosen uniformly,
will be between 0.1 and 1. This is not the most efficient choosing as
you typically want a smaller learning rate. It’s better to chose these
samples using a log scale between 0.0001 and 1.

### Momentum example

For the momentum term in exponentially weighted averages we will usually
want a values between *β* = 0.9 and *β* = 0.999. We can perform a
similar operation for *β* like we did for *α*. The operational
difference would be to subtract the randomly chosen value from 1,
returning some values between *β* = 0.9 and *β* = 0.999

## Model testing approach

### Babysitting

This happens typically when you don’t have much computational resources.
This means that incrementally you keep testing adaptations to the same
model.

### Parallel

You run many model variations at the same time and juxtapose the cost
function graphs to see which may work best.

## Normalization ACtivation in a Network

This is a technique that makes your model more robust to hyperparameter
variations. this is very helpful when trying to train a model.

Normalizing the inputs speeds up the learning process. In the case of
{EqT} this would be done in the {sac2EQTransformR} package if {EqT}
implements normalization.

If normalization in Batch Norm is applied, there may be scaling
parameters within some or all hidden layers. These parameters basically
shift the mean of the normalized inputs.

## Fitting Batch Norm into a Neural Network

Without batch norm, we find
*z*<sup>*i*</sup> = *w*<sup>*i*</sup> \* *x* + *b*<sup>*i*</sup>. We
then find *a*<sup>*i*</sup> = *g*<sup>*i*</sup>(*z*<sup>*i*</sup>). To
apply Batch Norm, we then scale *z*<sup>*i*</sup> by performing
*β*<sup>*i*</sup> \* *z*<sup>*i*</sup> + *γ*<sup>*i*</sup>. This *β* is
not momentum. These parameters are updated with the same
*β* = *β*<sup>*l*</sup> − *α*<sup>*l*</sup> \* *d**β*<sup>*l*</sup>
method in the backprop steps.

## Why does Batch Norm work?

Batch normalization helps because it causes your data to look similar
through out the hidden layer functions, thus speeding up the process.
This makes each layers learning semi-independent from other layers and
thus speed up the process.

There is a strange property of regularization, in that increasing the
size of the mini-batch diminishes the effect of regularization because
of droupout scaling.

## Softmax Regression

Softmax regression is a form of logistic regression that was designed
for non-binary classification cases. Essentially we start with the
regualr *z* = *W**a* + *b* but then our activation functions
*t* = *e*<sup>*z*</sup> , then $a=\\frac{e^z}{\\sum t}$ or
$g(z) =a=\\frac{t}{\\sum t}$.

## Training a Softmax Classifier

### Forwrad Pass

### Backward Pass

## Deep Learning Frameworks

-   Caffe/Caffe2

-   CNTK

-   DL4J

-   Keras

-   Lasagme

-   mxnet

-   PaddlePaddle

-   TensorFlow

-   Theano

-   Torch

When choosing one of these frameworks, it is important to think of:

1.  Ease of programming
2.  Running speed
3.  Truly open source

## Tensorflow

Tensorflow is a popular framework for deep learning. Lets use Tensorflow
to solve a quadratic equation.

``` python
import numpy as np
import tensorflow as tf

w = tf.Variable(0, dtype=tf.float32)
optimizer = tf.keras.optimizers.Adam(0.1)

def train_step:
  with tf.GradientTape() as tape:
    cost = w ** 2 - 10 * w + 25
  trainable_variables = [w]
  grads = tape.gradient(cost, trainable_variables)
  optimizer.apply_gradients(zip(grads, trainable_variables))
  
print(w)

train_step()
print(w)

for i in range(1000):
  train_step()
print(w)
```

Tensorflow is easy to use as it only requires the forward propagation
steps from the user and con figure out the backward prop on its own.

# Assignment 1: Intro to Tensorflow

## Packages

``` python
import h5py
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
from tensorflow.python.framework.ops import EagerTensor
from tensorflow.python.ops.resource_variable_ops import ResourceVariable
import time
```

## 2 - Basic Optimization with GradientTape

The beauty of TensorFlow 2 is in its simplicity. Basically, all you need
to do is implement forward propagation through a computational graph.
TensorFlow will compute the derivatives for you, by moving backwards
through the graph recorded with `GradientTape`. All that’s left for you
to do then is specify the cost function and optimizer you want to use!

When writing a TensorFlow program, the main object to get used and
transformed is the `tf.Tensor`. These tensors are the TensorFlow
equivalent of Numpy arrays, i.e. multidimensional arrays of a given data
type that also contain information about the computational graph.

Below, you’ll use `tf.Variable` to store the state of your variables.
Variables can only be created once as its initial value defines the
variable shape and type. Additionally, the `dtype` arg in `tf.Variable`
can be set to allow data to be converted to that type. But if none is
specified, either the datatype will be kept if the initial value is a
Tensor, or `convert_to_tensor` will decide. It’s generally best for you
to specify directly, so nothing breaks!

Since TensorFlow Datasets are generators, you can’t access directly the
contents unless you iterate over them in a for loop, or by explicitly
creating a Python iterator using `iter` and consuming its elements using
`next`. Also, you can inspect the `shape` and `dtype` of each element
using the `element_spec` attribute.

There’s one more additional difference between TensorFlow datasets and
Numpy arrays: If you need to transform one, you would invoke the `map`
method to apply the function passed as an argument to each of the
elements.

``` python
def normalize(image):
    """
    Transform an image into a tensor of shape (64 * 64 * 3, 1)
    and normalize its components.
    Arguments
    image - Tensor.
    Returns: 
    result -- Transformed tensor 
    """
    image = tf.cast(image, tf.float32) / 256.0
    image = tf.reshape(image, [-1,1])
    return image
```

``` python
new_train = x_train.map(normalize)
new_test = x_test.map(normalize)
```

### Linear Function

Let’s begin this programming exercise by computing the following
equation: *Y* = *W**X* + *b*, where *W* and *X* are random matrices and
b is a random vector.

### Exercise 1 - linear_function

Compute *W**X* + *b* where *W*, *X*, and *b* are drawn from a random
normal distribution. W is of shape (4, 3), X is (3,1) and b is (4,1). As
an example, this is how to define a constant X with the shape (3,1):

``` python
X = tf.constant(np.random.randn(3,1), name = "X")
```

Note that the difference between `tf.constant` and `tf.Variable` is that
you can modify the state of a `tf.Variable` but cannot change the state
of a `tf.constant`.

You might find the following functions helpful: - tf.matmul(…, …) to do
a matrix multiplication - tf.add(…, …) to do an addition -
np.random.randn(…) to initialize randomly

``` python
# GRADED FUNCTION: linear_function
def linear_function():
    """
    Implements a linear function: 
            Initializes X to be a random tensor of shape (3,1)
            Initializes W to be a random tensor of shape (4,3)
            Initializes b to be a random tensor of shape (4,1)
    Returns: 
    result -- Y = WX + b 
    """
    np.random.seed(1)
    """
    Note, to ensure that the "random" numbers generated match the expected results,
    please create the variables in the order given in the starting code below.
    (Do not re-arrange the order).
    """
    # (approx. 4 lines)
    # X = ...
    # W = ...
    # b = ...
    # Y = ...
    # YOUR CODE STARTS HERE
    X = tf.constant(np.random.randn(3,1), name = "X")
    W = tf.Variable(np.random.randn(4,3), name = "W")
    b = tf.Variable(np.random.randn(4,1), name = "b")
    Y = tf.add(tf.matmul(W,X), b)
    # YOUR CODE ENDS HERE
    return Y
```

### Computing the Sigmoid

Amazing! You just implemented a linear function. TensorFlow offers a
variety of commonly used neural network functions like `tf.sigmoid` and
`tf.softmax`.

For this exercise, compute the sigmoid of z.

In this exercise, you will: Cast your tensor to type `float32` using
`tf.cast`, then compute the sigmoid using
`tf.keras.activations.sigmoid`.

### Exercise 2 - sigmoid

Implement the sigmoid function below. You should use the following:

-   `tf.cast("...", tf.float32)`
-   `tf.keras.activations.sigmoid("...")`

``` python
# GRADED FUNCTION: sigmoid
def sigmoid(z):
    """
    Computes the sigmoid of z
    Arguments:
    z -- input value, scalar or vector
    Returns: 
    a -- (tf.float32) the sigmoid of z
    """
    # tf.keras.activations.sigmoid requires float16, float32, float64, complex64, or complex128.
    # (approx. 2 lines)
    # z = ...
    # a = ...
    # YOUR CODE STARTS HERE
    z = tf.cast(z,tf.float32)
    a = tf.keras.activations.sigmoid(z)
    # YOUR CODE ENDS HERE
    return a
```

### 2.3 - Using One Hot Encodings

Many times in deep learning you will have a *Y* vector with numbers
ranging from 0 to *C* − 1, where *C* is the number of classes. If *C* is
for example 4, then you might have the following y vector which you will
need to convert like this:

This is called “one hot” encoding, because in the converted
representation, exactly one element of each column is “hot” (meaning set
to 1). To do this conversion in numpy, you might have to write a few
lines of code. In TensorFlow, you can use one line of code:

-   [tf.one_hot(labels, depth,
    axis=0)](https://www.tensorflow.org/api_docs/python/tf/one_hot)

`axis=0` indicates the new axis is created at dimension 0

### Exercise 3 - one_hot_matrix

Implement the function below to take one label and the total number of
classes *C*, and return the one hot encoding in a column wise matrix.
Use `tf.one_hot()` to do this, and `tf.reshape()` to reshape your one
hot tensor!

-   `tf.reshape(tensor, shape)`

``` python
# GRADED FUNCTION: one_hot_matrix
def one_hot_matrix(label, depth=6):
    """
    Computes the one hot encoding for a single label
    Arguments:
        label --  (int) Categorical labels
        depth --  (int) Number of different classes that label can take
    Returns:
         one_hot -- tf.Tensor A single-column matrix with the one hot encoding.
    """
    # (approx. 1 line)
    # one_hot = ...
    # YOUR CODE STARTS HERE
    one_hot = tf.reshape(tf.one_hot(label, depth, axis = 0), [depth, 1])
    # YOUR CODE ENDS HERE
    return one_hot
```

### 2.4 - Initialize the Parameters

Now you’ll initialize a vector of numbers between zero and one. The
function you’ll be calling is `tf.keras.initializers.GlorotNormal`,
which draws samples from a truncated normal distribution centered on 0,
with `stddev = sqrt(2 / (fan_in + fan_out))`, where `fan_in` is the
number of input units and `fan_out` is the number of output units, both
in the weight tensor.

To initialize with zeros or ones you could use `tf.zeros()` or
`tf.ones()` instead.

### Exercise 4 - initialize_parameters

Implement the function below to take in a shape and to return an array
of numbers between -1 and 1.

-   `tf.keras.initializers.GlorotNormal(seed=1)`
-   `tf.Variable(initializer(shape=())`

``` python
# GRADED FUNCTION: initialize_parameters
def initialize_parameters():
   """
   Initializes parameters to build a neural network with TensorFlow. The shapes are:
                       W1 : [25, 12288]
                       b1 : [25, 1]
                       W2 : [12, 25]
                       b2 : [12, 1]
                       W3 : [6, 12]
                       b3 : [6, 1]
   Returns:
   parameters -- a dictionary of tensors containing W1, b1, W2, b2, W3, b3
   """
   initializer = tf.keras.initializers.GlorotNormal(seed=1)   
   #(approx. 6 lines of code)
   # W1 = ...
   # b1 = ...
   # W2 = ...
   # b2 = ...
   # W3 = ...
   # b3 = ...
   # YOUR CODE STARTS HERE
   W1 = tf.Variable(initializer(shape=([25, 12288])))
   b1 = tf.Variable(initializer(shape=([25, 1])))
   W2 = tf.Variable(initializer(shape=([12, 25])))
   b2 = tf.Variable(initializer(shape=([12, 1])))
   W3 = tf.Variable(initializer(shape=([6, 12])))
   b3 = tf.Variable(initializer(shape=([6, 1])))
   # YOUR CODE ENDS HERE
   parameters = {"W1": W1,
                 "b1": b1,
                 "W2": W2,
                 "b2": b2,
                 "W3": W3,
                 "b3": b3}
   return parameters
```

## 3 - Building Your First Neural Network in TensorFlow

In this part of the assignment you will build a neural network using
TensorFlow. Remember that there are two parts to implementing a
TensorFlow model:

-   Implement forward propagation
-   Retrieve the gradients and train the model

Let’s get into it!

### Implement Forward Propagation

One of TensorFlow’s great strengths lies in the fact that you only need
to implement the forward propagation function.

Here, you’ll use a TensorFlow decorator, `@tf.function`, which builds a
computational graph to execute the function. `@tf.function` is
polymorphic, which comes in very handy, as it can support arguments with
different data types or shapes, and be used with other languages, such
as Python. This means that you can use data dependent control flow
statements.

When you use `@tf.function` to implement forward propagation, the
computational graph is activated, which keeps track of the operations.
This is so you can calculate your gradients with backpropagation.

### Exercise 5 - forward_propagation

Implement the `forward_propagation` function.

**Note** Use only the TF API.

-   tf.math.add
-   tf.linalg.matmul
-   tf.keras.activations.relu

``` python
# GRADED FUNCTION: forward_propagation

@tf.function
def forward_propagation(X, parameters):
    """
    Implements the forward propagation for the model: LINEAR -> RELU -> LINEAR -> RELU -> LINEAR
    Arguments:
    X -- input dataset placeholder, of shape (input size, number of examples)
    parameters -- python dictionary containing your parameters "W1", "b1", "W2", "b2", "W3", "b3"
                  the shapes are given in initialize_parameters
    Returns:
    Z3 -- the output of the last LINEAR unit
    """
    # Retrieve the parameters from the dictionary "parameters" 
    W1 = parameters['W1']
    b1 = parameters['b1']
    W2 = parameters['W2']
    b2 = parameters['b2']
    W3 = parameters['W3']
    b3 = parameters['b3']
    #(approx. 5 lines)                   # Numpy Equivalents:
    # Z1 = ...                           # Z1 = np.dot(W1, X) + b1
    # A1 = ...                           # A1 = relu(Z1)
    # Z2 = ...                           # Z2 = np.dot(W2, A1) + b2
    # A2 = ...                           # A2 = relu(Z2)
    # Z3 = ...                           # Z3 = np.dot(W3, A2) + b3
    # YOUR CODE STARTS HERE
    Z1 = tf.math.add(tf.linalg.matmul(W1,X),b1)
    A1 = tf.keras.activations.relu(Z1) 
    Z2 = tf.math.add(tf.linalg.matmul(W2,A1),b2)
    A2 = tf.keras.activations.relu(Z2) 
    Z3 = tf.math.add(tf.linalg.matmul(W3,A2),b3)
    A3 = tf.keras.activations.relu(Z3) 
    # YOUR CODE ENDS HERE
    return Z3
```

### Compute the Cost

Here again, the delightful `@tf.function` decorator steps in and saves
you time. All you need to do is specify how to compute the cost, and you
can do so in one simple step by using:

`tf.reduce_mean(tf.keras.losses.binary_crossentropy(y_true = ..., y_pred = ..., from_logits=True))`

### Exercise 6 - compute_cost

Implement the cost function below. - It’s important to note that the
“`y_pred`” and “`y_true`” inputs of
[tf.keras.losses.binary_crossentropy](https://www.tensorflow.org/api_docs/python/tf/keras/losses/binary_crossentropy)
are expected to be of shape (number of examples, num_classes). Since
both the transpose and the original tensors have the same values, just
in different order, the result of calculating the binary_crossentropy
should be the same if you transpose or not the logits and labels. Just
for reference here is how the Binary Cross entropy is calculated in
TensorFlow:

`mean_reduce(max(logits, 0) - logits * labels + log(1 + exp(-abs(logits))), axis=-1)`

-   `tf.reduce_mean` basically does the summation over the examples.

``` python
# GRADED FUNCTION: compute_cost 
@tf.function
def compute_cost(logits, labels):
    """
    Computes the cost
    Arguments:
    logits -- output of forward propagation (output of the last LINEAR unit), of shape (6, number of examples)
    labels -- "true" labels vector, same shape as Z3
    
    Returns:
    cost - Tensor of the cost function
    """
    #(1 line of code)
    # cost = ...
    # YOUR CODE STARTS HERE
    cost = tf.reduce_mean(tf.keras.losses.binary_crossentropy(y_true = labels, y_pred = logits, from_logits=True))
    # YOUR CODE ENDS HERE
    return cost
```

### 3.3 - Train the Model

Let’s talk optimizers. You’ll specify the type of optimizer in one line,
in this case `tf.keras.optimizers.Adam` (though you can use others such
as SGD), and then call it within the training loop.

Notice the `tape.gradient` function: this allows you to retrieve the
operations recorded for automatic differentiation inside the
`GradientTape` block. Then, calling the optimizer method
`apply_gradients`, will apply the optimizer’s update rules to each
trainable parameter. At the end of this assignment, you’ll find some
documentation that explains this more in detail, but for now, a simple
explanation will do. ;)

Here you should take note of an important extra step that’s been added
to the batch training process:

-   `tf.Data.dataset = dataset.prefetch(8)`

What this does is prevent a memory bottleneck that can occur when
reading from disk. `prefetch()` sets aside some data and keeps it ready
for when it’s needed. It does this by creating a source dataset from
your input data, applying a transformation to preprocess the data, then
iterating over the dataset the specified number of elements at a time.
This works because the iteration is streaming, so the data doesn’t need
to fit into the memory.

``` python
def model(X_train, Y_train, X_test, Y_test, learning_rate = 0.0001,
          num_epochs = 1500, minibatch_size = 32, print_cost = True):
    """
    Implements a three-layer tensorflow neural network: LINEAR->RELU->LINEAR->RELU->LINEAR->SOFTMAX.
    Arguments:
    X_train -- training set, of shape (input size = 12288, number of training examples = 1080)
    Y_train -- test set, of shape (output size = 6, number of training examples = 1080)
    X_test -- training set, of shape (input size = 12288, number of training examples = 120)
    Y_test -- test set, of shape (output size = 6, number of test examples = 120)
    learning_rate -- learning rate of the optimization
    num_epochs -- number of epochs of the optimization loop
    minibatch_size -- size of a minibatch
    print_cost -- True to print the cost every 100 epochs
    Returns:
    parameters -- parameters learnt by the model. They can then be used to predict.
    """
    costs = []                                        # To keep track of the cost
    # Initialize your parameters
    #(1 line)
    parameters = initialize_parameters()
    W1 = parameters['W1']
    b1 = parameters['b1']
    W2 = parameters['W2']
    b2 = parameters['b2']
    W3 = parameters['W3']
    b3 = parameters['b3']
    optimizer = tf.keras.optimizers.SGD(learning_rate)
    X_train = X_train.batch(minibatch_size, drop_remainder=True).prefetch(8)# <<< extra step    
    Y_train = Y_train.batch(minibatch_size, drop_remainder=True).prefetch(8) # loads memory faster 
    # Do the training loop
    for epoch in range(num_epochs):
        epoch_cost = 0.
        for (minibatch_X, minibatch_Y) in zip(X_train, Y_train):
            # Select a minibatch
            with tf.GradientTape() as tape:
                # 1. predict
                Z3 = forward_propagation(minibatch_X, parameters)
                # 2. loss
                minibatch_cost = compute_cost(Z3, minibatch_Y)
            trainable_variables = [W1, b1, W2, b2, W3, b3]
            grads = tape.gradient(minibatch_cost, trainable_variables)
            optimizer.apply_gradients(zip(grads, trainable_variables))
            epoch_cost += minibatch_cost / minibatch_size
        # Print the cost every epoch
        if print_cost == True and epoch % 10 == 0:
            print ("Cost after epoch %i: %f" % (epoch, epoch_cost))
        if print_cost == True and epoch % 5 == 0:
            costs.append(epoch_cost)
    # Plot the cost
    plt.plot(np.squeeze(costs))
    plt.ylabel('cost')
    plt.xlabel('iterations (per fives)')
    plt.title("Learning rate =" + str(learning_rate))
    plt.show()
    # Save the parameters in a variable
    print ("Parameters have been trained!")
    return parameters
```
